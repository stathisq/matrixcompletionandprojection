import numpy as np
from numpy import genfromtxt
from scipy import linalg
import scipy.optimize as optimize

def solveSystem(h, b,delta=1000):
    "function_docstring"
    r = 4
    e = 0.1
    s = ((r * np.log(r)) / r) * np.log(r / e)
    # print("Subset size", s)
    # print(h.shape)
    idx = np.random.randint(low=0, high=h.shape[0]-1, size=int(10))
    subset = h
    minus_subset = subset*(-1)

    final_subset = np.vstack((minus_subset,subset))
    b_ub=[]
    for i in range(0,19):
        b_ub.append(delta-b[i])
    for i in range(0,19):
        b_ub.append(b[i]+delta)
    c=np.zeros([b.shape[0]])+1
    # c = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    A_ub = final_subset

    # print('A_ub:', A_ub.shape,'f:',len(c), 'b_ub:', b_ub)
    res= optimize.linprog(c=c,A_ub=A_ub,b_ub=b_ub, options={"disp": False})
    # if res.status == 2:
    #     print('Status:', res.status)

    return res.status






# def solveSystem(filename):
#     "function_docstring"
#     my_data = genfromtxt(filename, delimiter=',')
#
#     r = 4
#     e = 0.1
#     s = ((r * np.log(r)) / r) * np.log(r / e)
#     print(s)
#     a = my_data
#     idx = np.random.randint(a.shape[0], size=int(s))
#     subset = a[idx, :]
#     subset = np.transpose(subset)
#     b = np.random.rand(my_data.shape[1])
#     print(b.shape)
#     print(a.shape)
#     print(subset.shape)
#     c = np.linalg.lstsq(subset, b)
#     print(np.linalg.norm(subset.dot(c[0]) - b))
#
#     return c
