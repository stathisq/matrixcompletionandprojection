
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

# Load the diabetes dataset

data= np.genfromtxt("../regressionAll.csv",delimiter=",")
print(data.shape)
input_y=data[:,0]
input_x=data[:,1]
input_x= input_x.reshape((input_x.shape[0],1))
input_y= input_y.reshape((input_y.shape[0],1))
# diabetes = datasets.load_diabetes()


# Use only one feature
# diabetes_X = diabetes.data[:, np.newaxis, 1]

# x= np.random.rand(10,1)
# print(x)




# Split the data into training/testing sets
# diabetes_X_test = diabetes_X[-20:]
# Split the targets into training/testing sets
# diabetes_y_train = diabetes.target[:-20]
# diabetes_y_test = diabetes.target[-20:]

# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(input_x, input_y)

# Make predictions using the testing set
diabetes_y_pred = regr.predict(input_x)

# The coefficients
print('Coefficients: \n', regr.coef_)
# The mean squared error
print("Mean squared error: %.2f"
      % mean_squared_error(input_y, diabetes_y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(input_y, diabetes_y_pred))

# Plot outputs
print(len(input_x))
plt.scatter(input_x, input_y,  color='black',s=1)
plt.plot(input_x, diabetes_y_pred, color='blue', linewidth=1)
plt.xticks(fontsize=10)
plt.yticks(fontsize=10)

plt.tight_layout()
plt.show()