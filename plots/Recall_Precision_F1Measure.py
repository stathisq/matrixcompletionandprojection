import os
import matplotlib.pyplot as plt


def recallPlot(recall=[],deltas=[],figure_n=0,path="1"):
    label1 = "Recall"
    fontsize=15

    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0, 1])
    plt.plot(deltas, recall, '-go', linewidth=2, label=label1)
    # plt.plot(deltas_invalid, eventFlow, '-ro', linewidth=2, label=label2)
    plt.xlabel("Delta", fontsize=fontsize)
    plt.ylabel("Recall(%)", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/"):
        os.makedirs("plot_files/"+path+"/")
    save_plase = "plot_files/"+path+"/recall.pdf"
    plt.savefig(save_plase)
    plt.close()
    return




def plotWithErrorBar(recall_avg=[],recall_min=[],deltas=[],figure_n=0,ecolor='r',label="",fmt='',fname="",yname="",path="1"):
    label1 = label
    fontsize=15

    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0, 1])
    plt.errorbar(deltas, recall_avg,yerr=recall_min,fmt=fmt,ecolor=ecolor,elinewidth=0.5,capsize=2,capthick=1,linewidth=2, label=label1)
    plt.xlabel("Delta", fontsize=fontsize)
    plt.ylabel(yname, fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    x0, x1 = ax.get_xlim()
    y0, y1 = ax.get_ylim()
    ax.set_aspect((x1 - x0) / (y1 - y0))
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/"):
        os.makedirs("plot_files/"+path+"/")
    save_plase = "plot_files/"+path+"/"+str(fname)+".pdf"
    plt.savefig(save_plase)
    plt.close()
    return


def plotMultiple(recall_avg={},deltas=[],figure_n=0,ecolor='r',label="",fmt='',fname="",yname="",path="1",pos=0):
    label1 = label
    fontsize=15
    colors= ["-r","-y","-b","-g"]
    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0, 1])
    i  =0
    for k in recall_avg.keys():
        temp_l= label+" $\mu$="+str(k)
        if pos==2:
            print(max(recall_avg[k][pos]))
        plt.errorbar(deltas, recall_avg[k][pos],fmt=colors[i],ecolor=ecolor,elinewidth=0.5,capsize=2,capthick=1,linewidth=2,label=temp_l)
        i+=1

    plt.xlabel("Delta", fontsize=fontsize)
    plt.ylabel(yname, fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    x0, x1 = ax.get_xlim()
    y0, y1 = ax.get_ylim()
    #ax.set_aspect((x1 - x0) / (y1 - y0))
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/"):
        os.makedirs("plot_files/"+path+"/")
    save_plase = "plot_files/"+path+"/"+str(fname)+"_mean.pdf"
    plt.savefig(save_plase)
    plt.close()
    return



def precisionPlot(precision=[],deltas=[],figure_n=0,path="1"):
    label1 = "Precision"
    fontsize=15

    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0, 1])
    plt.plot(deltas, precision, '-ro', linewidth=2, label=label1)
    # plt.plot(deltas_invalid, eventFlow, '-ro', linewidth=2, label=label2)
    plt.xlabel("Delta", fontsize=fontsize)
    plt.ylabel("Precision(%)", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/"):
        os.makedirs("plot_files/"+path+"/")
    save_plase = "plot_files/"+path+"/precision.pdf"
    plt.savefig(save_plase)
    plt.close()
    return


def recallPrecisionF1ScorePlot(recall=[],precision=[],f1_score=[],deltas=[],figure_n=0,path="1"):
    label1 = "Recall"
    label2 = "Precision"
    label3 = "F1 Score"
    fontsize=15

    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0, 1])
    plt.plot(deltas, recall, '-go', linewidth=2, label=label1)
    plt.plot(deltas, precision, '-ro', linewidth=2, label=label2)
    plt.plot(deltas, f1_score, '-bo', linewidth=2, label=label3)
    plt.xlabel("Delta", fontsize=fontsize)
    plt.ylabel("Score(%)", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/"):
        os.makedirs("plot_files/"+path+"/")
    save_plase = "plot_files/"+path+"/recall_precisionf1_score.pdf"
    plt.savefig(save_plase)
    plt.close()
    return





def f1ScorePlot(f1_score=[],deltas=[],figure_n=0,path="1"):
    label1 = "F1 Score"
    fontsize=15

    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0, 1])
    plt.plot(deltas, f1_score, '-bo', linewidth=2, label=label1)
    # plt.plot(deltas_invalid, eventFlow, '-ro', linewidth=2, label=label2)
    plt.xlabel("Delta", fontsize=fontsize)
    plt.ylabel("F1 Score(%)", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/"):
        os.makedirs("plot_files/"+path+"/")
    save_plase = "plot_files/"+path+"/f1_score.pdf"
    plt.savefig(save_plase)
    plt.close()
    return