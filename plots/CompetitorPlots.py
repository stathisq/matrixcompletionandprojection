import matplotlib
import numpy as np
import matplotlib.pyplot as plt


knn =  [68,83,83,95,94,96]
pca =  [92,93,93,95,95,97]
hbos = [77,95,95,95,93,96]
abod = [23,78,83,91,93,94]
modular = [65,70,77,75,78,93]

patterns = [ "/" , "\\"  ]
means =[5,10,15,20,25,30]
i=0
number_of_groups = 6

fontsize=15


fig, ax = plt.subplots()

index = np.arange(number_of_groups)
bar_width = 0.2

opacity = 1
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, knn, bar_width,
                 alpha=opacity,
                 color='g',
                 label='KNN',
                 hatch =patterns[0])

rects2 = plt.bar(index + bar_width, pca, bar_width,
                 alpha=opacity,
                 color='r',
                 label='PCA',
                 hatch =patterns[1])
rects3 = plt.bar(index + 2*bar_width, hbos, bar_width,
                 alpha=opacity,
                 color='b',
                 label='HBOS',
                 hatch =patterns[0])

rects4 = plt.bar(index + 3*bar_width, hbos, bar_width,
                 alpha=opacity,
                 color='y',
                 label='ABOD',
                 hatch =patterns[1])
rects5 = plt.bar(index + 4*bar_width, modular, bar_width,
                 alpha=opacity,
                 color='m',
                 label='MODULaR',
                 hatch =patterns[0])

plt.xlabel('Mean ($\mu$)',fontsize=fontsize)
plt.ylabel('ROC (%)',fontsize=fontsize)
plt.xticks(index +3*bar_width / 2, ('5','10', '15', '20', '25', '30'),fontsize=fontsize)
plt.yticks(fontsize=fontsize)
plt.legend(fontsize=fontsize,loc=4)

plt.tight_layout()
plt.savefig("ROC.pdf")
plt.show()


knn =  [71,83,90,95,94,98]
pca =  [79,80,91,93,94,96]
hbos = [79,80,91,93,96,94]
abod = [57,80,89,93,93,94]
modular = [67,68,68,70,71,78]

patterns = [ "/" , "\\"  ]
means =[5,10,15,20,25,30]
i=0
number_of_groups = 6

fontsize=15


fig, ax = plt.subplots()

index = np.arange(number_of_groups)
bar_width = 0.18

opacity = 1
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, knn, bar_width,
                 alpha=opacity,
                 color='g',
                 label='KNN',
                 hatch =patterns[0])

rects2 = plt.bar(index + bar_width, pca, bar_width,
                 alpha=opacity,
                 color='r',
                 label='PCA',
                 hatch =patterns[1])
rects3 = plt.bar(index + 2*bar_width, hbos, bar_width,
                 alpha=opacity,
                 color='b',
                 label='HBOS',
                 hatch =patterns[0])

rects4 = plt.bar(index + 3*bar_width, hbos, bar_width,
                 alpha=opacity,
                 color='y',
                 label='ABOD',
                 hatch =patterns[1])
rects5 = plt.bar(index + 4*bar_width, modular, bar_width,
                 alpha=opacity,
                 color='m',
                 label='MODULaR',
                 hatch =patterns[0])

plt.xlabel('Mean ($\mu$)',fontsize=fontsize)
plt.ylabel('F1 Score (%)',fontsize=fontsize)
plt.xticks(index +4*bar_width / 2, ('5','10', '15', '20', '25', '30'),fontsize=fontsize)
plt.yticks(fontsize=fontsize)
plt.legend(fontsize=fontsize,loc=4)

plt.tight_layout()
plt.savefig("f1.pdf")
plt.show()


