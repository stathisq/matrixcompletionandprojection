import os
import matplotlib.pyplot as plt
import numpy as np


def inputMatrixHistogram(kwargs,figure_n=0,path=".",):
    """

    :param my_dict: Dictionary containing all the pair of <value,times> for the normal matrix
    :param my_dict2: Dictionary containing all the pair of <value,times> for the event matrix
    :param figure_n:
    :return: null
    """
    labels=[]
    label1 = "Normal"
    label2 = "Noisy"
    fontsize=15
    colors=['-m','-b','-c','-r','-g']
    ax=plt.figure(figure_n).gca()
    i=0
    for k in kwargs.keys():
        if k==0:
            label="Normal"
        else:
            label="Noisy ($\mu$="+str(k)+")"
        my_dict=kwargs[k]
        plt.plot(list(my_dict.keys())[1:170], list(my_dict.values())[1:170], colors[i], linewidth=2, label=label)
        i+=1

    #plt.plot(list(my_dict2.keys()), list(my_dict2.values()), '-r', linewidth=2, label=label2)
    # plt.plot(deltas_invalid, eventFlow, '-ro', linewidth=2, label=label2)
    plt.xlabel("Value", fontsize=fontsize)
    plt.ylabel("Frequency", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    ax.set_ylim(ymin=0)
    ax.set_xlim(xmin=0)
    plt.legend(fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/"):
        os.makedirs("plot_files/"+path+"/")
    save_plase = "plot_files/"+path+"/values_distribution.pdf"
    plt.savefig(save_plase)
    plt.show()
    plt.close()
    return





def inputRowHistogramFromDict(kwargs, figure_n=0, keys_to_show=(), path="files/", ):
    
    if len(keys_to_show)>0:
        d = {k:v for k,v in kwargs.items() if k in keys_to_show}
    else:
        d=kwargs
    fontsize=15
    colors=['mo','bo','co','ro','go']
    ax=plt.figure(figure_n).gca()
    i=0
    for k in d.keys():



        label="Sample "+str(k)
        my_dict=d[k]
        plt.plot(list(my_dict.keys()), list(my_dict.values()), colors[i],marker='.', linewidth=2, label=label)
        i+=1
        if i==4:
            break

    #plt.plot(list(my_dict2.keys()), list(my_dict2.values()), '-r', linewidth=2, label=label2)
    # plt.plot(deltas_invalid, eventFlow, '-ro', linewidth=2, label=label2)
    plt.xlabel("Value", fontsize=fontsize)
    plt.ylabel("Frequency", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    ax.set_ylim(ymin=0,ymax=200)
    ax.set_xlim(xmin=0,xmax=100)
    plt.legend(fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    # if not os.path.exists("plot_files/"+path+"/"):
    #     os.makedirs("plot_files/"+path+"/")
    # save_plase = "plot_files/"+path+"/values_distribution.pdf"
    # plt.savefig(save_plase)
    plt.show()
    plt.close()


def inputRowHistogram(values,stds, figure_n=0,color='g',label=""):
    x_labels=list(range(0,len(values)))
    fontsize = 15
    ax = plt.figure(figure_n).gca()

    plt.errorbar(x_labels, values,yerr=np.array(stds).reshape(len(stds,)),color=color,marker='.', linewidth=2, label=label)


    # plt.plot(list(my_dict2.keys()), list(my_dict2.values()), '-r', linewidth=2, label=label2)
    # plt.plot(deltas_invalid, eventFlow, '-ro', linewidth=2, label=label2)
    plt.xlabel("Value", fontsize=fontsize)
    plt.ylabel("Frequency", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)

    ax.set_xlim(xmin=0, xmax=100)
    plt.legend(fontsize=fontsize)
    plt.tight_layout()

    plt.show()
    plt.close()




def inputRowsHistogram(values,stds, figure_n=0,color=('g','r'),label=("Normal","Event"),delta_value=0):
    x_labels=list(range(0,len(values[0])))
    fontsize = 15
    ax = plt.figure(figure_n).gca()

    plt.errorbar(x_labels, values[1],yerr=np.array(stds[1]).reshape(len(stds[1],)),color=color[1],marker='.',capsize=2, linewidth=1, label=label[1])
    plt.errorbar(x_labels, values[0],yerr=np.array(stds[0]).reshape(len(stds[0],)),color=color[0],marker='.',capsize=2, linewidth=1, label=label[0])


    # plt.plot(list(my_dict2.keys()), list(my_dict2.values()), '-r', linewidth=2, label=label2)
    # plt.plot(deltas_invalid, eventFlow, '-ro', linewidth=2, label=label2)
    plt.xlabel("Value", fontsize=fontsize)
    plt.ylabel("Frequency", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    ax.set_ylim(ymin=0, ymax=150)
    ax.set_xlim(xmin=0, xmax=100)
    plt.legend(fontsize=fontsize)
    plt.tight_layout()
    plt.savefig("files/deviations_"+str(delta_value)+".pdf")
    plt.show()
    plt.close()
