import os
from collections import OrderedDict

from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator

fontsize=15



def DistanceChart(avgFlow,normalFlow,eventFlow,k=0,figure_n=0,day=0,delta=0,type="event",path="1"):
    label1 = "Normal"

    label2 = "Event"

    label3 = "Historical"

    # for xe, ye in zip(avgFlow, normalFlow):
    #     plt.scatter([xe] * len(ye), ye)

    sensors=list(range(1,len(avgFlow)+1))
    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')

    if type=="event":
        plt.plot(sensors, eventFlow, 'r*', ms=10, linewidth=2, label=label2)
    else:
        plt.plot(sensors, eventFlow, 'g*', ms=10, linewidth=2, label=label2)

    plt.plot(sensors, avgFlow, 'bo', ms=5, linewidth=2, label=label3)
    plt.xlabel("Sensor ID", fontsize=fontsize)
    plt.ylabel("Average Flow", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys())
    # plt.axes().set_aspect('equal', 'datalim')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.title("Day: "+str(day))
    if not os.path.exists("plot_files/"+path+"/distance_all_sensors/"+"K="+str(k)+"/"+str(delta)):
        os.makedirs("plot_files/"+path+"/distance_all_sensors/"+"K="+str(k)+"/"+str(delta))
    save_plase = "plot_files/"+path+"/distance_all_sensors/"+"K="+str(k)+"/"+str(delta)+"/D=" + str(day) + "_avg_"+type+"Distance.pdf"
    plt.tight_layout()
    plt.savefig(save_plase)
    # plt.show()
    plt.close()
    return


def plotStandardDeviation(deviationMatrix,avgFlow,normalFlow,eventFlow,k=0,figure_n=0,day=0,type="Standard",delta=0,path="1"):
    label1 = "Normal"

    label2 = "Event"

    label3 = "Historical"

    # for xe, ye in zip(avgFlow, normalFlow):
    #     plt.scatter([xe] * len(ye), ye)

    sensors=list(range(1,len(avgFlow)+1))

    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0,1])
    if normalFlow.shape[1] != 0:
        plt.plot(sensors, normalFlow, 'go', ms=5, linewidth=1, label=label1)
    if eventFlow.shape[1]!=0:
        plt.plot(sensors, eventFlow, 'r*', ms=5, linewidth=1, label=label2)
    plt.errorbar(sensors, avgFlow, yerr=deviationMatrix,capsize=2, linestyle='None', fmt='-o',label=label3)
    # plt.plot(sensors, avgFlow, 'bo', ms=10, linewidth=2, label=label3)
    plt.xlabel("Sensor ID", fontsize=fontsize)
    plt.ylabel("Average Flow ("+type+" Deviation)", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys())
    # plt.axes().set_aspect('equal', 'datalim')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.title("Day: "+str(day))
    if type=="Standard":
        if not os.path.exists("plot_files/"+path+"/distance_all_sensors_std/"+"K="+str(k)+"delta="+str(delta)):
            os.makedirs("plot_files/"+path+"/distance_all_sensors_std/"+"K="+str(k)+"delta="+str(delta))
        save_plase = "plot_files/"+path+"/distance_all_sensors_std/"+"K="+str(k)+"delta="+str(delta)+"/D=" + str(day) + "_avg_"+str(eventFlow.shape[1])+"Distance.pdf"
    else:
        if not os.path.exists("plot_files/"+path+"/distance_all_sensors_mean/"+"K="+str(k)+"delta="+str(delta)):
            os.makedirs("plot_files/"+path+"/distance_all_sensors_mean/"+"K="+str(k)+"delta="+str(delta))
        save_plase = "plot_files/"+path+"/distance_all_sensors_mean/"+"K="+str(k)+"delta="+str(delta)+"/D=" + str(day) + "_avg_"+str(eventFlow.shape[1])+"Distance.pdf"

    plt.tight_layout()
    plt.savefig(save_plase)
    # plt.show()
    plt.close()
    return


def plotStandardDeviationWithNormalAndEvent(deviationMatrix,avgFlow,normalSamples,eventSamples,figure_n=0,type="Normal"):
    label1 = "Historical"
    label2 = "Normal Average"
    label3 = "Event Average"

    # for xe, ye in zip(avgFlow, normalFlow):
    #     plt.scatter([xe] * len(ye), ye)

    sensors=list(range(1,len(normalSamples)+1))

    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0,1])
    plt.errorbar(sensors,avgFlow, yerr=deviationMatrix,capsize=2, linestyle='None', fmt='-o',label=label1)
    plt.errorbar(sensors,normalSamples,capsize=2, linestyle='None', fmt='g*',label=label2)
    plt.errorbar(sensors,eventSamples,capsize=2, linestyle='None', fmt='r+',label=label3)
    # plt.plot(sensors, avgFlow, 'bo', ms=10, linewidth=2, label=label3)
    plt.xlabel("Sensor ID", fontsize=fontsize)
    plt.ylabel("Average Flow", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    if not os.path.exists("plot_files/"):
        os.makedirs("plot_files/")
    save_plase = "plot_files/Distance"+str(type)+".pdf"


    plt.tight_layout()
    plt.savefig(save_plase)
    # plt.show()
    plt.close()
    return


def plotStandardDeviationWithNormalAndEvents(deviationMatrix,avgFlow,normalSamples,eventSamples={},figure_n=0,type="Normal"):
    label1 = "Historical"
    label2 = "Normal Average"
    label3 = "Event Average"
    colors=['-y','-r','-c','-r']

    # for xe, ye in zip(avgFlow, normalFlow):
    #     plt.scatter([xe] * len(ye), ye)

    sensors=list(range(1,len(normalSamples)+1))

    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0,1])
    plt.errorbar(sensors,avgFlow, yerr=deviationMatrix,capsize=2, linestyle='None', fmt='-o',label=label1)
    plt.errorbar(sensors,normalSamples,capsize=2, linestyle='None', fmt='g*',label=label2)
    i = 0
    for k in eventSamples.keys():
        label = "Event Average ($\mu$="+ str(k)+")"
        if k ==5 or k == 35:
            plt.errorbar(sensors,eventSamples,capsize=2, linestyle='None', fmt=colors[i],label=label)
            i+=1
    # plt.plot(sensors, avgFlow, 'bo', ms=10, linewidth=2, label=label3)
    plt.xlabel("Sensor ID", fontsize=fontsize)
    plt.ylabel("Average Flow", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    if not os.path.exists("plot_files/"):
        os.makedirs("plot_files/")
    save_plase = "plot_files/Distance"+str(type)+".pdf"


    plt.tight_layout()
    plt.savefig(save_plase)
    # plt.show()
    plt.close()
    return


def plotStandardDeviationOnlySensors(deviationMatrix,avgFlow,figure_n=0,type="Normal"):

    label3 = "Historical"

    # for xe, ye in zip(avgFlow, normalFlow):
    #     plt.scatter([xe] * len(ye), ye)

    sensors=list(range(1,len(avgFlow)+1))

    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    ax.grid(which='both')
    ax.set_ylim([0,1])
    plt.errorbar(sensors, avgFlow, yerr=deviationMatrix,capsize=2, linestyle='None', fmt='-o',label=label3)
    # plt.plot(sensors, avgFlow, 'bo', ms=10, linewidth=2, label=label3)
    plt.xlabel("Sensor ID", fontsize=fontsize)
    plt.ylabel("Average Flow ("+type+" Samples)", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys())
    # plt.axes().set_aspect('equal', 'datalim')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    if type=="Normal":
        if not os.path.exists("plot_files/"):
            os.makedirs("plot_files/")
        save_plase = "plot_files/"+"DistanceNormal.pdf"
    else:
        if not os.path.exists("plot_files/"):
            os.makedirs("plot_files/")
        save_plase = "plot_files/"+"DistanceEvent.pdf"

    plt.tight_layout()
    plt.savefig(save_plase)
    # plt.show()
    plt.close()
    return

def avgDistanceChart(avgFlow=[],normalFlow=[],eventFlow=[],k=0,figure_n=0,delta=0,path="1"):
    label1 = "Normal"

    label2 = "Event"

    label3 = "Historical"


    sensors=list(range(1,len(avgFlow)+1))
    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()

    plt.plot(sensors, normalFlow, 'g+', ms=5, linewidth=1, label=label1)
    plt.plot(sensors, eventFlow, 'r*', ms=5, linewidth=1, label=label2)
    plt.plot(sensors, avgFlow, 'bo', ms=5, linewidth=1, label=label3)
    plt.xlabel("Sensor ID", fontsize=fontsize)
    plt.ylabel("Average Flow", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.title("Delta="+str(delta))
    if not os.path.exists("plot_files/"+path+"/avg_distance/"+"K="+str(k)):
        os.makedirs("plot_files/"+path+"/avg_distance/"+"K="+str(k))
    save_plase = "plot_files/"+path+"/avg_distance/"+"K="+str(k)+"/D=" + str(delta) + "_avg_Distance.pdf"
    plt.savefig(save_plase)
    # plt.show()
    plt.close()
    return





#
# def DistanceChart(avgFlow,normalFlow,eventFlow,k=0,figure_n=0,day=0,delta=0,type="event"):
#     label1 = "Normal"
#
#     label2 = "Event"
#
#     label3 = "Historical"
#
#     # for xe, ye in zip(avgFlow, normalFlow):
#     #     plt.scatter([xe] * len(ye), ye)
#
#     sensors=list(range(1,len(avgFlow)+1))
#     plt.figure(figure_n)
#     ax = plt.figure(figure_n).gca()
#     ax.grid(which='both')
#
#     if type=="event":
#         plt.plot(sensors, eventFlow, 'r*', ms=10, linewidth=2, label=label2)
#     else:
#         plt.plot(sensors, eventFlow, 'g*', ms=10, linewidth=2, label=label2)
#
#     plt.plot(sensors, avgFlow, 'bo', ms=5, linewidth=2, label=label3)
#     plt.xlabel("Sensor ID", fontsize=fontsize)
#     plt.ylabel("Average Flow", fontsize=fontsize)
#     plt.xticks(fontsize=fontsize)
#     plt.yticks(fontsize=fontsize)
#     handles, labels = plt.gca().get_legend_handles_labels()
#     by_label = OrderedDict(zip(labels, handles))
#     plt.legend(by_label.values(), by_label.keys())
#     # plt.axes().set_aspect('equal', 'datalim')
#     ax.xaxis.set_major_locator(MaxNLocator(integer=True))
#     plt.title("Day: "+str(day))
#     if not os.path.exists("plot_files_synthetic_mean_20/distance_all_sensors/"+"K="+str(k)+"/"+str(delta)):
#         os.makedirs("plot_files_synthetic_mean_20/distance_all_sensors/"+"K="+str(k)+"/"+str(delta))
#     save_plase = "plot_files_synthetic_mean_20/distance_all_sensors/"+"K="+str(k)+"/"+str(delta)+"/D=" + str(day) + "_avg_"+type+"Distance.pdf"
#     plt.tight_layout()
#     plt.savefig(save_plase)
#     # plt.show()
#     plt.close()
#     return