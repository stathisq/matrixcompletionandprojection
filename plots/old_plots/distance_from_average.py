"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""
import numpy as np
import matplotlib.pyplot as plt


fontsize=20
n_groups = 1

means_men = (15.3692086383,)
# std_men = (2, 3, 4, 1, 2)

means_women = (23.7528552893,)
# std_women = (3, 5, 2, 3, 3)


index = np.arange(n_groups)
bar_width = 0.1

opacity = 0.4
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, means_men, bar_width,
                 alpha=opacity,
                 color='g',
                 label='Normal')

rects2 = plt.bar(index + bar_width, means_women, bar_width,
                 alpha=opacity,
                 color='r',
                 label='Event')

plt.xlabel('Type of Event',fontsize=fontsize)
plt.ylabel('Distance from Average Value',fontsize=fontsize)

plt.xticks(index + bar_width, ('',),fontsize=fontsize)
plt.yticks(fontsize=fontsize)
plt.legend(fontsize=fontsize)
# plt.axes().set_aspect('equal', 'datalim')

plt.show()

