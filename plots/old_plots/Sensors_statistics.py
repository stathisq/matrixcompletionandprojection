"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""
import numpy as np
import matplotlib.pyplot as plt


n_groups = 6

means_men = (63.33,66.66,86.66, 93.33,90.0, 96.66 )
# std_men = (2, 3, 4, 1, 2)

means_women = (36.66, 33.33, 13.33, 6.66, 10.0,3.33)
# std_women = (3, 5, 2, 3, 3)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.35

opacity = 0.4
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, means_men, bar_width,
                 alpha=opacity,
                 color='g',
                 label='Normal')

rects2 = plt.bar(index + bar_width, means_women, bar_width,
                 alpha=opacity,
                 color='r',
                 label='Event')

plt.xlabel('K')
plt.ylabel('Input Samples (%)')
plt.title('New Readings Classification for different Values of K')
plt.xticks(index + bar_width / 2, ('5', '10', '20', '30', '40','50'))
plt.legend()

plt.tight_layout()
plt.show()