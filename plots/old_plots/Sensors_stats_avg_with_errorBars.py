"""
========
Barchart
========

A bar plot with errorbars and height labels on individual bars
"""
import numpy as np
import matplotlib.pyplot as plt

fontsize=15
N = 5
men_means = (50.62,63.09,105.90,46.21,8.19)
men_std = (131.0-50.62, 155.0-63.09, 349.0-105.90, 171.0-46.21,  46.0-8.19)
lolims = np.array([0, 0,0,0,0], dtype=bool)

ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars
opacity=0.8
fig, ax = plt.subplots()
rects1 = ax.bar(ind, men_means, width, color='g',alpha=opacity, yerr=men_std,bottom=0,error_kw=dict(ecolor='red', ls='--',  lw=1, capsize=2, capthick=2))
#
# women_means = (25, 32, 34, 20, 25)
# women_std = (3, 5, 2, 3, 3)
# rects2 = ax.bar(ind DublinSCATS@localhost+ width, women_means, width, color='y', yerr=women_std)
# ax.margins(0.05)

# add some text for labels, title and axes ticks
ax.set_ylabel('Flow',fontsize=fontsize)
ax.set_xlabel('Sensor ID',fontsize=fontsize)
# ax.set_title('Average And Maximum Flow for each Sensor',fontsize=fontsize)
ax.set_xticks(ind )
ax.set_xticklabels(('Sensor 1', 'Sensor 2', 'Sensor 3', 'Sensor 4', 'Sensor 5'),fontsize=fontsize)

# ax.legend((rects1[0],), ('Average-Max Flow',))


def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
# autolabel(rects2)

plt.show()