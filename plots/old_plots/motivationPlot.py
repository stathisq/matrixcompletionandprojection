import numpy as np
import matplotlib.pyplot as plt

N = 1

day1 = ( 23.871072549)

ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, day1, width, color='r')

# noise= 100
# error  219.834520961
# error  183.790678105

# noise = 10
# error  18.0618286901
# error  18.1981080989
day2 = (  24.9994431352)
rects2 = ax.bar(ind + width, day2, width, color='y')

day3 = (75.028189124)
rects3 = ax.bar(ind + 2*width, day3, width, color='b')

day4 = (33.003720807)
rects4 = ax.bar(ind + 3*width, day4, width, color='g')
# add some text for labels, title and axes ticks
ax.set_ylabel('Error')
ax.set_title('Error in approximation(Low Noise)')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('',))

ax.legend((rects1[0],rects2[0],rects3,rects4), ('Backtrack Day 1','Backtrack Day 2','Synth Day 1','Synth Day2'))


def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

# autolabel(rects1)
# autolabel(rects2)
# autolabel(rects3)
# autolabel(rects4)

plt.show()
