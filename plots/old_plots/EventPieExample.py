import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 20})

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'Normal', 'Event'
sizes = [62.68, 37.52]
explode = (0, 0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',colors=["green","red"],
        shadow=True, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.show()