"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""
import numpy as np
import matplotlib.pyplot as plt


n_groups = 12

means_men = (0, 6.666666666666667 ,10.0
             , 20.0, 26.666666666666668, 16.666666666666664 ,
             36.666666666666664, 30.0,30.0,43.333333333333336,
             66.66666666666666, 80 )
# std_men = (2, 3, 4, 1, 2)

means_women = (100, 93.33333333333333,90.0, 80.0, 73.33333333333333, 83.33333333333334,63.33333333333333, 70.0,70.0, 56.666666666666664,33.33333333333333, 20.0  )
# std_women = (3, 5, 2, 3, 3)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.35

opacity = 0.4
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, means_men, bar_width,
                 alpha=opacity,
                 color='g',
                 label='Normal')

rects2 = plt.bar(index + bar_width, means_women, bar_width,
                 alpha=opacity,
                 color='r',
                 label='Event')

plt.xlabel('Delta')
plt.ylabel('Input Samples (%)')
plt.title('New Readings Classification for different Values of Delta')
plt.xticks(index + bar_width / 2, ('0','1', '2', '3', '4', '5','6','7','8','9','10','11'))
plt.legend()

plt.tight_layout()
plt.show()