"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle


n_groups = 6
fontsize=15
def TimeAndError(results=[]):
    means_men=()
    means_women=()
    for r in results:
        means_men+=(r[0],)
        means_women+=(r[1],)

    max_men = max(means_men)
    max_women = max(means_women)
    means_men = tuple(ti/max_men for ti in means_men)
    means_women = tuple(ti/max_women for ti in means_women)


    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.4

    opacity = 1
    error_config = {'ecolor': '0.3'}

    rects1 = plt.bar(index, means_women, bar_width,
                     alpha=opacity,
                     color='g',
                     label='Reconstruction Error')

    rects2 = plt.bar(index + bar_width, means_men, bar_width,
                     alpha=opacity,
                     color='r',
                     label='Running Time')

    plt.xlabel('Dimension Size',fontsize=fontsize)
    plt.ylabel('Normalized Percentage',fontsize=fontsize)
    plt.xticks(index + bar_width / 2, ('5','10', '15', '20', '25', '30'),fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize,loc=4)

    plt.tight_layout()
    plt.savefig("time_error_comparison.pdf")
    plt.show()


def TimeComp(results=[]):
    means_men=()
    means_women=()
    for r in results:
        means_men+=(r[0],)
        means_women+=(r[1],)




    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.5

    opacity = 1
    error_config = {'ecolor': '0.3'}



    rects2 = plt.bar(index + bar_width/2, means_men, bar_width,
                     alpha=opacity,
                     color='r',
                     label='Running Time')

    plt.xlabel('Dimension Size (r)',fontsize=fontsize)
    plt.ylabel('Time (Seconds)',fontsize=fontsize)
    plt.xticks(index + bar_width / 2, ('5','10', '15', '20', '25', '30'),fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize,loc=2)

    plt.tight_layout()
    plt.savefig("time_comparison.pdf")
    plt.show()



def ErrorComp(results=[]):
    means_men=()
    means_women=()
    for r in results:
        means_men+=(r[0],)
        means_women+=(r[1],)




    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.5

    opacity = 1
    error_config = {'ecolor': '0.3'}



    rects2 = plt.bar(index + bar_width/2, means_women, bar_width,
                     alpha=opacity,
                     color='g',
                     label='Reconstruction Error')

    plt.xlabel('Dimension Size (r)',fontsize=fontsize)
    plt.ylabel('Error (RMSE)',fontsize=fontsize)
    plt.xticks(index + bar_width / 2, ('5','10', '15', '20', '25', '30'),fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize,loc=4)

    plt.tight_layout()
    plt.savefig("error_comparison.pdf")
    plt.show()


def TimeErrorScatter(results=[]):
    means_men = ()
    means_women = ()
    for r in results:
        means_men += (r[0],)
        means_women += (r[1],)

    max_men = max(means_men)
    max_women = max(means_women)
    means_men = tuple(ti / max_men for ti in means_men)
    means_women = tuple(ti / max_women for ti in means_women)

    ax = plt.figure(0).gca()



    plt.tight_layout()
    step = [5,10,20,30,40,50]
    plt.plot(step,means_men,'.r--',markersize=15,label='Running Time')
    plt.plot(step, means_women,'+g--',markersize=15,label='Reconstruction Error')

    plt.xlabel('Dimension (r)', fontsize=fontsize)
    plt.ylabel('Normalized Percentage', fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.xticks(step,fontsize=fontsize)

    plt.legend(fontsize=fontsize, loc=4)
    plt.tight_layout()

    plt.savefig("time_error_comparison.pdf")
    plt.show()



res=pickle.load(open("/home/stathis/PycharmProjects/matrixcompletionandprojection/main_local_test/results.p",'rb'))

0.5451105806265

TimeErrorScatter(res)
exit(4)
TimeAndError(res)
TimeComp(res)
ErrorComp(res)