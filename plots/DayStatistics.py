import matplotlib
import numpy as np

import matplotlib.pyplot as plt


def dayStats(input=[], figure_n=0):

    """

    :param input: Input timeseries Array
    :param figure_n:
    :return:
    """
    k = 6
    index = np.arange(start=k * 30, stop=k * 30 + 30)
    rows = input.shape[0]
    d = 0
    i = 0
    final_matrix = np.zeros([18, 0])
    # Every Day
    while d < 7:
        reduced_matrix = np.zeros([0, 30])
        i = d
        #Every Sample
        while i < rows:
            row = input[i, index]
            reduced_matrix = np.concatenate([reduced_matrix, row.reshape((1, row.shape[0]))], axis=0)
            i += 7
        z = np.zeros([reduced_matrix.shape[0], 10])
        reduced_matrix = np.concatenate([reduced_matrix, z], axis=1)
        #Day with less samples
        if reduced_matrix.shape[0] < 18:
            line = np.zeros([1, reduced_matrix.shape[1]])
            reduced_matrix = np.concatenate([reduced_matrix, line], axis=0)

        final_matrix = np.concatenate([final_matrix, reduced_matrix], axis=1)
        d += 1

    sensors = list(range(1, final_matrix.shape[1] + 1))
    fontsize = 10
    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    for i in range(0, final_matrix.shape[0]):
        plt.plot(sensors, final_matrix[i, :])
    # plt.plot(deltas, negatives, 'r-o', label=label2)
    xticks = [20, 60, 100, 140, 180, 220,260]
    xlbls = ['Sunday', 'Monday', 'Tuesday',
             'Wednesday', 'Thursday', 'Friday',"Saturday"]
    ax.set_xticks(xticks)
    ax.set_xticklabels(xlbls)

    plt.xlabel("Minute", fontsize=fontsize)
    plt.ylabel("Flow", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize, )
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    plt.show()
    return reduced_matrix





#
# import numpy as np
# import os
# from collections import OrderedDict
#
# import matplotlib
# from theano.gradient import np
#
# matplotlib.use('Qt5Agg')
# import matplotlib.pyplot as plt
#
#
# def dayStats(input=[], figure_n=0):
#     k = 1
#     index = np.arange(start=k * 30, stop=k * 30 + 30)
#     rows = input.shape[0]
#     d = 0
#     i = 0
#     final_matrix = np.zeros([18, 0])
#     # Every Day
#     while d < 7:
#         reduced_matrix = np.zeros([0, 30])
#         i = d
#         #Every Sample
#         while i < rows:
#             row = input[i, index]
#             reduced_matrix = np.concatenate([reduced_matrix, row.reshape((1, row.shape[0]))], axis=0)
#             i += 7
#         z = np.zeros([reduced_matrix.shape[0], 10])
#         reduced_matrix = np.concatenate([reduced_matrix, z], axis=1)
#         #Day with less samples
#         if reduced_matrix.shape[0] < 18:
#             line = np.zeros([1, reduced_matrix.shape[1]])
#             reduced_matrix = np.concatenate([reduced_matrix, line], axis=0)
#
#         final_matrix = np.concatenate([final_matrix, reduced_matrix], axis=1)
#         d += 1
#
#     sensors = list(range(1, final_matrix.shape[1] + 1))
#     fontsize = 20
#     plt.figure(figure_n)
#     ax = plt.figure(figure_n).gca()
#     for i in range(0, final_matrix.shape[0]):
#         plt.plot(sensors, final_matrix[i, :])
#     # plt.plot(deltas, negatives, 'r-o', label=label2)
#     xticks = [20, 60, 100, 140, 180, 220,260]
#     xlbls = ['Sunday', 'Monday', 'Tuesday',
#              'Wednesday', 'Thursday', 'Friday',"Saturday"]
#     ax.set_xticks(xticks)
#     ax.set_xticklabels(xlbls)
#
#     plt.xlabel("Day", fontsize=fontsize)
#     plt.ylabel("Flow", fontsize=fontsize)
#     plt.xticks(fontsize=fontsize)
#     plt.yticks(fontsize=fontsize)
#     plt.legend(fontsize=fontsize, )
#     # plt.axes().set_aspect('equal', 'datalim')
#     plt.tight_layout()
#     plt.show()
#     return reduced_matrix
#
#
# x = np.genfromtxt("../timeSeries.csv", delimiter=',')
#
# dayStats(x)
