import os
from collections import OrderedDict

import matplotlib
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

fontsize=20



def deltaVarianceChart(deltas=[],positives=[],negatives=[],m=0,k=0,figure_n=0,path="1"):
    label1="Normal (r="
    label1+=str(k)
    label1+=")"

    label2="Event (r="
    label2+=str(k)
    label2+=")"

    plt.figure(figure_n)
    plt.plot(deltas, positives, 'g-o', label=label1,)
    plt.plot(deltas, negatives, 'r-o', label=label2)
    plt.xlabel("Delta",fontsize=fontsize)
    plt.ylabel("Events (%)",fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize,)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()

    if not os.path.exists("plot_files/"+path+"/delta_variance_diffenent_k"):
        os.makedirs("plot_files/"+path+"/delta_variance_diffenent_k")
    save_plase="plot_files/"+path+"/delta_variance_diffenent_k/m="+str(m)+"K="+str(k)+"_Delta_Variance.pdf"
    plt.savefig(save_plase)
    plt.close()
    # plt.show()
    return

def new_input_whole_sensor_data_plot(daily_samples
                   ,x,K=0,Delta=0,remained=0,labels=[],path="1"):


    r = np.arange(start=0,stop=daily_samples)

    # size=input.shape[0]

    for k in range(0, int(x.shape[1] / daily_samples)):
        temp = 0
        for l in labels[:30]:
            i=int(l[1])

            index = np.arange(start=k * daily_samples, stop=k * daily_samples + daily_samples)

            label1 = "Sensor "
            label1 += str(k)

            # plt.figure(k)
            # if(type=="event"):
            if labels[temp]==0:
                plt.plot(r,x[i,index], 'b-o')
            if labels[temp]==1:
                plt.plot(r, x[i,index], 'r-o' )
            temp+=1
            # else:
            # plt.plot(r,x[index], 'b-o', label=label1, )
        # plt.plot(deltas, negatives, 'r-o', label=label2)
        plt.xlabel("Sample",fontsize=fontsize)
        plt.ylabel("Flow",fontsize=fontsize)
        plt.xticks(fontsize=fontsize)
        plt.yticks(fontsize=fontsize)
        plt.legend(fontsize=fontsize,)
        # plt.axes().set_aspect('equal', 'datalim')
        plt.tight_layout()

        if not os.path.exists("plot_files/"+path+"/sensor_whole_input/K="+str(K)+"D="+str(Delta)):
            os.makedirs("plot_files/"+path+"/sensor_whole_input/K="+str(K)+"D="+str(Delta))
        save_plase="plot_files/"+path+"/sensor_whole_input/"+"K="+str(K)+"D="+str(Delta)+"/Delta="+str(Delta)+"_sensor="+str(k)+"_flow.pdf"
        plt.savefig(save_plase)
        plt.close()
        # plt.show()
    return


def new_input_plot(daily_samples,type
                   ,x,K=0,Delta=0,day=0,path="1"):


    r = np.arange(start=0,stop=daily_samples)

    # size=input.shape[0]
    i=0
    for k in range(0, int(x.shape[0] / daily_samples)):
        index = np.arange(start=k * daily_samples, stop=k * daily_samples + daily_samples)
        temp=i
        label1 = "Sensor "
        label1 += str(k)

        # plt.figure(k)
        # if(type=="event"):
        plt.plot(r,x[index], '-o')
        # else:
        # plt.plot(r,x[index], 'b-o', label=label1, )
    # plt.plot(deltas, negatives, 'r-o', label=label2)
    plt.xlabel("Sample",fontsize=fontsize)
    plt.ylabel("Flow",fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize,)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()

    if not os.path.exists("plot_files/"+path+"/sensor_input/K="+str(K)+"D="+str(Delta)):
        os.makedirs("plot_files/"+path+"/sensor_input/K="+str(K)+"D="+str(Delta))
    save_plase="plot_files/"+path+"/sensor_input/"+"K="+str(K)+"D="+str(Delta)+"/Delta="+str(Delta)+"_sensor="+str(k)+"_day="+str(day)+"_"+type+"_flow.pdf"
    plt.savefig(save_plase)
    plt.close()
    # plt.show()
    return


def deltaVarianceImpactOnFlowChart(deltas=[],deltas_invalid=[],normalFlow=[],eventFlow=[],k=0,figure_n=0,path="1"):
    label1= "Normal (K="
    label1+=str(k)
    label1+=")"

    label2="Event (K="
    label2+=str(k)
    label2+=")"

    plt.figure(figure_n)
    plt.plot(deltas, normalFlow, '-go',linewidth=2,label=label1)
    plt.plot(deltas_invalid, eventFlow, '-ro',linewidth=2,label=label2)
    plt.xlabel("Delta", fontsize=fontsize)
    plt.ylabel("Average Distance" ,fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize)
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/flow_distance_different_k"):
        os.makedirs("plot_files/"+path+"/flow_distance_different_k")
    save_plase = "plot_files/"+path+"/flow_distance_different_k/K=" + str(k) + "_Flow_Distance.pdf"
    plt.savefig(save_plase)
    plt.close()
    return


def plotValidInvalidPercentage(k=0,valid=[],invalid=[],deltas=[],path="1"):

    n_groups = len(valid)

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4
    error_config = {'ecolor': '0.3'}

    rects1 = plt.bar(index, valid, bar_width,
                     alpha=opacity,
                     color='g',
                     label='Normal')

    rects2 = plt.bar(index + bar_width, invalid, bar_width,
                     alpha=opacity,
                     color='r',
                     label='Event')

    plt.xlabel('Delta')
    plt.ylabel('Input Samples (%)')
    plt.title('New samples Classification for different values of D with fixed K=' + str(k))
    plt.xticks(index + bar_width / 2, deltas)
    plt.legend()
    plt.tight_layout()
    if not os.path.exists("plot_files/"+path+"/normal_event_percentage/"):
        os.makedirs("plot_files/"+path+"/"+"/normal_event_percentage/")
    plt.savefig("plot_files/"+path+"/"+"/normal_event_percentage/"+"K="+str(k)+"valid_invalid.pdf")
    # plt.show()
    plt.close()


def plotScoresWithDifferentMeans(scores: list , deltas:list, plotName: str, mean :str):

    plt.xlabel("Delta")
    plt.ylabel(plotName+" (%)")
    plt.figure(1)
    plt.ylim((0,100))
    plt.plot(deltas, [i*100 for i in scores], 'go', linewidth=2, label="mean= "+mean)
    plt.legend()
    plt.tight_layout()
    plt.show()
