from matplotlib import pyplot as plt
import os
import pickle

def coverage(input=[],color= 'b', figure_n=0):


    """

    :type input: Array containing all instances of RMSE
    """

    step = list(range(0, len(input)))
    fontsize = 15
    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    plt.plot(input,color=color, label='Reconstruction Error')
    # plt.plot(deltas, negatives, 'r-o', label=label2)

    plt.xlabel("iteration", fontsize=fontsize)
    plt.ylabel("RMSE", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize, )
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"):
        os.makedirs("plot_files/")
    save_plase = "plot_files/Iterations-train.eps"
    plt.savefig(save_plase)
    pickle.dump(input,open("RMSEinit.p","wb"))
    plt.show()
    plt.close()


# input=pickle.load(open("RMSE.p","rb"))
# coverage(input)



def multiplecoverage(inputs=[],epoch_n="test" ,figure_n=0):


    """

    :type inputs: Array containing all instances of RMSE
    """
    fontsize = 15
    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    ax.grid(True)
    ax.set_ylim(0,200)
    start=0
    counter=1
    for inpt in inputs:
        step = list(range(start,start+len(inpt)))
        if counter==1:
            plt.axvline(x=step[-1])

        start+=len(inpt)
        if counter%2==0:
            plt.plot(step, inpt,color='gray',linewidth=2)
        else:
            plt.plot(step, inpt,color='black',linewidth=2)

        counter+=1
    # plt.plot(deltas, negatives, 'r-o', label=label2)

    plt.xlabel("iterations", fontsize=fontsize)
    plt.ylabel("RMSE", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    # plt.legend(fontsize=fontsize, )
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"):
        os.makedirs("plot_files/")
    save_plase = "plot_files/Iterations-train-"+epoch_n+"-epoch.eps"
    plt.savefig(save_plase)

    plt.close()



def lastsamplecoverage(all_inputs=[],epoch_n="test" ,figure_n=0):


    """

    :type inputs: Array containing all instances of RMSE
    """
    colors = ['red',"green", "blue"]
    fontsize = 15
    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()
    ax.grid(True)
    start=0
    counter=0
    to_plot=[]
    for inputs in all_inputs:
        for inpt in inputs[1:]:
            to_plot.append(inpt[-1])
            # step = list(range(start,start+len(inpt)))
            # if counter==1:
            #     plt.axvline(x=step[-1])

            # start+=len(inpt)
        print("in")
        if counter==0:
            plt.plot(to_plot,"-",color=colors[counter],linewidth=3,label="Offline")
        else:
            plt.plot(to_plot,"-",color=colors[counter],linewidth=3,label = "Online ("+str(counter)+" epochs)")
        counter+=1
        to_plot=[]
    # plt.plot(deltas, negatives, 'r-o', label=label2)

    plt.xlabel("Samples", fontsize=fontsize)
    plt.ylabel("RMSE", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.legend(fontsize=fontsize, )
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"):
        os.makedirs("plot_files/")
    save_plase = "plot_files/offline_online_comparison.eps"
    plt.savefig(save_plase)
    plt.show()

    plt.close()


