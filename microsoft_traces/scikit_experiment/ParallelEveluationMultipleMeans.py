import csv
import gc
import multiprocessing
import multiprocessing as mp
import sys
from time import time
import SharedArray as sa

sys.path.extend(["../matrixcompletionandprojection"])
import numpy as np
from projection_algorithm.ProjectionAlgorithm import solveSystem
import pickle
from maco_implementations.online_versoin.maco_full_parallel_python_with_updates import copy_to_shared_Array


def solveSystemParallel(R, i, validation_matrix, delta,s,q):
    # print(delta*x_max,i,sep=" ")
    R=sa.attach("shm://"+R)
    validation_matrix=sa.attach("shm://"+validation_matrix)
    value = solveSystem(h=R, b=(validation_matrix[i, :]), delta=delta,s=s)
    q.put((delta, (i, value)))
    return (delta, (i, value))



# synth=sa.attach("shm://synth")
# R=sa.attach("shm://R")
for i in sa.list():
   print(str(i[0]))
   sa.delete("shm://"+ str(i[0].decode('UTF-8')))

synth = np.load("/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/normal_matrix.npy")
event = np.load("/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/event_matrix.npy")

# R = pickle.load(open("R_K=10_S=1.p", "rb"))
#
# gc.collect()

R= np.zeros((233089,10))
with open('/home/stathis/IdeaProjects/blocked_als/data/movies10.txt/all2.csv', 'r') as csvfile:
   spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
   for row in spamreader:
       R[int(row[0]),int(row[1])]=row[2]

R=R.transpose()
print(R.shape)
r = R.shape[0]
e = 0.1
s = ((r * np.log(r)) / e) * np.log(r * np.log(r) / e)
print(s)

x_max = np.amax(synth)
synth = synth / x_max
event = event / x_max
# noisy = synth
synth= copy_to_shared_Array(synth,"synth")
R= copy_to_shared_Array(R,"R")

# noisy = noisy / x_max
daily_samples = 1
n_components = 10


percentage = 0.1
total_deltas_to_try = 101


deltas_step = 5

size_times = int(synth.shape[1] / daily_samples)

rows_to_add=()
for i in range(0,100):
    rows_to_add+=(synth[i,:].reshape(-1,1).T,)
for i in range(0,100):
    rows_to_add+=(event[i,:].reshape(-1,1).T,)
matrix_to_evaluate = np.concatenate(rows_to_add,axis=0)
m = multiprocessing.Manager()
q = m.Queue()
results = []
for d in range(0, total_deltas_to_try, deltas_step):
    delta = d / x_max
    for i in range(0, matrix_to_evaluate.shape[0]):
        results.append(("R", i, "synth", delta,s, q))
for iteration in range(0,1):

    print("Starting calculation of projections")
    t1 = time()
    pool = mp.Pool(16)
    pool.starmap(solveSystemParallel, results)
    pool.close()
    pool.join()
    d_results = dict()
    while not q.empty():
        i=q.get()
        try:
            print(i[0],i[1])
            d_results[i[0]].append(i[1])
        except KeyError:
            d_results[i[0]]=[]
            d_results[i[0]].append(i[1])
            pass

        # d_results[i[0]] = i[1]
    pickle.dump(d_results,open("d_results"+str(n_components)+".p",'wb'))
    print("Done")
    print(time()-t1)
