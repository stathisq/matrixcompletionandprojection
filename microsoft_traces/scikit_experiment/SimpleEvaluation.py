import csv

import numpy as np
import pandas as pd
from microsoft_traces.matrix_creator import heatmap_creator
from projection_algorithm.ProjectionAlgorithm import solveSystem

synth = np.load("/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/normal_matrix.npy")
synth_event = np.load("/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/event_matrix.npy")

# R = np.load("R_K=20_S=1.npy")
# R = np.genfromtxt("/home/stathis/all1_csv.csv",  dtype=float,delimiter=',')
# gc.collect()
data = pd.read_csv('/home/stathis/IdeaProjects/blocked_als/data/movies10.txt/all2.csv',header=None,delimiter=' ')
print(data[data.columns[0]].min())
R= np.zeros((233089,10))
with open('/home/stathis/IdeaProjects/blocked_als/data/movies10.txt/all2.csv', 'r') as csvfile:
   spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
   for row in spamreader:
       R[int(row[0]),int(row[1])]=row[2]

R=R.transpose()
print(R.shape)
r = R.shape[0]
e = 0.1
s = ((r * np.log(r)) / e) * np.log(r * np.log(r) / e)
print(s)
total_deltas_to_try=100
deltas_step=10

heatmap_creator(R,"R.pdf")
x_max = np.amax(synth)
print(x_max)
synth = synth / x_max



test_matrix = np.zeros((10,R.shape[1]))
noise=0
for i in range(test_matrix.shape[0]):
    test_matrix[i,:]+=noise
    noise+=10

heatmap_creator(test_matrix,"test_matrix.pdf")

for d in range(0, total_deltas_to_try, deltas_step):
    delta = d / x_max
    for i in range(0, test_matrix.shape[0]):
        r=solveSystem(h=R, b=(test_matrix[i, :]), delta=delta, s=s,disp=False)
        print("Delta: ",d," "," sample: ",i," result: ", r)