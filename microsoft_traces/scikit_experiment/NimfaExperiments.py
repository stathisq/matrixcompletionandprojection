import numpy as np
import nimfa

print("Select dimension K for the matrix completion")
dimension=int(input())

x = np.load("/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/x_half.npy")
print(x.shape)

max = np.amax(x)
x = x / max

print("factorizing")
psmf = nimfa.Psmf(x, seed=None, rank=10, max_iter=12, prior=np.random.rand(10))
psmf_fit = psmf()
print("done")