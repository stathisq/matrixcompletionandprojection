from tools import MatrixCreator
import pickle
import numpy as np


# mkl_rt = ctypes.CDLL('libmkl_rt.so')
# mkl_get_max_threads = mkl_rt.mkl_get_max_threads
# def mkl_set_num_threads(cores):
#     mkl_rt.mkl_set_num_threads(ctypes.byref(ctypes.c_int(cores)))
# np.__config__.show()
# mkl_set_num_threads(multiprocessing.cpu_count())


print("Select dimension K for the matrix completion")
dimension=int(input())

x = np.load("/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/x_half.npy")
print(x.shape)

max = np.amax(x)
x = x / max

R = MatrixCreator.scikitFactorization(x, n_components=dimension,alpha=0.000001,l1_ratio=0)
np.save("R_K="+str(dimension)+"_S=1",R)
# pickle.dump(R,open("R_K="+str(dimension)+"_S=1.p","wb"))
