import random

from matplotlib import pyplot as plt
import numpy as np
import pickle

from tqdm import tqdm

from microsoft_traces.matrix_creator import calculate_distributions, calculate_row_means
from plots.input_matrix_histogram import inputRowHistogram, inputRowsHistogram
from tools.CustomDict import CustomDict

def syth_matrix_stats():
    synth = np.load(
        "/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/normal_matrix.npy")
    event = np.load(
        "/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/files/event_matrix.npy")

    rows_to_add = ()
    for i in range(0, 100):
        rows_to_add += (synth[i, :].reshape(-1, 1).T,)
    for i in range(0, 100):
        rows_to_add += (event[i, :].reshape(-1, 1).T,)
    matrix_to_evaluate = np.concatenate(rows_to_add, axis=0)

    calculate_distributions(matrix_to_evaluate)
    calculate_row_means(matrix_to_evaluate,save=True,filename="eval")


# syth_matrix_stats()

results = pickle.load(open("/home/stathis/PycharmProjects/matrixcompletionandprojection/microsoft_traces/scikit_experiment/d_results10.p",'rb'))
d= pickle.load(open("files/matrix_distributions.p", 'rb'))
deltas = []
samples = []
positive_rows = []
negative_rows = []
delta_to_investigate=90
counter=0
for k,v in results.items():
    fig, ax = plt.subplots()

    rows_means = pickle.load(open("files/eval_rows_means.p", 'rb'))
    rows_means = sorted(rows_means, key=lambda tup: tup[1])
    seperate_means = list(zip(*rows_means))
    #
    # for row in rows_means:
    ordered_values = sorted(v, key=lambda tup: tup[0])
    seperate_values = list(zip(*ordered_values))

    normals=[]
    events = []

    for i in range(0,len(seperate_means[0])):
        if seperate_values[1][i]==0:
            normals.append(seperate_means[0][i])
            events.append(-1)
            if int(k * 100) == delta_to_investigate:
                positive_rows.append(i)
        else:
            normals.append(-1)
            events.append(seperate_means[0][i])
            if int(k * 100) == delta_to_investigate:
                negative_rows.append(i)
    normals = np.asarray(normals, dtype=np.float32).copy()
    events = np.asarray(events, dtype=np.float32).copy()

    ax.axvline(100, linewidth=2,linestyle = '--', color='blue', label='Normal-Event Seperator', zorder=2)


    normals[normals == -1] = np.nan
    events[events == -1] = np.nan
    normal_n= np.count_nonzero(~np.isnan(normals))
    event_n= np.count_nonzero(~np.isnan(events))
    ax.scatter(list(seperate_values[0]), list(events),marker='.', color='r', label='Events('+str(event_n)+" Samples)")
    ax.scatter(list(seperate_values[0]), list(normals), marker='.', color='g', label='Normal('+str(normal_n)+" Samples) ")





    plt.xlabel("Time (5 minutes intervals)")
    plt.ylabel("Cpu Usage (%)")
    # plt.xlim((0, 667))
    # plt.ylim((8, 14))
    plt.legend()
    plt.title("Delta value :"+str(k*100))
    plt.tight_layout()
    plt.savefig("./evaluation/"+str(int(k*100))+".png")
    counter+=1
    plt.show()

final_positive_dict=CustomDict()
final_negative_dict=CustomDict()
for row in tqdm(positive_rows):
    for k, v in d.items():

        if row == k:
            for k1,v1 in v.items():
                final_positive_dict.insert_value(k1,v1)
            break

for row in tqdm(negative_rows):
    for k, v in d.items():
        if row == k:
            for k1,v1 in v.items():
                final_negative_dict.insert_value(k1,v1)
            break


positive_values = []
positive_values_stds = []
negative_values = []
negative_values_stds = []

final_positive_dict.fill_with_zeros()
final_negative_dict.fill_with_zeros()


for k,v in final_positive_dict.d_results.items():
    positive_values.append(np.average(np.array(v)))
    positive_values_stds.append(np.std(np.array(v)))
print(positive_values)
print(positive_values_stds)
#
for k,v in final_negative_dict.d_results.items():
    negative_values.append(np.average(np.array(v)))
    negative_values_stds.append(np.std(np.array(v)))
print(negative_values)
print(negative_values_stds)

#
# rows_to_show =  random.sample(positive_rows, 5)
# inputRowHistogram(positive_values,positive_values_stds,color='g',label="Normal")
# inputRowHistogram(negative_values,negative_values_stds,color='r',label="Event")
inputRowsHistogram((positive_values,negative_values),(positive_values_stds,negative_values_stds),delta_value=delta_to_investigate)
# rows_to_show =  random.sample(negative_rows, 5)
# inputRowHistogram(d,keys_to_show=rows_to_show)
#
#


