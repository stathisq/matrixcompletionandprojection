import pickle

import sqlalchemy

from sqlalchemy import create_engine, and_, distinct
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Query
import numpy as np
from tqdm import tqdm
import logging

from microsoft_traces.cpu_plots import plot_Single_VM_cpu_usage, heatmap_creator

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
from alchemy_exports.models import *


def conectToDb():
    engine = create_engine("mysql://root:dsl4b-r00t@192.168.1.131:3306/AzurePublicDataset")
    Base.metadata.create_all(engine)

    session = sessionmaker(bind=engine)

    # create a Session
    s = session()
    return s


def fetchVmsFromTimestamps(timestamp=200000, filename="timestamps", output_name="final_dict_half"):
    s = conectToDb()
    timestamps = []
    for result in s.query(distinct(Vmcpu.timestamp)).filter(Vmcpu.timestamp <= timestamp).all():
        timestamps.append(result[0])
    pickle.dump(timestamps, open("files/" + filename + ".p", 'wb'))
    s.close()

    entries = dict()
    final_dict = dict()

    for t in tqdm(timestamps):
        for result in s.query(Vmcpu.vmid, Vmcpu.avg_cpu_util, Vmcpu.min_cpu_util, Vmcpu.max_cpu_util).filter(
                Vmcpu.timestamp == t).all():
            entry = dict()
            entry['avg'] = result[1]
            entry['min'] = result[2]
            entry['max'] = result[3]
            entries[result[0]] = entry

        final_dict[t] = entries
    pickle.dump(final_dict, open("files/" + output_name + ".p", 'wb'), protocol=4)
    s.close()


def vmsToMatrix(timestamp=200000, file_name="vms"):
    s = conectToDb()
    cursor = s.query(distinct(Vmcpu.vmid)).filter(Vmcpu.timestamp <= timestamp).all()
    vms = []
    for record in tqdm(cursor):
        vms.append(record[0])

    s.close()

    pickle.dump(vms, open("files/" + file_name + ".p", 'wb'))
    return vms


def fetchSortedSingleVms():
    s = conectToDb()
    cursor = s.query(Vmtable.vmid, Vmtable.vmdeleted - Vmtable.vmcreated).filter(
        Vmtable.vmdeleted - Vmtable.vmcreated > 1000).all()
    # print(cursor)
    ll = []
    for record in tqdm(cursor):
        ll.append(record)
    sorted_by_second = sorted(ll, key=lambda tup: tup[1], reverse=True)
    print(sorted_by_second[:10])
    pickle.dump(sorted_by_second, open("utilization.p", 'wb'))
    s.close()
    return sorted_by_second


def fetchSingeVMTimeseriesL(ll):
    s = conectToDb()
    cursor = s.query(Vmcpu).filter(Vmcpu.vmid == ll[0][0]).all()
    entries = []
    vector_list=[]
    for entry in cursor:
        entries.append(entry)
        vector_list.append(entry.avg_cpu_util)
        print(entry.to_dict())
    for i in range(0,18):
        vector_list.append(0)
    s.close()
    matrix = np.asarray(vector_list).reshape((int(len(vector_list)/50),50))
    heatmap_creator(matrix)
    return entries

if __name__ == "__main__":
    ll=fetchSortedSingleVms()
    ll=fetchSingeVMTimeseriesL(ll)
    plot_Single_VM_cpu_usage(ll)
    # fetchVmsFromTimestamps(400000)
    # vmsToMatrix(400000)
