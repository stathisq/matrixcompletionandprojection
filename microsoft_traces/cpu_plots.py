import pickle
from matplotlib import pyplot as plt
import numpy as np




def plot_cpu_usage_no_errorbar():
    rows_means = pickle.load(open("files/x_half_rows_means.p", 'rb'))
    seperate_means = list(zip(*rows_means))
    avg = sum(seperate_means[0]) / float(len(seperate_means[0]))

    above_avg = np.asarray(seperate_means[0]).copy()
    below_avg = above_avg.copy()

    above_avg[above_avg<=avg] = np.nan
    below_avg[below_avg>avg] = np.nan

    fig, ax = plt.subplots()


    # plt.plot(seperate_means[1],above_avg,color='g',label='CPU Average Usage(Above Avg.)',zorder=2)
    ax.scatter(seperate_means[1],above_avg,marker='.',color='g',label='CPU Max Usage(Above Avg.)',zorder=2)
    # plt.plot(seperate_means[1],below_avg,color = 'r',label='CPU Average Usage(Below Avg.)',zorder=2)
    ax.scatter(seperate_means[1],below_avg,marker='.',color = 'r',label='CPU Max Usage(Below Avg.)',zorder=2)
    ax.axhline(avg, linewidth=2,linestyle = '--', color='blue', label='CPU Average', zorder=2)
    plt.xlabel("Time (5 minutes intervals)")

    plt.ylabel("Cpu Usage (%)")
    # plt.xlim((0,1000))
    plt.legend()

    plt.tight_layout()
    plt.savefig("resource_usage_max.pdf")
    plt.show()




def plot_cpu_usage_per_MV():
    rows_means = pickle.load(open("files/matrix_columns_means.p", 'rb'))
    print(len(rows_means))
    rows_means=rows_means[rows_means>2]
    print(len(rows_means))
    avg = sum(rows_means) / float(len(rows_means))
    x_values = range(len(rows_means))
    above_avg = np.asarray(rows_means).copy()
    below_avg = above_avg.copy()

    above_avg[above_avg<=avg] = np.nan
    below_avg[below_avg>avg] = np.nan

    fig, ax = plt.subplots()


    # plt.plot(seperate_means[1],above_avg,color='g',label='CPU Average Usage(Above Avg.)',zorder=2)
    ax.scatter(x_values,above_avg,marker='.',color='g',label='CPU Max Usage(Above Avg.)',zorder=2)
    # plt.plot(seperate_means[1],below_avg,color = 'r',label='CPU Average Usage(Below Avg.)',zorder=2)
    ax.scatter(x_values,below_avg,marker='.',color = 'r',label='CPU Max Usage(Below Avg.)',zorder=2)
    ax.axhline(avg, linewidth=2,linestyle = '--', color='blue', label='CPU Average', zorder=2)
    plt.xlabel("Virtual Machine")

    plt.ylabel("Cpu Usage (%)")
    # plt.xlim((0,1000))
    plt.legend()

    plt.tight_layout()
    plt.savefig("files/vm_resource_usage_max.pdf")
    plt.show()




def plot_cpu_usage_with_errorbar():

    rows_means = pickle.load(open("rows_means.p", 'rb'))
    seperate_means = list(zip(*rows_means))

    rows_stds = pickle.load(open("rows_stds.p", 'rb'))
    seperate_stds = list(zip(*rows_stds))



    avg = sum(seperate_means[0]) / float(len(seperate_means[0]))

    above_avg = np.asarray(seperate_means[0]).copy()
    below_avg = above_avg.copy()

    above_avg[above_avg <= avg] = np.nan
    below_avg[below_avg > avg] = np.nan

    fig, ax = plt.subplots()

    # plt.plot(seperate_means[1],above_avg,color='g',label='CPU Average Usage(Above Avg.)',zorder=2)
    ax.errorbar(seperate_means[1], above_avg,seperate_means[0], marker='.', color='g', label='CPU Max Usage(Above Avg.)', zorder=2)
    # plt.plot(seperate_means[1],below_avg,color = 'r',label='CPU Average Usage(Below Avg.)',zorder=2)
    ax.scatter(seperate_means[1], below_avg, marker='.', color='r', label='CPU Max Usage(Below Avg.)', zorder=2)
    ax.axhline(avg, linewidth=2, linestyle='--', color='blue', label='CPU Average', zorder=2)
    plt.xlabel("Time (5 minutes intervals)")

    plt.ylabel("Cpu Usage (%)")
    plt.xlim((0, 1000))
    plt.legend()

    plt.tight_layout()
    plt.savefig("resource_usage_max.pdf")
    plt.show()


def plot_Single_VM_cpu_usage(values: tuple()):


    min_cpu=[]
    max_cpu=[]
    avg_cpu=[]
    for v in values:
        max_cpu.append(v.max_cpu_util)
        min_cpu.append(v.min_cpu_util)
        avg_cpu.append(v.avg_cpu_util)

    fig, ax = plt.subplots()

    plt.plot(max_cpu,color='r',label='CPU Max Usage',zorder=2)
    plt.plot(min_cpu,color='g',label='CPU Min Usage',zorder=2)
    plt.plot(avg_cpu,color='b',label='CPU Avg Usage',zorder=2)

    plt.xlabel("Time (5 minutes intervals)")

    plt.ylabel("Cpu Usage (%)")
    #plt.xlim((0, 1000))
    plt.legend()

    plt.tight_layout()
    plt.show()

def make_square_axes(ax):
    """Make an axes square in screen units.

    Should be called after plotting.
    """
    ax.set_aspect(1 / ax.get_data_ratio())

def heatmap_creator(X, filename="heatmap.pdf"):
    plt.imshow(X)
    plt.xlabel('Sensor', fontsize=15)
    plt.ylabel('Day', fontsize=15)
    make_square_axes(plt.gca())
    plt.tight_layout()
    plt.savefig(filename)
    plt.show()

if __name__ == "__main__":

    # plot_cpu_usage_per_MV()
    plot_cpu_usage_no_errorbar()
    # plot_cpu_usage_with_errorbar()