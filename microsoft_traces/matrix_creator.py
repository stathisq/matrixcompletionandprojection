import pickle
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

import mysql.connector
from tqdm import tqdm

from plots.input_matrix_histogram import inputMatrixHistogram, inputRowHistogramFromDict
from tools.matrix_tools import calculateDistribution




def vmsToMatrix(timestamps=200000, file_name="vms"):
    cnx = mysql.connector.connect(user='root', password='dsl4b-r00t',
                                  host='dslab-server1', port=3306,
                                  database='AzurePublicDataset')

    query = ("SELECT distinct (vmid) FROM vmcpu where timestamp < %s") % timestamps
    cursor = cnx.cursor(buffered=True)
    cursor.reset()
    cursor.execute(query, 'set max_allowed_packet=67108864')
    vms = []
    counter = 0
    for (record) in tqdm(cursor):
        vms.append(record[0])

    print(len(vms))
    pickle.dump(vms, open("files/" + file_name + ".p", 'wb'))
    return vms


def createMatrixTimeSeries(timestamps=200000, file_name="timestamps"):
    cnx = mysql.connector.connect(user='root', password='dsl4b-r00t',
                                  host='dslab-server1', port=3306,
                                  database='AzurePublicDataset')

    query = ("SELECT distinct(timestamp) FROM vmcpu WHERE timestamp <  %s") % (timestamps)
    cursor = cnx.cursor(buffered=True)
    cursor.reset()
    cursor.execute(query, )
    timestamps = []
    for (record) in cursor:
        timestamps.append(record[0])

    pickle.dump(timestamps, open("files/" + file_name + ".p", 'wb'))

    final_dict = dict()

    for timestamp in tqdm(timestamps):
        query = ("SELECT vmid,avg_cpu_util,min_cpu_util,max_cpu_util FROM vmcpu where timestamp='%s'" % (timestamp,))
        cursor = cnx.cursor(buffered=True)
        cursor.reset()
        cursor.execute(query, )

        entries = dict()
        for (record) in cursor:
            entry = dict()
            entry['avg'] = record[1]
            entry['min'] = record[2]
            entry['max'] = record[3]
            entries[record[0]] = entry
        final_dict[timestamp] = entries
    pickle.dump(final_dict, open("files/final_dict_half.p", 'wb'), protocol=4)


def createDatasetFromPickles(vms_name="vms_half",final_dict_name="final_dict_half",timestamps_name="timestamps",output_name="initial_array"):
    vms = pickle.load(open("files/"+vms_name+".p", 'rb'))
    final_dict = pickle.load(open("files/"+final_dict_name+".p", 'rb'))

    X = np.zeros((len(final_dict), len(vms)))
    print(X.shape)
    counter = 0
    vm_to_row = dict()
    for vm in vms:
        vm_to_row[vm] = counter
        counter += 1

    timestamps_to_row = dict()
    timestamps = pickle.load(open("files/"+timestamps_name+".p", 'rb'))
    counter = 0

    for t in timestamps:
        timestamps_to_row[t] = counter
        counter += 1

    for k, v in tqdm(final_dict.items()):
        for k1, v1 in v.items():
            X[timestamps_to_row[k], vm_to_row[k1]] = v1["max"]
    return X


def make_square_axes(ax):
    """Make an axes square in screen units.

    Should be called after plotting.
    """
    ax.set_aspect(1 / ax.get_data_ratio())


def heatmap_creator(X, filename="heatmap.pdf"):
    plt.imshow(X)
    plt.xlabel('Virtual Machine', fontsize=15)
    plt.ylabel('Time( 5 minutes intervals)', fontsize=15)
    make_square_axes(plt.gca())
    plt.tight_layout()
    plt.savefig(filename)
    plt.show()


# createMatrixTimeSeries()
# vms = vmsToMatrix()

def calculate_row_means(x,save=False,filename = "x_half"):
    rows_means = []
    rows_stds = []
    for i in range(0, x.shape[0]):
        rows_means.append((np.nanmean(x[i, :]), i))
        rows_stds.append((np.nanmean(x[i, :]), np.nanstd(x[i, :]), i))
    if save:
        pickle.dump(rows_means, open("files/"+filename +"_rows_means.p", 'wb'))
        pickle.dump(rows_stds, open("files/"+filename +"_rows_stds.p", 'wb'))

    return rows_means, rows_stds


def calculate_col_means(x, save=False,filename="x_half"):
    columns_means = []
    columns_stds = []
    for i in range(0, x.shape[1]):
        columns_means.append((np.nanmean(x[:, i]), i))
        columns_stds.append((np.nanmean(x[:, i]), np.nanstd(x[:, i]), i))
    if save:
        pickle.dump(columns_means, open("files/"+filename +"_columns_means.p", 'wb'))
        pickle.dump(columns_stds, open("files/"+filename +"_columns_stds.p", 'wb'))

    return columns_means, columns_stds


def calculate_matrix_col_stats(input_name="initial_array"):
    x = np.load("files/"+input_name+".npy")
    total_mean = np.nanmean(x, axis=0)
    pickle.dump(total_mean, open("files/matrix_columns_means.p", 'wb'))
    print(np.mean(np.array(total_mean)))
    print(np.max(np.array(total_mean)))
    print(np.min(np.array(total_mean)))


def create_all_matrices(use_nan=False, save=False, filename="x_half"):
    x = createDatasetFromPickles()
    if save:
        np.save("files/" + filename, x)
        max_v = np.amax(x)
        x = x / max_v
        f = open("files/" + filename + "_non_zeros.csv", "w")
        ltz = np.where(x > 0)
        for i in range(0, ltz[0].shape[0]):
            s = str(ltz[0][i]) + ", " + str(ltz[1][i]) + ", " + str(x[ltz[0][i], ltz[1][i]]) + "\n"
            f.write(s)
    heatmap_creator(x)

    print(x.shape)
    # x = np.genfromtxt("x_half.csv", delimiter=',')
    if use_nan:
        x[x == 0] = np.nan
    x= np.load("files/initial_array.npy")
    rows_means, rows_stds = calculate_row_means(x,save=save)

    pickle.dump(rows_means, open("files/" + filename + "_rows_means.p", 'wb'))
    pickle.dump(rows_stds, open("files/" + filename + "_rows_stds.p", 'wb'))

    columns_means, columns_stds = calculate_col_means(x)

    pickle.dump(columns_means, open("files/" + filename + "_columns_means.p", 'wb'))
    pickle.dump(columns_stds, open("files/" + filename + "_columns_stds.p", 'wb'))

    rows_to_remove = []
    for row in rows_means:
        if row[0] < 5 or row[0] > 12:
            rows_to_remove.append(row[1])

    print(len(rows_to_remove))
    x = createDatasetFromPickles()
    y = np.delete(x, rows_to_remove, axis=0)
    heatmap_creator(y, "heatmap_no_outliers")

    if save:
        np.savetxt("files/" + filename + "_no_outliers", y)
    if use_nan:
        y[y == 0] = np.nan

    rows_means, rows_stds = calculate_row_means(y)

    pickle.dump(rows_means, open("files/" + filename + "+_rows_means_y.p", 'wb'))
    pickle.dump(rows_stds, open("files/" + filename + "_rows_stds_y.p", 'wb'))


def remove_unutilzed_entries(filename="initial_array"):
    x = np.load("files/" + filename + ".npy")

    total_mean = np.nanmean(x, axis=0)
    pickle.dump(total_mean, open("files/matrix_columns_means.p", 'wb'))
    columns_means, columns_stds = calculate_col_means(x)
    cols_to_remove = []
    for col in columns_means:
        if col[0] < 5:
            cols_to_remove.append(col[1])
    print(x.shape)
    y = np.delete(x, cols_to_remove, axis=1)
    heatmap_creator(y, "heatmap_no_outliers")
    rows_means, rows_stds = calculate_row_means(y, True)

    rows_to_remove = []
    for row in rows_means:
        if row[0] < 5:
            rows_to_remove.append(row[1])

    print(len(rows_to_remove))
    np.save("files/"+filename+"_no_outliers", y)
    y = np.delete(y, rows_to_remove, axis=0)

    print(y.shape)
    np.save("files/"+filename+"_no_outliers_shrinked", y)
    # rows_means, rows_stds=  calculate_row_means(y,True)

    print(np.mean(np.array(total_mean)))
    print(np.max(np.array(total_mean)))
    print(np.min(np.array(total_mean)))


def calculate_distributions(matrix: np.ndarray):
    rows = matrix.shape[0]
    results = dict()
    for row in tqdm(range(rows)):
        results[row] = calculateDistribution(matrix[row, :])
    pickle.dump(results, open("files/matrix_distributions.p", 'wb'))
    return results


def seperateMatricesToTrainTest(matrix: np.ndarray, l_limit=95):
    rows = ()
    ll=[]

    for i in range(matrix.shape[0]):
        ll.append(len(np.where(matrix[i, :] >= l_limit)[0]))

    average = sum(ll) / float(len(ll))
    for i in range(matrix.shape[0]):
        if len(np.where(matrix[i, :]>=l_limit)[0]) >= average:
            rows += (matrix[i, :].reshape(-1, 1).T,)

    event_matrix = np.concatenate(rows, axis=0)



    np.save("files/event_matrix", event_matrix)
    rows=()


    for i in range(matrix.shape[0]):
        if len(np.where(matrix[i, :]>=l_limit)[0])<average:
            rows += (matrix[i, :].reshape(-1, 1).T,)
    normal_matrix = np.concatenate(rows,axis=0)
    np.save("files/normal_matrix", normal_matrix)
    return normal_matrix, event_matrix

def matrixToCsv(x,filename=""):
    max_v = np.amax(x)
    x = x / max_v
    f = open("files/" + filename + "_non_zeros.csv", "w")
    ltz = np.where(x > 0)
    for i in range(0, ltz[0].shape[0]):
        s = str(ltz[0][i]) + ", " + str(ltz[1][i]) + ", " + str(x[ltz[0][i], ltz[1][i]]) + "\n"
        f.write(s)
    f.close()


# d= calculate_distributions(np.load("files/x_half.npy").round(2))
# d= pickle.load(open("files/matrix_distributions.p", 'rb'))
# createMatrixTimeSeries()
# inputRowHistogramFromDict(d, keys_to_show=(0, 100, 200, 3))
# vmsToMatrix()
# create_all_matrices(use_nan=True, save=True,filename='x_max')
#
#
# y=np.load("files/x_avg.npy")
# #
# normal,event= seperateMatricesToTrainTest(y)
# matrixToCsv(normal,"normal")
# matrixToCsv(event,"event")
#
# calculate_matrix_col_stats()
