import sys
sys.path.extend(["../matrixcompletionandprojection"])
from time import time

import numpy as np
from scipy import linalg

# from plots.DeltaVariance import deltaVarianceChart,deltaVarianceImpactOnFlowChart
from maco_implementations.old_implementations.maco_simple import sparsityMask
from maco_implementations.old_implementations.maco_simple_no_iter_calc import maco
from plots.DeltaVariance import deltaVarianceChart, deltaVarianceImpactOnFlowChart, plotValidInvalidPercentage, \
    new_input_whole_sensor_data_plot, new_input_plot
from plots.Recall_Precision_F1Measure import recallPlot, precisionPlot, f1ScorePlot
from plots.distance_plots import DistanceChart, plotStandardDeviation, avgDistanceChart
# a = np.zeros((2, 512*512), dtype=np.float32)
# a = np.array([[1,2,3,4,5,6,7,8,9],
#               [5,43,5,6,23,235,2,124,13]])
#
# c = np.std(a, axis=1)
from plots.input_matrix_histogram import inputMatrixHistogram
from projection_algorithm import ProjectionAlgorithm
from tools import MatrixCreator
from tools.matrix_tools import calculateDistribution

daily_samples = 1
delete_range = 30
components_list = [20, 30, 50]

for n_components in components_list:

    # Create Synthetic Matrix and add Noise
    inp ,noisy = MatrixCreator.createSynthMatrix(input_matrix="timeSeriesSingleSamplesFull.csv", size=100, Delta=5, mean=50)
    # np.savetxt("inp.csv", inp, delimiter=",")
    # np.savetxt("noisy.csv", noisy, delimiter=",")


    # inp, noisy, x, X_u, h, R = MatrixCreator.restoreSynthMatrices()

    # d=calculateDistribution(inp.astype(int))
    # print(calculateDistribution(inp.astype(int)))
    inputMatrixHistogram(calculateDistribution(inp.astype(int)),calculateDistribution(noisy.astype(int)))
    # inputMatrixHistogram(calculateDistribution(noisy.astype(int)))
    # print(calculateDistribution(noisy.astype(int)))
    exit(3)
    max = np.amax(inp)
    print("Max Value", max)

    #################################
    # Normalize input matrices
    inp = inp / max
    noisy = noisy / max
    #################################

    #
    # #Sparsity Mask Init Init
    x, X_u = sparsityMask(input=inp, upper_bounds=1, percentance=30)
    np.savetxt("inpx.csv", x, delimiter=",")
    np.savetxt("X_u.csv", X_u, delimiter=",")
    #

    # X_l init
    X_l = np.zeros(x.shape)

    h, R = maco(x, X_u, X_l, m=0.001, iterations=50, dimension=n_components,epoch_n=100)
    # idx = np.random.randint(100, size=1
    # remained=x.shape[0]-delete_range

    # Delta calculation as described in the Draft

    max_value = np.amax(x[2, :])
    min_value = np.amin(x[2, :
                        ])
    delta = max_value - min_value
    print(max_value)
    print('Delta: ', delta)
    # sample = x[idx, :]

    # Arrays for varius statistics
    sensor_avg = []
    sensor_max = []
    sensor_min = []
    samples_Standard_deviation = []
    samples_Mean_deviation = []

    # initialization of statistic arrays
    for i in range(0, int(x.shape[1])):
        # index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
        sensor_max.append(np.amax(x[:, i]))
        sensor_min.append(np.amin(x[:, i]))
        sensor_avg.append(np.average(x[:, i]))
        samples_Standard_deviation.append(np.std(x[:, i]))
        samples_Mean_deviation.append(np.mean(x[:, i]))
        # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

    deltas_valid = []
    deltas_invalid = []
    deltas_total = []

    positive = []
    negative = []
    normal_flow = []
    event_flow = []
    plot_valid_percentage = []
    plot_invalid_percentage = []

    # iteration parameters
    step_n = 10
    number_of_samples = x.shape[0] / step_n

    validation_matrix = MatrixCreator.createVadilationMatrix(inp, noisy, 0.75, 0.020)
    negative_start = validation_matrix.shape[0] * 0.75

    recall_list=[]
    precision_list=[]
    f1_score_list=[]
    delta_list=[]
    # Iteration through many \Delta values
    for d in range(0, 70):
        true_positive = []
        true_negative = []
        false_positive = []
        false_negative = []
        print("Non normalized Delta: ", d)
        plot_delta = delta
        delta = d / max

        t0 = time()
        labels = []

        counter_valid = 0
        total_avg_distance_valid = 0

        # total_avg_valid=[float]*int(x.shape[1]/daily_samples)
        total_avg_valid = np.zeros([int(x.shape[1] / daily_samples)])
        counter_invalid = 0
        total_avg_distance_invalid = 0
        # total_avg_invalid=[float]*int(x.shape[1]/daily_samples)
        total_avg_invalid = np.zeros([int(x.shape[1] / daily_samples)])

        all_days = []
        worst_days = []
        m = np.mean(x, axis=0)
        for i in range(0, x.shape[0]):
            all_days.append(i)
            worst_days.append(linalg.norm(x[i, :] - m))
            # print(linalg.norm(x[i,:] - m))
        s = sorted(zip(worst_days, all_days), reverse=True)

        # print(s[:30])
        remained = 30

        print("Total Samples", validation_matrix.shape[0])
        print("Negative Start", negative_start)
        # Iteration through Matrices samples
        for l in range(0, int(validation_matrix.shape[0])):

            i = l
            normal_total = np.zeros([int(x.shape[1] ), 0])
            event_total = np.zeros([int(x.shape[1] ), 0])
            avg = np.zeros([int(x.shape[1])])
            avg_distance = 0
            for k in range(0, int(x.shape[1] )):
                # index = np.arange(start=k * daily_samples, stop=k * daily_samples + daily_samples)
                avg_distance += abs(sensor_avg[k] - np.average(x[i, k]))
                avg[k] = (np.average(x[i, k]))
            value = ProjectionAlgorithm.solveSystem(h=h, b=(validation_matrix[i, :]), delta=delta)
            if value == 0:
                if i < negative_start:
                    true_positive.append(1)
                if i > negative_start:
                    false_positive.append(1)
                total_avg_distance_valid += avg_distance / (x.shape[1])
                total_avg_valid += avg
                counter_valid += 1
                normal_total = np.concatenate([normal_total, avg.reshape((avg.shape[0], 1))], axis=1)
                new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="normal", Delta=delta,
                               day=i)
                labels.append(0)
                daily_avg = []
                sensor_id = []
                for y in range(0, int(x.shape[1])):
                    temp1 = x[i, (y):(y+1)]
                    temp2 = np.average(temp1)
                    sensor_id.append(y)
                    daily_avg.append(temp2)
                DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                              k=n_components, delta=delta, type="normal")
            if value == 2:

                if i > negative_start:
                    true_negative.append(1)
                if i < negative_start:
                    false_negative.append(1)

                total_avg_distance_invalid += avg_distance / (x.shape[1] / daily_samples)
                total_avg_invalid += avg
                event_total = np.concatenate([event_total, avg.reshape((avg.shape[0], 1))], axis=1)

                sensor_id = []
                daily_avg = []
                for y in range(0, int(x.shape[1])):
                    labels.append(1)
                    temp1 = x[i, (y):(y+1)]
                    temp2 = np.average(temp1)
                    sensor_id.append(y)
                    daily_avg.append(temp2)
                DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                              k=n_components, delta=delta, type="event")
                # print(avg/19)
                labels.append(1)
                new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="event", Delta=delta, day=i)
                counter_invalid += 1
                # print("EVENT DAY:")
                # distance = sorted(zip(daily_avg, sensor_id), reverse=True)[:10]
                # print(distance)
            # DistanceChart(avgFlow=sensor_avg, normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components)
            plotStandardDeviation(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg,
                                  normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components,
                                  type="Standard", delta=delta)
            plotStandardDeviation(deviationMatrix=samples_Mean_deviation, avgFlow=sensor_avg, normalFlow=normal_total,
                                  eventFlow=event_total, day=i, k=n_components, type="Mean", delta=delta)
        print("--------------------------------")
        print("Delta: ", delta)
        print("-----")
        print("Normal: ", (counter_valid / (counter_invalid + counter_valid)) * 100, "%")
        plot_valid_percentage.append(counter_valid / (x.shape[0] / step_n) * 100)
        if counter_valid != 0:
            print("Distance from average: ", total_avg_distance_valid / counter_valid)
        else:
            print("Distance from average: Not Found")

        print("-----")
        print("Event : ", (counter_invalid / (counter_invalid + counter_valid)) * 100, "%")
        plot_invalid_percentage.append((counter_invalid / (x.shape[0] / step_n)) * 100)
        if counter_invalid != 0:
            print("Distance from average: ", total_avg_distance_invalid / counter_invalid)
        else:
            print("Distance from average:Not Found ", )

        print("--------------------------------")
        positive.append((counter_valid / (x.shape[0] / step_n)) * 100)
        negative.append((counter_invalid / (x.shape[0] / step_n)) * 100)
        deltas_total.append(delta)
        if counter_valid != 0:
            normal_flow.append(total_avg_distance_valid / counter_valid)
            # total_avg_valid = [x / counter_valid for x in total_avg_valid]
            total_avg_valid = total_avg_valid / counter_valid
            deltas_valid.append(delta)
        if counter_invalid != 0:
            event_flow.append(total_avg_distance_invalid / counter_invalid)
            deltas_invalid.append(delta)
            total_avg_invalid = total_avg_invalid / counter_invalid
            # total_avg_invalid = [x / counter_invalid for x in total_avg_invalid]

        avgDistanceChart(avgFlow=sensor_avg, normalFlow=total_avg_valid, eventFlow=total_avg_invalid, delta=delta,
                         k=n_components)
        new_input_whole_sensor_data_plot(daily_samples=daily_samples, x=x, K=n_components, Delta=delta,
                                         remained=remained, labels=s)
        print("Test done in %0.3fs." % (time() - t0))
        print("True positives", len(true_positive))
        print("True negatives", len(true_negative))
        print("False positives", len(false_positive))
        print("False negatives", len(false_negative))
        if len(true_positive)>0 or len(false_positive)>0:
            precision = len(true_positive) / (len(true_positive) + len(false_positive))
        else:
            precision=0
        if len(true_positive)>0 or len(false_negative)>0:
            recall = len(true_positive) / (len(true_positive) + len(false_negative))
        else:
            recall=0
        # F-Measure
        # b=0.1
        # f_measure = (1+b*b)*((precision*recall)/(b*b*precision+recall))

        if precision>0 or recall>0:
            f1_score = 2 * (precision * recall) / (precision + recall)
        else:
            f1_score=0
        print("Precision:", precision)
        print("Recall:", recall)
        print("F1 Score:", f1_score)
        recall_list.append(recall)
        precision_list.append(precision)
        f1_score_list.append(f1_score)
        delta_list.append(d)
    plotValidInvalidPercentage(k=n_components, valid=plot_valid_percentage, invalid=plot_invalid_percentage,
                               deltas=deltas_total)
    deltaVarianceChart(deltas_total, positive, negative, k=n_components)
    deltaVarianceImpactOnFlowChart(deltas_valid, deltas_invalid, normal_flow, event_flow, k=n_components)
    recallPlot(recall_list,delta_list)
    precisionPlot(precision_list,delta_list)
    f1ScorePlot(f1_score_list,delta_list)
    deltas_valid = []
    deltas_invalid = []
    deltas_total = []

    positive = []
    negative = []
    normal_flow = []
    event_flow = []
    plot_valid_percentage = []
    plot_invalid_percentage = []


    # iteration parameters
    # step_n = 10
    # number_of_samples = x.shape[0] / step_n

    input("Press Enter to continue...")
    #
    #
    #
    #
    #
    #     for d in range(0, 10):
    #         print("Non normalized Delta: ", d)
    #         delta=d/10
    #         t0 = time()
    #         labels = []
    #
    #         counter_valid = 0
    #         total_avg_distance_valid = 0
    #
    #         # total_avg_valid=[float]*int(x.shape[1]/daily_samples)
    #         total_avg_valid=np.zeros([int(x.shape[1]/daily_samples)])
    #         counter_invalid = 0
    #         total_avg_distance_invalid = 0
    #         # total_avg_invalid=[float]*int(x.shape[1]/daily_samples)
    #         total_avg_invalid=np.zeros([int(x.shape[1]/daily_samples)])
    #
    # ######################################
    #         #find worst or best days (distance from input mean)
    #         all_days=[]
    #         worst_days=[]
    #         m=np.mean(x,axis=0)
    #         for i in range(0,x.shape[0]):
    #             all_days.append(i)
    #             worst_days.append(linalg.norm(x[i,:] - m))
    #             # print(linalg.norm(x[i,:] - m))
    #         s=sorted(zip(worst_days,all_days),reverse=True)
    #
    #         print(s[:30])
    #         remained=30
    # ###############################################
    #         for l in range(0,int(x.shape[0]),step_n):
    #
    #             i = l
    #             normal_total = np.zeros([int(x.shape[1] / daily_samples), 0])
    #             event_total = np.zeros([int(x.shape[1] / daily_samples), 0])
    #             avg = np.zeros([int(x.shape[1] / daily_samples)])
    #             avg_distance = 0
    #             for k in range(0, int(x.shape[1] / daily_samples)):
    #                 index = np.arange(start=k * daily_samples, stop=k * daily_samples + daily_samples)
    #                 avg_distance += abs(sensor_avg[k] - np.average(x[i, index]))
    #                 avg[k] = (np.average(x[i, index]))
    #             value = ProjectionTestNew.solveSystem(h=h, b=(x[i, :]), delta=delta)
    #             if value == 0:
    #                 total_avg_distance_valid += avg_distance / (x.shape[1] / daily_samples)
    #                 total_avg_valid += avg
    #                 counter_valid += 1
    #                 normal_total = np.concatenate([normal_total, avg.reshape((avg.shape[0], 1))], axis=1)
    #                 new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="normal", Delta=delta, day=i)
    #                 labels.append(0)
    #                 daily_avg = []
    #                 sensor_id = []
    #                 for y in range(0, int(x.shape[1] / daily_samples)):
    #                     temp1 = x[i, (30 * y):(y * 30 + 30)]
    #                     temp2 = np.average(temp1)
    #                     sensor_id.append(y)
    #                     daily_avg.append(temp2)
    #                 DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
    #                               k=n_components, delta=delta, type="normal")
    #                 distance = sorted(zip(daily_avg, sensor_id))[:10]
    #                 # print(distance)
    #             if value == 2:
    #                 total_avg_distance_invalid += avg_distance / (x.shape[1] / daily_samples)
    #                 total_avg_invalid += avg
    #                 event_total = np.concatenate([event_total, avg.reshape((avg.shape[0], 1))], axis=1)
    #
    #                 sensor_id = []
    #                 daily_avg = []
    #                 for y in range(0, int(x.shape[1] / daily_samples)):
    #                     labels.append(1)
    #                     temp1 = x[i, (30 * y):(y * 30 + 30)]
    #                     temp2 = np.average(temp1)
    #                     sensor_id.append(y)
    #                     daily_avg.append(temp2)
    #                 DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
    #                               k=n_components, delta=delta, type="event")
    #                 # print(avg/19)
    #                 labels.append(1)
    #                 new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="event", Delta=delta, day=i)
    #                 counter_invalid += 1
    #                 # print("EVENT DAY:")
    #                 # distance = sorted(zip(daily_avg, sensor_id), reverse=True)[:10]
    #                 # print(distance)
    #             # DistanceChart(avgFlow=sensor_avg, normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components)
    #             plotStandardDeviation(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg, normalFlow=normal_total,
    #                                   eventFlow=event_total, day=i, k=n_components, type="Standard", delta=delta)
    #             plotStandardDeviation(deviationMatrix=samples_Mean_deviation, avgFlow=sensor_avg, normalFlow=normal_total,
    #                                   eventFlow=event_total, day=i, k=n_components, type="Mean", delta=delta)
    #         print("--------------------------------")
    #         print("Delta: ", delta)
    #         print("-----")
    #         print("Normal: ", (counter_valid / (x.shape[0]/step_n)) * 100, "%")
    #         plot_valid_percentage.append(counter_valid / (delete_range) * 100)
    #         if counter_valid != 0:
    #             print("Distance from average: ", total_avg_distance_valid / counter_valid)
    #         else:
    #             print("Distance from average: Not Found")
    #
    #         print("-----")
    #         print("Event : ", (counter_invalid / (x.shape[0]/step_n)) * 100, "%")
    #         plot_invalid_percentage.append((counter_invalid / (x.shape[0]/step_n)) * 100)
    #         if counter_invalid != 0:
    #             print("Distance from average: ", total_avg_distance_invalid / counter_invalid)
    #         else:
    #             print("Distance from average:Not Found ", )
    #
    #         print("--------------------------------")
    #         positive.append((counter_valid / (x.shape[0]/step_n)) * 100)
    #         negative.append((counter_invalid / (x.shape[0]/step_n)) * 100)
    #         deltas_total.append(delta)
    #         if counter_valid != 0:
    #             normal_flow.append(total_avg_distance_valid / counter_valid)
    #             # total_avg_valid = [x / counter_valid for x in total_avg_valid]
    #             total_avg_valid = total_avg_valid / counter_valid
    #             deltas_valid.append(delta)
    #         if counter_invalid != 0:
    #             event_flow.append(total_avg_distance_invalid / counter_invalid)
    #             deltas_invalid.append(delta)
    #             total_avg_invalid = total_avg_invalid / counter_invalid
    #             # total_avg_invalid = [x / counter_invalid for x in total_avg_invalid]
    #
    #         avgDistanceChart(avgFlow=sensor_avg, normalFlow=total_avg_valid, eventFlow=total_avg_invalid, delta=delta,
    #                          k=n_components)
    #         new_input_whole_sensor_data_plot(daily_samples=daily_samples, x=x, K=n_components, Delta=delta, remained=remained,
    #                                          labels=s)
    #         print("Test done in %0.3fs." % (time() - t0))
    #
    #     plotValidInvalidPercentage(k=n_components, valid=plot_valid_percentage, invalid=plot_invalid_percentage,
    #                                deltas=deltas_total)
    #     deltaVarianceChart(deltas_total, positive, negative, k=n_components)
    #     deltaVarianceImpactOnFlowChart(deltas_valid, deltas_invalid, normal_flow, event_flow, k=n_components)
