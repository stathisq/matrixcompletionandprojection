import pickle
import numpy as np

from plots.Recall_Precision_F1Measure import recallPlot, precisionPlot, f1ScorePlot, recallPrecisionF1ScorePlot, \
    plotWithErrorBar


def average(l):
    llen = len(l)

    def divide(x): return x / llen

    return map(divide, map(sum, zip(*l)))


def minl(l):
    llen = len(l)
    mlen = len(l[0])
    result = []
    for j in range(0, mlen):
        temp = []
        for i in range(0, llen):
            temp.append(l[i][j])
        result.append(min(temp))
    return result


def maxl(l):
    llen = len(l)
    mlen = len(l[0])
    result = []
    for j in range(0, mlen):
        temp = []
        for i in range(0, llen):
            temp.append(l[i][j])
        result.append(max(temp))
    return result


deltas = pickle.load(open("delta_list1.p", "rb"))
recalls=[]
precisions=[]
f1_scores=[]

for i in range(0,20):
    recalls.append(pickle.load(open("recall_list"+str(i)+".p", "rb")))
    precisions.append(pickle.load(open("precision_list"+str(i)+".p", "rb")))
    f1_scores.append(pickle.load(open("f1_score_list"+str(i)+".p", "rb")))



recalls_mean = np.mean(recalls, axis=0)
recalls_std = np.std(recalls, axis=0)

plotWithErrorBar(recalls_mean, recalls_std, deltas,label="Recall (With Standard Deviation)",fmt='-g*',fname="recall_error",yname="Recall(%)", path=".")
precisions_mean = np.mean(precisions, axis=0)
precisions_std = np.std(precisions, axis=0)

precisionPlot(precisions_mean, deltas, path=".")
plotWithErrorBar(precisions_mean, precisions_std, deltas,ecolor='g', label="Precision (With Standard Deviation)", fmt='-r*', fname="precision_error", yname="Precision(%)", path=".")


f1_scores_mean = np.mean(f1_scores, axis=0)
f1_scores_std = np.std(f1_scores, axis=0)

f1ScorePlot(f1_scores_mean, deltas, path=".")
plotWithErrorBar(f1_scores_mean, f1_scores_std, deltas, label="F1 Score (With Standard Deviation)", fmt='-b*', fname="f1_score_error", yname="F1 Score(%)", path=".")

recallPrecisionF1ScorePlot(recalls_mean, precisions_mean, f1_scores_mean, deltas, path="")
