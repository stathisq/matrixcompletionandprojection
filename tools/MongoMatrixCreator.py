import json

import pymongo
import numpy as np
from sshtunnel import SSHTunnelForwarder
import pickle
from datetime import datetime, timedelta, timezone


class MongoMatrixCreator:

    """
        The class that will have all the necessary attributes for connecting
        with the Mongo database and the collections that are used by the the multiple frameworks.
        """

    def __init__(self, f):
        """
        The constructor that will initialize the class from a dictionary.
        :param parameters_dictionary: the parameters' dictionary.
        """
        j= open(f)

        parameters_dictionary=json.load(j)
        print(parameters_dictionary)
        self.server = SSHTunnelForwarder(
            parameters_dictionary['ssh_address'],
            ssh_username=parameters_dictionary['ssh_username'],
            ssh_password=parameters_dictionary['ssh_password'],
            remote_bind_address=('127.0.0.1', 27017)
        )
        self.server.start()

        self.client = pymongo.MongoClient(host=parameters_dictionary['hostname'], port=self.server.local_bind_port,
                                          username="nikolas", password="busISA")
        self.mongo_DB = self.client[parameters_dictionary['database']]
        self.mongo_collection = self.mongo_DB[parameters_dictionary['collection_name']]
        print("Connected")
    def get_find_cursor(self, query):
        """
        The function that will be used for issuing a query and retrieving the results.
        :param query: the query to utilize.
        :return: the results.
        """
        return self.mongo_collection.find(query)


    def get_aggregate_cursor(self, query):
        """
        The function that will be used for issuing a query and retrieving the results.
        :param query: the query to utilize.
        :return: the results.
        """
        return self.mongo_collection.aggregate(query)


    def creator(self,ids,interval=2,start_time="01 01 2018 UTC",end_time="11 30 2018 UTC",start_hour=7,end_hour=10):
        start_date = datetime.strptime(start_time, '%m %d %Y %Z').replace(tzinfo=timezone.utc)
        end_date = datetime.strptime(end_time, '%m %d %Y %Z').replace(tzinfo=timezone.utc)
        print(start_date.timestamp())
        matrices=dict()
        days=(end_date.timestamp()-start_date.timestamp())/86400
        samples_per_day= int((end_hour-start_hour)*60/interval)



        print("Samples per day:", samples_per_day)
        for id in ids:
            matrices[id] = np.zeros((int(days),samples_per_day))
        matrices["shape"]= (int(days),samples_per_day)



        row = 0
        for day in range(int(start_date.timestamp())*1000, int(end_date.timestamp()*1000),86400000):
            actual = day+(3600000*start_hour)
            s = 0
            for r in range(0,samples_per_day):
                t1=actual+(r*interval)*60000
                # print(t1)
                t2=t1+interval*60000
                # print(t2)
                q=dict()
                inner=dict()
                inner["$gte"]=t1
                inner["$lt"]=t2
                q["timestamp"]=inner

                for cursor in self.get_find_cursor(q):
                    # print(cursor['site'],cursor['detectorIndex'],cursor['flow'])
                    id=(cursor['site_number'],cursor['detectorID'])
                    if id in ids:
                        matrices[id][row,r]=cursor['measured_flow']
                        s+=1
            print("Day: ",row, "/",int(days)," Samples: ", s)
            row+=1
        self.server.stop()
        pickle.dump(matrices,open("matrices_2017"+str(interval)+".p","wb"))
        return

    def pyphonFilter(self):
        i=0
        output = set()
        d=dict()
        c=self.get_find_cursor(d).batch_size(10000000)
            # .sort("{\"timestamp\" : -1 }")
        count=c.count()
        for cursor in c:
            t= (cursor['site_number'],cursor['detectorID'])
            output.add(t)
            print(i,"/", count,len(output))
            i+=1
            if len(output)==332:
                break


        c.close()
        print("done, Picking")
        pickle.dump(output, open("ids.p", "wb"))
        return output
        #
        # exit(1)


    def mongoFilter(self):
        i=0
        output = []
        # d=dict()
        dict={"$group": { "_id": { "site_number": "$site_number", "detectorID": "$detectorID" } } }
        c=self.get_aggregate_cursor([dict])
        for cursor in c:
            output.append(cursor)



        c.close()
        print("done, Picking")
        pickle.dump(output, open("idsmongo.p", "wb"))
        self.server.stop()
        return output
        #
        # exit(1)
c= MongoMatrixCreator("mongo_config.json")
c.pyphonFilter()
# full = pickle.load(open("idsmongo.p", "rb"))
# print(len(full))
# d=dict()
# d["id"]=full[0]["_id"]
# print(d["id"])
ids=pickle.load(open("ids.p","rb"))
c.creator(ids,interval=2)
#
