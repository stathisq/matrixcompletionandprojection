import datetime
import random

import mysql.connector
from mysql.connector import errorcode
import numpy as np
from sklearn.decomposition import NMF
from time import time
from scipy import linalg as lg
import pickle
from maco_implementations.online_versoin.maco_full_parallel_python_with_updates import maco
from tools.Tools import sparsityMask, L_ucalc


def createMatrixMultipleSegments(n_components):
    model = NMF(n_components=n_components, init='random', random_state=0, max_iter=10000, shuffle=True)
    print("Initializing Matrix")
    # segments=[199,200, 201, 202, 203, 204, 205, 206,207,208,209]
    my_columns = list(map(lambda line: line.split(sep=',')[0],
                          open(file='/home/stathis/datasets/SCATS-Dublin/CCITY_20120101.acc_out.gps')))
    segments = list(set(my_columns))
    print(len(segments))
    # segments=[80,500,356,755,843,652,646,157,1162,667,57,88,695,669,1149,237,464,1279,1360]

    actual_segments = segments[:15]
    t0 = time()
    try:
        cnx = mysql.connector.connect(user='root', password='dsl4b-j0hn',
                                      host='195.251.252.151',
                                      database='DublinSCATS')
        counter = 0
        segment_row = 0
        x = np.zeros([3660, len(actual_segments)])

        for segment in actual_segments:
            print(segment)
            cursor = cnx.cursor(buffered=True)
            query = (
                    "SELECT t1.flow,DAY(from_unixtime(t1.upperTime)),MONTH(from_unixtime(t1.upperTime)), hour(from_unixtime(t1.upperTime)), minute(from_unixtime(t1.upperTime))"
                    " FROM DublinSCATS.road_fragments as t1 inner join ( SELECT count(upperTime) as c,DAY(from_unixtime(upperTime)) as t2day, month(from_unixtime(upperTime)) as t2month "
                    "FROM DublinSCATS.road_fragments where streetSegId='%s'  group by month(from_unixtime(upperTime)),DAY(from_unixtime(upperTime))) as t2 on DAY(from_unixtime(t1.upperTime))=t2.t2day "
                    "and MONTH(from_unixtime(t1.upperTime))=t2.t2month and streetSegId='%s' and hour(from_unixtime(t1.upperTime))<10 and hour(from_unixtime(t1.upperTime))>6;" % (
                    segment, segment))
            cursor.reset()
            cursor.execute(query, )

            found = False
            for (record) in cursor:
                x[counter, segment_row] = record[0]
                counter += 1
            segment_row += 1
            counter = 0

        print("Matrix Creating done in %0.3fs." % (time() - t0))

        print("Factorizing")
        t0 = time()
        np.savetxt("x.csv", x, delimiter=",")
        x = np.delete(x, x[x.shape[0] - 1, :], axis=0)
        model.fit(x)
        print("done in %0.3fs." % (time() - t0))

        print(model.reconstruction_err_)

        # fact=fi.MatrixFactorization.complete()

        cnx.close()
        np.savetxt("InputFull.csv", model.components_, delimiter=",")


    except Exception as e:

        print(e)

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err, "vvla")
    else:
        cnx.close()

    return [x, model.components_]


def createMatrixMultipleSegmentsCsv(n_components=20, input_matrix="x.csv", delete_range=1000, iterations=1, m=0.0):
    # model = NMF(n_components=n_components, init='random',solver='cd', random_state=0, max_iter=100000000)
    print("Initializing Matrix")
    print(iterations)
    print("Factorizing")
    t0 = time()
    x = np.genfromtxt(input_matrix, delimiter=',')
    full_matrix = x

    # for i in range(1,delete_range):
    #     x = np.delete(x, [x.shape[0]-1], axis=0)

    print(x.shape)
    # max = np.amax(x)
    max = np.amax(x)
    print(max)
    min = np.amin(x)
    print(min)
    x = x / max
    # max = np.amax(x)
    # print(max)
    # min = np.amin(x)
    # print(min)
    # y,X_u=sparsityMask(input=x,upper_bounds=1,percentance=30)
    X_u = L_ucalc(input=x)
    # X_u = np.zeros(x.shape)
    # X_u = X_u + 1
    X_l = np.zeros(x.shape)
    L, R = maco(x, X_u, X_l, m=m, dimension=n_components)

    # x = np.delete(x, x[x.shape[0] - 1, :], axis=0)
    # x = np.delete(x, x[x.shape[0] - 1, :], axis=0)

    # model.fit(x)
    print("done in %0.3fs." % (time() - t0))

    # print("Error in reconstruction",model.reconstruction_err_)

    # fact=fi.MatrixFactorization.complete()

    return [x, L, R, max]


def scikitFactorization(input_x, n_components=20, alpha=0, l1_ratio=0):
    model = NMF(n_components=n_components, init='nndsvd', solver='cd', shuffle=True, random_state=0, alpha=alpha,
                l1_ratio=l1_ratio, max_iter=1000000000)
    print("Factorizing")
    t0 = time()
    model.fit(input_x)
    print("done in %0.3fs." % (time() - t0))
    print(model.reconstruction_err_)
    print(model.n_iter_)
    return model.components_


def createMatrixTimeSeries(n_components=20):
    model = NMF(n_components=n_components, init='random', random_state=0, max_iter=10000)
    print("Initializing Matrix")
    t0 = time()
    try:
        cnx = mysql.connector.connect(user='root', password='dsl4b-j0hn',
                                      host='195.251.252.151',
                                      database='DublinSCATS')

        my_columns = list(map(lambda line: line.split(sep=',')[0],
                              open(file='/home/stathis/datasets/SCATS-Dublin/CCITY_20120101.acc_out.gps')))
        segments = list(set(my_columns))
        print(len(segments))
        x = np.zeros([122, 0])

        for segment in segments:
            cursor = cnx.cursor(buffered=True)
            query = (
                    "SELECT t1.flow,DAY(from_unixtime(t1.upperTime)),MONTH(from_unixtime(t1.upperTime)), hour(from_unixtime(t1.upperTime)), minute(from_unixtime(t1.upperTime))"
                    " FROM DublinSCATS.road_fragments as t1 inner join ( SELECT count(upperTime) as c,DAY(from_unixtime(upperTime)) as t2day, month(from_unixtime(upperTime)) as t2month "
                    "FROM DublinSCATS.road_fragments where streetSegId='%s'  group by month(from_unixtime(upperTime)),DAY(from_unixtime(upperTime))) as t2 on DAY(from_unixtime(t1.upperTime))=t2.t2day "
                    "and MONTH(from_unixtime(t1.upperTime))=t2.t2month and streetSegId='%s' and hour(from_unixtime("
                    "t1.upperTime))<10 and hour(from_unixtime(t1.upperTime))>6;" % (
                        segment, segment))
            cursor.reset()
            cursor.execute(query, )

            temp = np.zeros([122, 30])

            day_counter = 0
            sample_counter = 0
            for (record) in cursor:
                temp[day_counter, sample_counter] = record[0]
                sample_counter += 1
                if sample_counter == 30:
                    day_counter += 1
                    sample_counter = 0

            x = np.concatenate([x, temp], axis=1)
        print("Matrix Creating done in %0.3fs." % (time() - t0))

        print("Factorizing")
        t0 = time()

        # x=np.delete(x, x[x.shape[0] - 1, :],axis=0)
        model.fit(x)
        print("done in %0.3fs." % (time() - t0))

        print(model.reconstruction_err_)

        # fact=fi.MatrixFactorization.complete()

        cnx.close()
        np.savetxt("timeSeriesFull.csv", x, delimiter=",")
        # np.savetxt("Input.csv", model.components_, delimiter=",")




    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        cnx.close()

    return [x, model.components_]


def createSynthMatrixKeepZeros(input_matrix="", Delta=5, size=1000, mean=50,sigma=1, sensors_to_keep=100):
    x = np.genfromtxt(input_matrix, delimiter=',')
    if (sensors_to_keep > 0):
        new_size = 90 * sensors_to_keep
        x = x[:, :new_size]
    synth = np.zeros((0, x.shape[1]))
    noisy = np.zeros((0, x.shape[1]))
    randoms = []
    for i in range(0, size):
        randoms.append(random.uniform(0.01, 2))
    for r in randoms:
        i = random.randrange(0, x.shape[0])
        temp = x[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        idxs = np.where(temp > 0)
        # Try until there are nonzero values
        while idxs[0].shape[0] == 0:
            i = random.randrange(0, x.shape[0])
            temp = x[i, :]
            temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
            idxs = np.where(temp > 0)

        temp = temp * r
        for idx in np.nditer(idxs):
            temp[idx] += np.random.uniform(0, 0.8 * Delta)
        synth = np.concatenate([synth, temp], axis=0)

    for r in range(0, size):
        temp = synth[r, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()

        noise = np.random.normal(mean, sigma, temp.shape[1])
        temp = temp + noise
        noisy = np.concatenate([noisy, temp], axis=0)

    print("noisy matrix norm: ", lg.norm(noisy))
    print("synth matrix norm: ", lg.norm(synth))

    return [synth, noisy]


def createIncrementalSynthMatrixKeepZeros(input_matrix="", Delta=5, size=1000, mean=50, sensors_to_keep=100):
    x = np.genfromtxt(input_matrix, delimiter=',')
    if (sensors_to_keep > 0):
        new_size = 90 * sensors_to_keep
        x = x[:, :new_size]
    synth = np.zeros((0, x.shape[1]))
    noisy = np.zeros((0, x.shape[1]))
    randoms = []
    for i in range(0, size):
        randoms.append(random.uniform(0.01, 2))
    for r in randoms:
        i = random.randrange(0, x.shape[0])
        temp = x[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        idxs = np.where(temp > 0)
        # Try until there are nonzero values
        while idxs[0].shape[0] == 0:
            i = random.randrange(0, x.shape[0])
            temp = x[i, :]
            temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
            idxs = np.where(temp > 0)

        temp = temp * r
        for idx in np.nditer(idxs):
            temp[idx] += np.random.uniform(0, 0.8 * Delta)
        synth = np.concatenate([synth, temp], axis=0)
    step_size = mean / size
    value = 0
    for r in range(0, size):
        temp = synth[r, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()

        noise = np.random.normal(value, 0.1, temp.shape[1])
        value += step_size
        # print("Noise Value ", value)
        temp = temp + noise
        noisy = np.concatenate([noisy, temp], axis=0)
        synth[synth < 0] = 0
    print("Normal matrix norm: ", lg.norm(noisy))
    print("Noisy matrix norm: ", lg.norm(synth))

    return [synth, noisy]


def createNoisyMatrix(input_matrix, size=1000, mean=50):
    temps = ()
    for r in range(0, size):
        i = random.randint(0, input_matrix.shape[0] - 1)
        temp = input_matrix[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()

        noise = np.random.normal(mean, mean / 2, temp.shape[1])
        temp = temp + noise
        temps += (temp,)
    noisy = np.concatenate(temps, axis=0)
    return noisy


def createNoisyMatrixWithStaticNoise(input_matrix, size=1000, noise=50):
    temps = ()
    for r in range(0, size):
        i = random.randint(0, input_matrix.shape[0] - 1)
        temp = input_matrix[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        temp = temp + noise
        temps += (temp,)
    noisy = np.concatenate(temps, axis=0)
    return noisy


def createOnlySynthMatrixKeepZeros(input_matrix="", Delta=5, size=1000, sensors_to_keep=100):
    x = np.genfromtxt(input_matrix, delimiter=',')
    print(x.shape)
    if (sensors_to_keep > 0):
        new_size = 90 * sensors_to_keep
        x = x[:, :new_size]
    synth = np.zeros((0, x.shape[1]))
    randoms = []
    for i in range(0, size):
        print(i)
        randoms.append(random.uniform(0.01, 2))
    total = len(randoms)
    counter = 1
    temps = ()
    for r in randoms:
        print(counter, "/", total, " ")
        counter += 1
        i = random.randrange(0, x.shape[0])
        temp = x[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        idxs = np.where(temp > 0)
        # Try until there are nonzero values
        while idxs[0].shape[0] == 0:
            i = random.randrange(0, x.shape[0])
            temp = x[i, :]
            temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
            idxs = np.where(temp > 0)

        temp = temp * r
        for idx in np.nditer(idxs):
            temp[idx] += np.random.uniform(0, 0.8 * Delta)
        temps += (temp,)
    synth = np.concatenate(temps, axis=0)

    print("Synthetic matrix norm: ", lg.norm(synth))

    return synth


def createSynthMatrix(input_matrix="", Delta=5, size=100, mean=50):
    """

    :param input_matrix: The file where the input matrix is stored
    :param Delta: The value of Delta parameter (uncertainty of data)
    :param size: the number of the rows of the output matrices
    :param mean: the mean of the normal distribution
    :return: [synth,noisy] syth contains the synthetic output matrix w/o noise.
                            noisy contains the synthetic output matrix with noise
    """
    x = np.genfromtxt(input_matrix, delimiter=',')
    print()
    print(x.size - np.count_nonzero(x))
    print()
    synth = np.zeros((0, x.shape[1]))
    noisy = np.zeros((0, x.shape[1]))
    randoms = []
    for i in range(0, size):
        randoms.append(random.uniform(0.01, 2))
    for r in randoms:
        i = random.randrange(0, x.shape[0])
        temp = x[i, :] * r

        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        noise = np.random.uniform(-0.8 * Delta, 0.8 * Delta, temp.shape[1])
        temp = temp + noise
        synth = np.concatenate([synth, temp], axis=0)

    for r in range(0, size):
        temp = synth[r, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()

        noise = np.random.normal(mean, 0.1, temp.shape[1])
        temp = temp + noise
        noisy = np.concatenate([noisy, temp], axis=0)
    print("noisy matrix norm: ", lg.norm(noisy))
    print("synth matrix norm: ", lg.norm(synth))

    return [synth, noisy]


def createSyntheticMatrixColumnExtender(input_matrix="stored_matrices/timeSeriesFull.csv"):
    x_1 = np.genfromtxt(input_matrix, delimiter=',')
    print("Input Matrix Has ", x_1.shape[1], "Columns \n", "Select Column repetition:")
    row_repetition = input()
    print("Select the number of rows of the synthetic dataset")
    size = int(input())
    print("Select the Delta [-\Delta,+\Delta]")
    Delta = int(input())
    print("Select mean for the Gaussian Distribution")
    mean = int(input())

    x = np.array(x_1)
    for i in range(0, int(row_repetition)):
        x = np.concatenate([x, x_1], axis=1)

    synth = np.zeros((0, x.shape[1]))
    noisy = np.zeros((0, x.shape[1]))
    randoms = []
    for i in range(0, size):
        randoms.append(random.uniform(0.01, 2))
    for r in randoms:
        i = random.randrange(0, x.shape[0])
        temp = x[i, :] * r

        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        noise = np.random.uniform(-0.8 * Delta, 0.8 * Delta, temp.shape[1])
        temp = temp + noise
        synth = np.concatenate([synth, temp], axis=0)

    for r in range(0, size):
        temp = synth[r, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()

        noise = np.random.normal(mean, 0.1, temp.shape[1])
        temp = temp + noise
        noisy = np.concatenate([noisy, temp], axis=0)
    print("noisy matrix norm: ", lg.norm(noisy))
    print("synth matrix norm: ", lg.norm(synth))

    return [synth, noisy]


def tensorFlattener(matricesp="../tools/matrices1.p"):
    matrices = pickle.load(open(matricesp, "rb"))
    print(len(matrices))
    counter = 1
    values = ()
    for key, value in matrices.items():
        print(counter, len(matrices))
        counter += 1
        values += (value,)
    output = np.concatenate(values, axis=1)
    print(output)
    print(output.shape)
    words = matricesp.split("/")

    np.savetxt("../stored_matrices/new_data_2017/" + words[-1], output, delimiter=",")


def restoreSynthMatrices(input_matrix="stored_matrices/inp.csv", noisy_matrix="stored_matrices/noisy.csv",
                         input_with_sparsity="stored_matrices/inpx.csv", X_u="stored_matrices/X_u.csv",
                         L="stored_matrices/L.csv", R="stored_matrices/R.csv"):
    with open('stored_matrices/max.txt', 'r') as f:
        lines = f.readlines()
    f.close()

    return [np.genfromtxt(input_matrix, delimiter=','), np.genfromtxt(noisy_matrix, delimiter=','),
            np.genfromtxt(input_with_sparsity, delimiter=','), np.genfromtxt(X_u, delimiter=','),
            np.genfromtxt(L, delimiter=','), np.genfromtxt(R, delimiter=','), int(lines[0])]


def createVadilationMatrix(normal_matrix, noisy_matrix, percentage=0.25, size_percentage=1):
    validate = np.zeros((0, normal_matrix.shape[1]))
    size = normal_matrix.shape[0] * size_percentage
    normal_size = int(size * percentage)
    event_size = int(size * (1 - percentage))
    #
    # print(normal_size)
    # print(event_size)
    idx_normals = np.random.randint(low=0, high=normal_matrix.shape[0], size=int(normal_size))
    idx_events = np.random.randint(low=0, high=noisy_matrix.shape[0], size=int(event_size))
    for i in np.nditer(idx_normals):
        temp = normal_matrix[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()

        validate = np.concatenate([validate, temp], axis=0)

    for i in np.nditer(idx_events):
        temp = noisy_matrix[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        validate = np.concatenate([validate, temp], axis=0)

    return validate


def createIncrementalSynthMatrixKeepZeros(input_matrix="", Delta=5, size=1000, mean=50, sensors_to_keep=100):
    x = np.genfromtxt(input_matrix, delimiter=',')
    if (sensors_to_keep > 0):
        new_size = 90 * sensors_to_keep
        x = x[:, :new_size]
    synth = np.zeros((0, x.shape[1]))
    noisy = np.zeros((0, x.shape[1]))
    randoms = []
    for i in range(0, size):
        randoms.append(random.uniform(0.01, 2))
    for r in randoms:
        i = random.randrange(0, x.shape[0])
        temp = x[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        idxs = np.where(temp > 0)
        # Try until there are nonzero values
        while idxs[0].shape[0] == 0:
            i = random.randrange(0, x.shape[0])
            temp = x[i, :]
            temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
            idxs = np.where(temp > 0)

        temp = temp * r
        for idx in np.nditer(idxs):
            temp[idx] += np.random.uniform(0, 0.8 * Delta)
        synth = np.concatenate([synth, temp], axis=0)
    step_size = mean / size
    value = 0
    for r in range(0, size):
        temp = synth[r, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        print("Value: ", value)
        noise = np.random.normal(value, 0.1, temp.shape[1])
        value += step_size
        temp = temp + noise
        noisy = np.concatenate([noisy, temp], axis=0)
        synth[synth < 0] = 0
    print("normal matrix norm: ", lg.norm(noisy))
    print("synth matrix norm: ", lg.norm(synth))

    return [synth, noisy]


def createColumnNoisyMatrixFromFile(matrix_name, size=1000, scale=1):
    input_matrix = np.genfromtxt(matrix_name, delimiter=',')
    deviations = []
    for i in range(0, input_matrix.shape[1]):
        deviations.append(np.std(input_matrix[:, i]))
    noisy = np.zeros((0, input_matrix.shape[1]))
    temps = ()
    for r in range(0, size):
        i = random.randint(0, input_matrix.shape[0] - 1)
        temp = input_matrix[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        temps += (temp,)
    noisy = np.concatenate(temps, axis=0)

    for i in range(0, input_matrix.shape[1]):
        noise = np.random.normal(scale * deviations[i], scale * deviations[i] / 2, noisy.shape[0])
        noise[noise < 0] = 0
        noisy[:, i] += noise

    return noisy


def createColumnNoisyMatrix(input_matrix: np.array, size: int = 1000, scale: int = 1) -> np.array:
    """
    createColumnNoisyMatrix creates a synthetic matrix that each column is multiplied with
    its deviation*scale
    :param input_matrix: Input matrix
    :param size:  Size of the output matrix
    :param scale: scale that we multiply  the column deviation
    :return: noisy: Synthetic output matrix
    """
    deviations = []
    for i in range(0, input_matrix.shape[1]):
        deviations.append(np.std(input_matrix[:, i]))
    temps = ()
    for r in range(0, size):
        i = random.randint(0, input_matrix.shape[0] - 1)
        temp = input_matrix[i, :]
        temp = np.reshape(temp, (temp.shape[0], 1)).transpose()
        temps += (temp,)
    noisy = np.concatenate(temps, axis=0)

    for i in range(0, input_matrix.shape[1]):
        noise = np.random.normal(scale * deviations[i], scale * deviations[i] / 2, noisy.shape[0])
        # noise[noise<0]= 0
        noisy[:, i] += noise

    return noisy

#
# def createMatrixTimeSeries():
#     model = NMF(n_components=50, init='random', random_state=0, max_iter=10000)
#     print("Initializing Matrix")
#     t0 = time()
#     try:
#         cnx = mysql.connector.connect(user='root', password='dsl4b-j0hn',
#                                       host='195.251.252.151',
#                                       database='DublinSCATS')
#         segments = [80, 500, 356, 755, 843, 652, 646, 157, 1162, 667, 57, 88, 695, 669, 1149, 237, 464, 1279, 1360]
#
#         cursor = cnx.cursor()
#         query = (
#             "SELECT t1.flow,DAY(from_unixtime(t1.upperTime)),MONTH(from_unixtime(t1.upperTime)),t1.upperTime, hour(from_unixtime(t1.upperTime)), minute(from_unixtime(t1.upperTime))"
#             " FROM DublinSCATS.road_fragments as t1 inner join ( SELECT count(upperTime) as c,DAY(from_unixtime(upperTime)) as t2day, month(from_unixtime(upperTime)) as t2month "
#             "FROM DublinSCATS.road_fragments where streetSegId=200  group by month(from_unixtime(upperTime)),DAY(from_unixtime(upperTime))) as t2 on DAY(from_unixtime(t1.upperTime))=t2.t2day "
#             "and MONTH(from_unixtime(t1.upperTime))=t2.t2month and streetSegId=200 and hour(from_unixtime(t1.upperTime))<10 and hour(from_unixtime(t1.upperTime))>6;")
#         cursor.execute(query, )
#
#         maxSamples = 0
#         Days = 0
#         prevday = 1
#         for (record) in cursor:
#
#             if prevday != record[1]:
#                 # print(maxSamples)
#                 maxSamples = 0
#                 Days += 1
#             date = datetime.datetime.fromtimestamp(record[3]).strftime('%Y-%m-%d %H:%M:%S')
#             maxSamples = maxSamples + 1
#             # print(record)
#             # print("Time= ", datetime.datetime.fromtimestamp(record[3]).strftime('%Y-%m-%d %H:%M:%S'))
#             prevday = record[1]
#
#         print(Days)
#         x = np.zeros([Days + 1, 240])
#         Days = 0
#         maxSamples = 0
#         prevday = 1
#         cursor.reset()
#         cursor.execute(query, )
#
#         for (record) in cursor:
#
#             if prevday != record[1]:
#                 Days += 1
#             date = datetime.datetime.fromtimestamp(record[3]).strftime('%Y-%m-%d %H:%M:%S')
#             # print(record[3], " ", record[4])
#             if 24 > record[4] >= 0 and 60 > record[5] >= 0:
#                 x[Days, int((record[4] * 10) + (record[5] + 1 - 6) / 6)] = record[0]
#             prevday = record[1]
#
#         print("Matrix Creating done in %0.3fs." % (time() - t0))
#
#         print("Factorizing")
#         t0 = time()
#
#         # x=np.delete(x, x[x.shape[0] - 1, :],axis=0)
#         model.fit(x)
#         print("done in %0.3fs." % (time() - t0))
#
#         print(model.reconstruction_err_)
#
#         # fact=fi.MatrixFactorization.complete()
#
#
#         cnx.close()
#         np.savetxt("timeSeries.csv", x, delimiter=",")
#         # np.savetxt("Input.csv", model.components_, delimiter=",")
#
#
#
#
#     except mysql.connector.Error as err:
#         if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
#             print("Something is wrong with your user name or password")
#         elif err.errno == errorcode.ER_BAD_DB_ERROR:
#             print("Database does not exist")
#         else:
#             print(err)
#     else:
#         cnx.close()
#
#     return [x,model.components_]


# x= np.random.rand(300,300)
# x,h=createSynthMatrixColumnExtender(x,300,5,5000,50)
# print(x.shape)
# np.savetxt("test.csv", x, delimiter=",")
# tensorFlattener()

# tensorFlattener("/home/stathis/PycharmProjects/matrixcompletionandprojection/stored_matrices/new_data_2017/pickle/matrices1.p")
