import sys

sys.path.extend(['/home/stathis/PycharmProjects/matrixcompletionandprojection'])
sys.path.extend(["../matrixcompletionandprojection"])
import ctypes
import json
import multiprocessing
import pickle
from time import time

import numpy as np
from maco_implementations.maco_full_parallel import maco
from plots.DeltaVariance import (deltaVarianceChart,
                                 deltaVarianceImpactOnFlowChart,
                                 new_input_plot, plotValidInvalidPercentage)
from plots.distance_plots import (DistanceChart, avgDistanceChart,
                                  plotStandardDeviation)
from plots.Recall_Precision_F1Measure import (f1ScorePlot, precisionPlot,
                                              recallPlot)
from projection_algorithm.ProjectionAlgorithm import solveSystem
from scipy.linalg import norm
from tools import MatrixCreator
from tools.Tools import L_ucalc

mkl_rt = ctypes.CDLL('libmkl_rt.so')
mkl_get_max_threads = mkl_rt.mkl_get_max_threads
def mkl_set_num_threads(cores):
    mkl_rt.mkl_set_num_threads(ctypes.byref(ctypes.c_int(cores)))
np.__config__.show()
mkl_set_num_threads(multiprocessing.cpu_count())

with open("settings.json") as settings_json:
    settings = json.load(settings_json)

number_of_sensors=settings["number_of_sensors"]
# print("Select the number of rows of the synthetic matrices:")
count_rows = settings["rows"]
# print("Select [-D,+D] of the synthetic matrices:")
synth_delta=settings["synth_delta"]
# print("Select dimension K for the matrix completion")
dimension=settings['dimension']
# print("Select percentage of validation size. (E.g. if input size rows=1000, percentage=0.1 will lead to 100 normal and 100 event test samples)")
# test_size=float(input())
#
x,x1 = MatrixCreator.createSynthMatrixKeepZeros(input_matrix="/home/stathis/PycharmProjects/matrixcompletionandprojection/stored_matrices/timeSeriesFull.csv", size=count_rows,mean=5,sigma=2, Delta=synth_delta,sensors_to_keep=number_of_sensors)
# print("x shape: ", x.shape)
# pickle.dump(x,open("x_100_sensors.p","wb"))
print(x.size)
x = pickle.load(open("x_100_sensors.p","rb"))
max = np.amax(x)
x = x / max

# Factorization with MACO
X_u=L_ucalc(x,synth_delta/max)
X_l = L_ucalc(x,-synth_delta/max)
X_l[X_l<0]=0


# Maco
#L, R = maco(x, X_u, X_l, m=0.1, dimension=dimension, epoch_n=settings["maco_epoch"])

R = MatrixCreator.scikitFactorization(x, n_components=dimension,alpha=0.1)
L=np.zeros((10,10))
pickle.dump([L,R],open("L-R_sensors.p","wb"))

exit(1)




