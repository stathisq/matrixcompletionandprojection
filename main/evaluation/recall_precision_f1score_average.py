import pickle
import numpy as np

from plots.Recall_Precision_F1Measure import plotMultiple, recallPrecisionF1ScorePlot


def average(l):
    llen = len(l)

    def divide(x): return x / llen

    return map(divide, map(sum, zip(*l)))


def minl(l):
    llen = len(l)
    mlen = len(l[0])
    result = []
    for j in range(0, mlen):
        temp = []
        for i in range(0, llen):
            temp.append(l[i][j])
        result.append(min(temp))
    return result


def maxl(l):
    llen = len(l)
    mlen = len(l[0])
    result = []
    for j in range(0, mlen):
        temp = []
        for i in range(0, llen):
            temp.append(l[i][j])
        result.append(max(temp))
    return result


deltas = pickle.load(open("./results/"+"mean=5k=10_delta_list0.p", "rb"))
deltas = deltas[:70]
recalls=[]
precisions=[]
f1_scores=[]
n_components=10
means=dict()
for m in range(15,35,5):
    mean = m
    recalls = []
    precisions = []
    f1_scores = []
    for i in range(0,1):
        recalls.append(pickle.load(open("./results/"+"mean="+str(mean)+"k="+str(n_components)+"_recall_list"+str(i)+".p", "rb")))
        precisions.append(pickle.load(open("./results/"+"mean="+str(mean)+"k="+str(n_components)+"_precision_list"+str(i)+".p", "rb")))
        f1_scores.append(pickle.load(open("./results/"+"mean="+str(mean)+"k="+str(n_components)+"_f1_score_list"+str(i)+".p", "rb")))
    recalls_mean = np.mean(recalls, axis=0)
    precisions_mean = np.mean(precisions, axis=0)
    f1_scores_mean = np.mean(f1_scores, axis=0)
    means[mean] = (recalls_mean,precisions_mean,f1_scores_mean)





plotMultiple(means, deltas,label="",fmt='-g*',fname="recall_error",yname="Recall", path=".",pos=0)
precisions_mean = np.mean(precisions, axis=0)
precisions_std = np.std(precisions, axis=0)

#recisionPlot(means, deltas, path=".")
plotMultiple(means, deltas,ecolor='g', label="", fmt='-r*', fname="precision_error", yname="Precision", path=".",pos=1)


f1_scores_mean = np.mean(f1_scores, axis=0)
f1_scores_std = np.std(f1_scores, axis=0)

#f1ScorePlot(means, deltas, path=".")
plotMultiple(means, deltas, label="", fmt='-b*', fname="f1_score_error", yname="F1 Score", path=".",pos=2)

recallPrecisionF1ScorePlot(recalls_mean, precisions_mean, f1_scores_mean, deltas[:35], path="")



