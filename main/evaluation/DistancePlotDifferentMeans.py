import sys
import multiprocessing as mp
from time import time

from multiprocessing import Queue

import multiprocessing

from plots.input_matrix_histogram import inputMatrixHistogram
from tools.matrix_tools import calculateDistribution

sys.path.extend(["../matrixcompletionandprojection"])
import numpy as np
from plots.DeltaVariance import plotValidInvalidPercentage, deltaVarianceChart, deltaVarianceImpactOnFlowChart, \
    new_input_plot
from plots.Recall_Precision_F1Measure import recallPlot, precisionPlot, f1ScorePlot
from plots.distance_plots import avgDistanceChart, DistanceChart, plotStandardDeviation, \
    plotStandardDeviationOnlySensors, plotStandardDeviationWithNormalAndEvent, plotStandardDeviationWithNormalAndEvents
from projection_algorithm.ProjectionAlgorithm import solveSystem
from tools import MatrixCreator
import pickle


def solveSystemParallel(R, i, validation_matrix, delta,x_max,q):
    print(delta*x_max,i,sep=" ")
    value = solveSystem(h=R, b=(validation_matrix[i, :]), delta=delta)
    q.put(((delta, i), value))
    return ((delta, i), value)


synth = pickle.load(open("synth_S=0_D=8.p", "rb"))
R = pickle.load(open("R_K=10_S=0.p", "rb"))
print(R.shape)
# print(calculateDistribution(x.astype(int)))
# inputMatrixHistogram(calculateDistribution(x.astype(int)), calculateDistribution(noisy.astype(int)))

x_max = np.amax(synth)
devs=dict()
sensor_avg = []
sensor_max = []
sensor_min = []
samples_Standard_deviation = []
samples_Mean_deviation = []
x = synth / x_max
daily_samples = 90
size_times = int(x.shape[1] / daily_samples)

for i in range(0, size_times):
    index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
    sensor_max.append(np.amax(x[:, index]))
    sensor_min.append(np.amin(x[:, index]))
    sensor_avg.append(np.average(x[:, index]))
    samples_Standard_deviation.append(np.std(x[:, index]))
    samples_Mean_deviation.append(np.mean(x[:, index]))
    # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

plotStandardDeviationOnlySensors(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg,
                                 type="Reconstruction")
pos_sensor_avg = []
for mean in range(5,50,5):
    noisy = MatrixCreator.createNoisyMatrix(synth,100,mean)
    x = synth / x_max
    noisy = noisy / x_max
    daily_samples = 90
    n_components = 10
    percentage = 0.5
    total_deltas_to_try = 70
    deltas_step = 1
    size_times = int(x.shape[1] / daily_samples)
    # Arrays for varius statistics


    # initialization of statistic arrays


    deltas_valid = []
    deltas_invalid = []
    deltas_total = []

    positive = []
    negative = []
    normal_flow = []
    event_flow = []
    plot_valid_percentage = []
    plot_invalid_percentage = []

    # iteration parameters
    step_n = 10
    number_of_samples = x.shape[0]

    validation_matrix = MatrixCreator.createVadilationMatrix(x, noisy, percentage, 0.20)
    negative_start = int(validation_matrix.shape[0] * percentage)

    pos = validation_matrix[:negative_start, :]
    neg = validation_matrix[negative_start:, :]

    # Arrays for various statistics
    pos_sensor_avg = []
    pos_max = []
    pos_min = []
    pos_samples_Standard_deviation = []
    pos_samples_Mean_deviation = []

    for i in range(0, int(pos.shape[1] / daily_samples)):
        index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
        pos_max.append(np.amax(pos[:, index]))
        pos_min.append(np.amin(pos[:, index]))
        pos_sensor_avg.append(np.average(pos[:, index]))
        pos_samples_Standard_deviation.append(np.std(pos[:, index]))
        pos_samples_Mean_deviation.append(np.mean(pos[:, index]))
        # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

    neg_sensor_avg = []
    neg_max = []
    neg_min = []
    neg_samples_Standard_deviation = []
    neg_samples_Mean_deviation = []

    for i in range(0, int(neg.shape[1] / daily_samples)):
        index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
        neg_max.append(np.amax(neg[:, index]))
        neg_min.append(np.amin(neg[:, index]))
        neg_sensor_avg.append(np.average(neg[:, index]))
        neg_samples_Standard_deviation.append(np.std(neg[:, index]))
        neg_samples_Mean_deviation.append(np.mean(neg[:, index]))
        # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

    devs[mean]=neg_sensor_avg



plotStandardDeviationWithNormalAndEvents(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg,normalSamples=pos_sensor_avg,eventSamples=devs,
                                     type="Historical")