from plots.input_matrix_histogram import inputMatrixHistogram
import pickle

from tools import MatrixCreator
from tools.matrix_tools import calculateDistribution

x= pickle.load(open("synth_S=0_D=8.p",'rb'))

matrices=dict()
for i in range(5,36,10):
    noisy= MatrixCreator.createNoisyMatrix(x,mean=i)
    matrices[i]=calculateDistribution(noisy[:,10].astype(int))

pickle.dump(matrices,open("mean_matrix.p",'wb'))
matrices[0]=calculateDistribution(x[:,10].astype(int))
inputMatrixHistogram(**matrices)
