from time import time

import numpy as np
from scipy import linalg

# from plots.DeltaVariance import deltaVarianceChart,deltaVarianceImpactOnFlowChart
from plots.DeltaVariance import deltaVarianceChart, deltaVarianceImpactOnFlowChart, plotValidInvalidPercentage, \
    new_input_whole_sensor_data_plot, new_input_plot
from plots.distance_plots import DistanceChart, plotStandardDeviation, avgDistanceChart
from projection_algorithm import ProjectionAlgorithm
from tools import MatrixCreator

# a = np.zeros((2, 512*512), dtype=np.float32)
# a = np.array([[1,2,3,4,5,6,7,8,9],
#               [5,43,5,6,23,235,2,124,13]])
#
# c = np.std(a, axis=1)

daily_samples=30
delete_range = 30
components_list = [30,50]

for n_components in components_list:
    x, R,h = MatrixCreator.createMatrixMultipleSegmentCsv(n_components=n_components, input_matrix="timeSeriesFull.csv", delete_range=delete_range, iterations=5000)
    # idx = np.random.randint(100, size=10)

    remained=x.shape[0]-delete_range
    max_value = np.average(x[:, 1])
    min_value = np.amin(x[:, 1])
    delta = max_value - min_value
    print(max_value)
    print('Delta: ', delta)
    # sample = x[idx, :]

    sensor_avg = []
    sensor_max = []
    sensor_min = []
    samples_Standard_deviation=[]
    samples_Mean_deviation=[]
    # print(x.shape)
    # print sensors' statistics
    for i in range(0, int(x.shape[1]/daily_samples)):
        index= np.arange(start=i*daily_samples,stop=i*daily_samples+daily_samples)
        sensor_max.append(np.amax(x[:, index]))
        sensor_min.append(np.amin(x[:, index]))
        sensor_avg.append(np.average(x[:, index]))
        samples_Standard_deviation.append(np.std(x[:, index]))
        samples_Mean_deviation.append(np.mean(x[:, index]))
        # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

    deltas_valid = []
    deltas_invalid = []
    deltas_total=[]

    positive = []
    negative = []
    normal_flow = []
    event_flow = []
    plot_valid_percentage=[]
    plot_invalid_percentage=[]

    for delta in range(0, 20):
        t0 = time()
        labels = []

        counter_valid = 0
        total_avg_distance_valid = 0

        # total_avg_valid=[float]*int(x.shape[1]/daily_samples)
        total_avg_valid=np.zeros([int(x.shape[1]/daily_samples)])
        counter_invalid = 0
        total_avg_distance_invalid = 0
        # total_avg_invalid=[float]*int(x.shape[1]/daily_samples)
        total_avg_invalid=np.zeros([int(x.shape[1]/daily_samples)])

        all_days=[]
        worst_days=[]
        m=np.mean(x,axis=0)
        for i in range(0,x.shape[0]):
            all_days.append(i)
            worst_days.append(linalg.norm(x[i,:] - m))
            print(linalg.norm(x[i,:] - m))
        s=sorted(zip(worst_days,all_days), reverse=True)

        print(s)
        remained=30
        for l in s[:30]:

            i=l[1]
            normal_total = np.zeros([int(x.shape[1]/daily_samples), 0])
            event_total = np.zeros([int(x.shape[1]/daily_samples), 0])
            avg=np.zeros([int(x.shape[1]/daily_samples)])
            avg_distance = 0
            for k in range(0, int(x.shape[1]/daily_samples)):
                index = np.arange(start=k * daily_samples, stop=k * daily_samples + daily_samples)
                avg_distance += abs(sensor_avg[k] - np.average(x[i, index]))
                avg[k]=(np.average(x[i, index]))
            value = ProjectionAlgorithm.solveSystem(h=h, b=(x[i, :] * 2.3), delta=delta)
            if value == 0:
                total_avg_distance_valid += avg_distance / (x.shape[1] / daily_samples)
                total_avg_valid+=avg
                counter_valid += 1
                normal_total=np.concatenate([normal_total,avg.reshape((avg.shape[0],1))],axis=1)
                new_input_plot(daily_samples=daily_samples,x=x[i, :],K=n_components,type="normal",Delta=delta,day=i)
                labels.append(0)
                daily_avg = []
                sensor_id = []
                for y in range(0, int(x.shape[1] / daily_samples)):
                    temp1 = x[i, (30 * y):(y * 30 + 30)]
                    temp2 = np.average(temp1)
                    sensor_id.append(y)
                    daily_avg.append(temp2)
                DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                              k=n_components, delta=delta,type="normal")
                print("NORMAL DAY:")
                distance = sorted(zip(daily_avg, sensor_id))[:10]
                print(distance)
            if value == 2:
                total_avg_distance_invalid += avg_distance / (x.shape[1] / daily_samples)
                total_avg_invalid += avg
                event_total=np.concatenate([event_total,avg.reshape((avg.shape[0],1))],axis=1)

                sensor_id = []
                daily_avg= []
                for y in range(0, int(x.shape[1] / daily_samples)):
                    temp1 = x[i, (30 * y):(y * 30 + 30)]
                    temp2 = np.average(temp1)
                    sensor_id.append(y)
                    daily_avg.append(temp2)
                DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                              k=n_components, delta=delta,type="event")
                # print(avg/19)
                labels.append(1)
                new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="event",Delta=delta,day=i)
                counter_invalid += 1
                print("EVENT DAY:")
                distance = sorted(zip(daily_avg, sensor_id), reverse=True)[:10]
                print(distance)
            # DistanceChart(avgFlow=sensor_avg, normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components)
            plotStandardDeviation(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg, normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components,type="Standard",delta=delta)
            plotStandardDeviation(deviationMatrix=samples_Mean_deviation, avgFlow=sensor_avg, normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components,type="Mean",delta=delta)
        print("--------------------------------")
        print("Delta: ", delta)
        print("-----")
        print("Normal: ", (counter_valid / (delete_range)) * 100, "%")
        plot_valid_percentage.append(counter_valid / (delete_range)* 100)
        if counter_valid!=0:
            print("Distance from average: ", total_avg_distance_valid / counter_valid)
        else:
            print("Distance from average: Not Found")

        print("-----")
        print("Event : ", (counter_invalid / (delete_range)) * 100, "%")
        plot_invalid_percentage.append((counter_invalid / (delete_range)) * 100)
        if counter_invalid!=0:
            print("Distance from average: ", total_avg_distance_invalid / counter_invalid)
        else :
            print("Distance from average:Not Found ", )

        print("--------------------------------")
        positive.append((counter_valid / (delete_range)) * 100)
        negative.append((counter_invalid / (delete_range)) * 100)
        deltas_total.append(delta)
        if counter_valid!=0:
            normal_flow.append(total_avg_distance_valid / counter_valid)
            # total_avg_valid = [x / counter_valid for x in total_avg_valid]
            total_avg_valid=total_avg_valid/counter_valid
            deltas_valid.append(delta)
        if counter_invalid != 0:
            event_flow.append(total_avg_distance_invalid / counter_invalid)
            deltas_invalid.append(delta)
            total_avg_invalid = total_avg_invalid / counter_invalid
            # total_avg_invalid = [x / counter_invalid for x in total_avg_invalid]

        avgDistanceChart(avgFlow=sensor_avg,normalFlow=total_avg_valid,eventFlow=total_avg_invalid,delta=delta,k=n_components)
        new_input_whole_sensor_data_plot(daily_samples=daily_samples,x=x,K=n_components,Delta=delta,remained=remained,labels=labels)
        print("Test done in %0.3fs." % (time() - t0))

    plotValidInvalidPercentage(k=n_components,valid=plot_valid_percentage,invalid=plot_invalid_percentage,deltas=deltas_total)
    deltaVarianceChart(deltas_total, positive, negative,k=n_components)
    deltaVarianceImpactOnFlowChart(deltas_valid, deltas_invalid, normal_flow, event_flow, k=n_components)