import sys
import multiprocessing as mp
import os
from time import time


import multiprocessing
import SharedArray as sa

from maco_implementations.competitors.pyod_tests import evaluateCompetitors
from maco_implementations.online_versoin.maco_full_parallel_python_with_updates import copy_to_shared_Array
sys.path.extend(["../matrixcompletionandprojection"])
import numpy as np
from plots.DeltaVariance import deltaVarianceChart
from plots.distance_plots import plotStandardDeviationOnlySensors, plotStandardDeviationWithNormalAndEvent
from projection_algorithm.ProjectionAlgorithm import solveSystem
from tools import MatrixCreator
import pickle
import SharedArray as sa

def solveSystemParallel(R, i, validation_matrix, delta,x_max,s,q):
    # print(delta*x_max,i,sep=" ")
    value = solveSystem(h=R, b=(validation_matrix[i, :]), delta=delta,s=s,disp=True)
    q.put(((delta, i), value))
    return ((delta, i), value)




for i in sa.list():
    sa.delete("shm://" + str(i[0].decode('UTF-8')))


synth = pickle.load(open("/home/stathis/PycharmProjects/matrixcompletionandprojection/main/x_100_sensors.p", "rb"))

[L,R] = pickle.load(open("/home/stathis/PycharmProjects/matrixcompletionandprojection/main/L-R_sensors.p", "rb"))

R = copy_to_shared_Array(R,"R")

r = R.shape[0]

e = 0.1

s = ((r * np.log(r)) / e) * np.log(r * np.log(r) / e)


# print(calculateDistribution(x.astype(int)))
# inputMatrixHistogram(calculateDistribution(x.astype(int)), calculateDistribution(noisy.astype(int)))


x_max = np.amax(synth)
for mean in range(5,35,5):
    print("Evaluating Mean: ", mean)
    noisy = MatrixCreator.createNoisyMatrix(synth,100,mean)
    x = synth / x_max
    noisy = noisy / x_max
    daily_samples = 90
    n_components = 10
    percentage = 0.5
    total_deltas_to_try = 40
    deltas_step = 10
    size_times = int(x.shape[1] / daily_samples)


    # Arrays for various statistics
    sensor_avg = []
    sensor_max = []
    sensor_min = []
    samples_Standard_deviation = []
    samples_Mean_deviation = []

    # initialization of statistic arrays
    for i in range(0, size_times):
        index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
        sensor_max.append(np.amax(x[:, index]))
        sensor_min.append(np.amin(x[:, index]))
        sensor_avg.append(np.average(x[:, index]))
        samples_Standard_deviation.append(np.std(x[:, index]))
        samples_Mean_deviation.append(np.mean(x[:, index]))
        # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

    plotStandardDeviationOnlySensors(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg,
                                     type="Reconstruction")

    deltas_valid = []
    deltas_invalid = []
    deltas_total = []

    positive = []
    negative = []
    normal_flow = []
    event_flow = []
    plot_valid_percentage = []
    plot_invalid_percentage = []

    # iteration parameters
    step_n = 10
    number_of_samples = x.shape[0]

    validation_matrix = MatrixCreator.createVadilationMatrix(x, noisy, percentage, 0.5)



    negative_start = int(validation_matrix.shape[0] * percentage)


    # evaluateCompetitors(x,validation_matrix,negative_start)

    pos = validation_matrix[:negative_start, :]
    neg = validation_matrix[negative_start:, :]

    # Arrays for various statistics
    pos_sensor_avg = []
    pos_max = []
    pos_min = []
    pos_samples_Standard_deviation = []
    pos_samples_Mean_deviation = []

    for i in range(0, int(pos.shape[1] / daily_samples)):
        index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
        pos_max.append(np.amax(pos[:, index]))
        pos_min.append(np.amin(pos[:, index]))
        pos_sensor_avg.append(np.average(pos[:, index]))
        pos_samples_Standard_deviation.append(np.std(pos[:, index]))
        pos_samples_Mean_deviation.append(np.mean(pos[:, index]))
        # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

    neg_sensor_avg = []
    neg_max = []
    neg_min = []
    neg_samples_Standard_deviation = []
    neg_samples_Mean_deviation = []

    for i in range(0, int(neg.shape[1] / daily_samples)):
        index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
        neg_max.append(np.amax(neg[:, index]))
        neg_min.append(np.amin(neg[:, index]))
        neg_sensor_avg.append(np.average(neg[:, index]))
        neg_samples_Standard_deviation.append(np.std(neg[:, index]))
        neg_samples_Mean_deviation.append(np.mean(neg[:, index]))
        # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")






    plotStandardDeviationWithNormalAndEvent(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg,normalSamples=pos_sensor_avg,eventSamples=neg_sensor_avg,
                                     type="Historical")


    plotStandardDeviationOnlySensors(deviationMatrix=pos_samples_Standard_deviation, avgFlow=pos_sensor_avg,
                                     type="Normal")

    plotStandardDeviationOnlySensors(deviationMatrix=neg_samples_Standard_deviation, avgFlow=neg_sensor_avg,

                                     type="Event")


    m = multiprocessing.Manager()
    q = m.Queue()
    results = []

    for d in range(0, total_deltas_to_try, deltas_step):
        delta = d / x_max
        for i in range(0, validation_matrix.shape[0]):
            results.append((R, i, validation_matrix, delta, x_max,s, q))
    for iteration in range(0,1):

        print("Starting calculation of projections")
        print("Mean : ",mean)
        print("Rows to be examined: ",len(results))
        t1 = time()
        pool = mp.Pool(16)
        pool.starmap(solveSystemParallel, results)
        pool.close()
        pool.join()
        d_results = dict()
        while not q.empty():
            i=q.get()
            d_results[i[0]] = i[1]
        pickle.dump(d_results,open("d_results"+str(n_components)+"_"+str(mean)+ "_.p",'wb'))
        print("Done")
        print(time()-t1)
        recall_list = []
        precision_list = []
        f1_score_list = []
        delta_list = []
        path = "synthetic-" + str(validation_matrix.shape[0]) + "-" + str(negative_start)
        # d_results = pickle.load(open("d_results"+str(n_components)+"_"+str(mean)+ "_.p", 'rb'))

        # try:
        for d in range(0, total_deltas_to_try, deltas_step):
            # Initialize local statistc matrices
            true_positive = []
            true_negative = []
            false_positive = []
            false_negative = []
            print("Non normalized Delta: ", d)
            plot_delta = d
            delta = d / (x_max)

            t0 = time()
            labels = []

            counter_valid = 0
            total_avg_distance_valid = 0

            total_avg_valid = np.zeros([size_times])
            counter_invalid = 0
            total_avg_distance_invalid = 0
            total_avg_invalid = np.zeros([size_times])

            print("Total Samples", validation_matrix.shape[0])
            print("Negative Start", negative_start)

            # Iteration through Matrices samples
            for l in range(0, int(validation_matrix.shape[0])):

                # Initialize local statistic matrices (for each sample)
                i = l
                normal_total = np.zeros([size_times, 0])
                event_total = np.zeros([size_times, 0])
                avg = np.zeros([size_times])
                avg_distance = 0
                for k in range(0, size_times):
                    index = np.arange(start=k * daily_samples, stop=k * daily_samples + daily_samples)
                    avg_distance += abs(sensor_avg[k] - np.average(validation_matrix[i, index]))
                    avg[k] = (np.average(x[i, index]))
                value = d_results[(delta, i)]
                # value = solveSystem(h=R, b=(validation_matrix[i, :]), delta=delta)
                if value == 0:
                    if i < negative_start:
                        true_positive.append(1)
                    if i > negative_start:
                        false_positive.append(1)
                    total_avg_distance_valid += avg_distance / (x.shape[1] / daily_samples)
                    total_avg_valid += avg
                    counter_valid += 1
                    normal_total = np.concatenate([normal_total, avg.reshape((avg.shape[0], 1))], axis=1)
                    # new_input_plot(daily_samples=daily_samples, x=validation_matrix[i, :], K=n_components, type="normal",
                    #                Delta=int(delta * x_max),
                    #                day=i, path=path)
                    labels.append(0)
                    daily_avg = []
                    sensor_id = []
                    for y in range(0, size_times):
                        temp1 = x[i, (daily_samples * y):(y * daily_samples + daily_samples)]
                        temp2 = np.average(temp1)
                        sensor_id.append(y)
                        daily_avg.append(temp2)
                # DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                #               k=n_components, delta=int(delta * x_max), type="normal", path=path)
                if value == 2:
                    if i > negative_start:
                        true_negative.append(1)
                    if i < negative_start:
                        false_negative.append(1)

                    total_avg_distance_invalid += avg_distance / (size_times)
                    total_avg_invalid += avg
                    event_total = np.concatenate([event_total, avg.reshape((avg.shape[0], 1))], axis=1)

                    sensor_id = []
                    daily_avg = []
                    for y in range(0, size_times):
                        labels.append(1)
                        temp1 = x[i, (daily_samples * y):(y * daily_samples + daily_samples)]
                        temp2 = np.average(temp1)
                        sensor_id.append(y)
                        daily_avg.append(temp2)
                    #  DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                    #               k=n_components, delta=int(delta * x_max), type="event", path=path)
                    labels.append(1)
                    # new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="event", Delta=int(delta * x_max),
                    #                day=i,
                    #                path=path)
                    counter_invalid += 1
                # plotStandardDeviation(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg,
                #                       normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components,
                #                       type="Standard", delta=int(delta * x_max), path=path)
                # plotStandardDeviation(deviationMatrix=samples_Mean_deviation, avgFlow=sensor_avg, normalFlow=normal_total,
                #                       eventFlow=event_total, day=i, k=n_components, type="Mean", delta=int(delta * x_max),
                #                       path=path)
            print("--------------------------------")
            print("Delta: ", delta)
            print("-----")
            print("Normal: ", (counter_valid / (counter_invalid + counter_valid)) * 100, "%")
            plot_valid_percentage.append(counter_valid / (validation_matrix.shape[0]) * 100)
            if counter_valid != 0:
                print("Distance from average: ", total_avg_distance_valid / counter_valid)
            else:
                print("Distance from average: Not Found")

            print("-----")
            print("Event : ", (counter_invalid / (counter_invalid + counter_valid)) * 100, "%")
            plot_invalid_percentage.append((counter_invalid / (validation_matrix.shape[0])) * 100)
            if counter_invalid != 0:
                print("Distance from average: ", total_avg_distance_invalid / counter_invalid)
            else:
                print("Distance from average:Not Found ", )

            print("--------------------------------")
            positive.append((counter_valid / (validation_matrix.shape[0])) * 100)
            negative.append((counter_invalid / (validation_matrix.shape[0])) * 100)
            deltas_total.append(int(delta * x_max))
            if counter_valid != 0:
                normal_flow.append(total_avg_distance_valid / counter_valid)
                # total_avg_valid = [x / counter_valid for x in total_avg_valid]
                total_avg_valid = total_avg_valid / counter_valid
                deltas_valid.append(delta)
            if counter_invalid != 0:
                event_flow.append(total_avg_distance_invalid / counter_invalid)
                deltas_invalid.append(delta)
                total_avg_invalid = total_avg_invalid / counter_invalid

            # avgDistanceChart(avgFlow=sensor_avg, normalFlow=total_avg_valid, eventFlow=total_avg_invalid,
            #                  delta=int(delta * x_max),
            #                  k=n_components, path=path)

            # new_input_whole_sensor_data_plot(daily_samples=daily_samples, x=x, K=n_components, Delta=int(delta*max),
            #                                  remained=remained, labels=s, path=path)
            print("Test done in %0.3fs." % (time() - t0))
            print("True positives", len(true_positive))
            print("True negatives", len(true_negative))
            print("False positives", len(false_positive))
            print("False negatives", len(false_negative))
            if len(true_positive) > 0 or len(false_positive) > 0:
                precision = len(true_positive) / (len(true_positive) + len(false_positive))
            else:
                precision = 0
            if len(true_positive) > 0 or len(false_negative) > 0:
                recall = len(true_positive) / (len(true_positive) + len(false_negative))
            else:
                recall = 0
                # F-Measure
                # b=0.1
                # f_measure = (1+b*b)*((precision*recall)/(b*b*precision+recall))

            if precision > 0 or recall > 0:
                f1_score = 2 * (precision * recall) / (precision + recall)
            else:
                f1_score = 0
            print("Precision:", precision)
            print("Recall:", recall)
            print("F1 Score:", f1_score)
            recall_list.append(recall)
            precision_list.append(precision)
            f1_score_list.append(f1_score)
            delta_list.append(d)

        # Varius Plots
        # except KeyboardInterrupt:
        #     pass

        #plotValidInvalidPercentage(k=n_components, valid=plot_valid_percentage, invalid=plot_invalid_percentage,
        #                          deltas=deltas_total, path=path)
        deltaVarianceChart(deltas_total, positive, negative,m=mean, k=n_components, path=path)
        if not os.path.exists("./results"):
            os.mkdir("./results")
        pickle.dump(recall_list, open("./results/"+"mean="+str(mean)+"k="+str(n_components)+"_recall_list"+str(iteration)+".p", "wb"))
        pickle.dump(precision_list, open("./results/"+"mean="+str(mean)+"k="+str(n_components)+"_precision_list"+str(iteration)+".p", "wb"))
        pickle.dump(f1_score_list, open("./results/"+"mean="+str(mean)+"k="+str(n_components)+"_f1_score_list"+str(iteration)+".p", "wb"))
        pickle.dump(delta_list, open("./results/"+"mean="+str(mean)+"k="+str(n_components)+"_delta_list"+str(iteration)+".p", "wb"))
