import sys
sys.path.extend(["../../matrixcompletionandprojection"])

from tools import MatrixCreator
import pickle
import ctypes
import numpy as np
import multiprocessing


mkl_rt = ctypes.CDLL('libmkl_rt.so')
mkl_get_max_threads = mkl_rt.mkl_get_max_threads
def mkl_set_num_threads(cores):
    mkl_rt.mkl_set_num_threads(ctypes.byref(ctypes.c_int(cores)))
np.__config__.show()
mkl_set_num_threads(multiprocessing.cpu_count())

print(multiprocessing.cpu_count())
print("Select the number of sensors you want to keep (type 0 to select all available sensors)")
number_of_sensors=int(input())
print("Select the number of rows of the synthetic matrices:")
count_rows = int(input())
print("Select [-D,+D] of the synthetic matrices:")
synth_delta=int(input())
#print("Select mean of the Gaussian noise on the event synthetic matrix")
#synth_mean=int(input())
print("Select dimension K for the matrix completion")
dimension=int(input())
#print("Select percentage of validation size. (E.g. if input size rows=1000, percentage=0.1 will lead to 100 normal and 100 event test samples)")
#test_size=float(input())
#

x = MatrixCreator.createOnlySynthMatrixKeepZeros(input_matrix="/home/stathis/PycharmProjects/matrixcompletionandprojection/stored_matrices/new_data_2017/matrices1.p", size=count_rows, Delta=synth_delta,sensors_to_keep=number_of_sensors)
# pickle.dump(x,open("synth_S="+str(number_of_sensors)+"_D="+str(synth_delta)+".p","wb"),protocol=4)

x =pickle.load(open("synth_S=0_D=10.p","rb"))
# R=pickle.load(open("R_K="+str(dimension)+"_S="+str(number_of_sensors)+".p","rb"))

max = np.amax(x)
x = x / max

R = MatrixCreator.scikitFactorization(x, n_components=dimension)
pickle.dump(R,open("R_K="+str(dimension)+"_S="+str(number_of_sensors)+".p","wb"))
