import sys
sys.path.extend(["../matrixcompletionandprojection"])
from time import time
import numpy as np
from maco_implementations.old_implementations.maco_simple_no_iter_calc import maco
from plots.DeltaVariance import deltaVarianceChart, deltaVarianceImpactOnFlowChart, plotValidInvalidPercentage, \
    new_input_plot
from plots.Recall_Precision_F1Measure import recallPlot, precisionPlot, f1ScorePlot
from plots.distance_plots import DistanceChart, plotStandardDeviation, avgDistanceChart
from projection_algorithm import ProjectionAlgorithm
from tools import MatrixCreator
from tools.Tools import sparsityMask
import os

daily_samples = 30

# mini menu

print(os.getcwd())
print(sys.path)
print("Do you want to use stored_matrices:", "1: Yes", "2: No", sep='\n')
stored_option = int(input())
store_option = 2
if stored_option != 1 and stored_option != 2:
    print("Please select a valid option")
    exit(2)
if (stored_option == 2):
    print("Do you want to store the newly created matrices:", "1: Yes", "2: No", sep='\n')
    store_option = int(input())
    if store_option != 1 and store_option != 2:
        print("Please select a valid option")
        exit(2)

print("Please decomposition dimension:")
n_components = int(input())

print("Please select of percentage of the validation matrix compare to initial matrices\n"
      "(e.g. if the initial matrix has 1000 rows percentage=0.1 will lead to 100 rows to validation matrix)")
percentage=float(input())
print("Please select of percentage of the positive/negative samples on validation dataset\n"
      "(e.g. percentage=0.75 will lead to 75% positive and 25% negative rows to validation matrix)")
size_percentage=float(input())

if stored_option == 1:
    inp, noisy, x, X_u, L, R, max = MatrixCreator.restoreSynthMatrices(
        input_matrix="stored_matrices/synthetic_repeated_cols/inp.csv",
        noisy_matrix="stored_matrices/synthetic_repeated_cols/noisy.csv",
        input_with_sparsity="stored_matrices/synthetic_repeated_cols/x.csv",
        X_u="stored_matrices/synthetic_repeated_cols/X_u.csv",
        L="stored_matrices/synthetic_repeated_cols/L.csv", R="stored_matrices/synthetic_repeated_cols/R.csv")

#################################
# Normalize input matrices

# Create Synthetic Matrix and add Noise
if stored_option == 2:
    inp, noisy = MatrixCreator.createSyntheticMatrixColumnExtender(input_matrix="/home/stathis/PycharmProjects/matrixcompletionandprojection/stored_matrices/timeSeriesFull.csv")

    # inputMatrixHistogram(calculateDistribution(inp.astype(int)), calculateDistribution(noisy.astype(int)))

    # #Sparsity Mask Init Init
    x, X_u = sparsityMask(input=inp, upper_bounds=1, percentance=30)
    #################################
    # Normalize input matrices
    #################################
    max = np.amax(inp)
    print("Max Value", max)
    inp = inp / max
    noisy = noisy / max
    X_l = np.zeros(x.shape)

    # Factorization with MACO

    L, R = maco(x, X_u, X_l, m=0.001, dimension=n_components, epoch_n=52)
    if (store_option == 1):
        if not os.path.exists("stored_matrices/synthetic_repeated_cols"):
            os.makedirs("stored_matrices/synthetic_repeated_cols")
        np.savetxt("stored_matrices/synthetic_repeated_cols/inp.csv", inp, delimiter=",")
        np.savetxt("stored_matrices/synthetic_repeated_cols/noisy.csv", noisy, delimiter=",")
        np.savetxt("stored_matrices/synthetic_repeated_cols/X_u.csv", X_u, delimiter=",")
        np.savetxt("stored_matrices/synthetic_repeated_cols/L.csv", L, delimiter=",")
        with open('stored_matrices/synthetic_repeated_cols/max.txt', 'w') as f:
            f.write('%d' % max)

max_value = np.amax(x[2, :])
min_value = np.amin(x[2, :
                    ])
delta = max_value - min_value
print(max_value)
print('Delta: ', delta)
# sample = x[idx, :]

# Arrays for varius statistics
sensor_avg = []
sensor_max = []
sensor_min = []
samples_Standard_deviation = []
samples_Mean_deviation = []

# initialization of statistic arrays
for i in range(0, int(x.shape[1] / daily_samples)):
    index = np.arange(start=i * daily_samples, stop=i * daily_samples + daily_samples)
    sensor_max.append(np.amax(x[:, index]))
    sensor_min.append(np.amin(x[:, index]))
    sensor_avg.append(np.average(x[:, index]))
    samples_Standard_deviation.append(np.std(x[:, index]))
    samples_Mean_deviation.append(np.mean(x[:, index]))
    # print("Sensor Max:", sensor_max[i], " Min:", sensor_min[i], "Average: ",sensor_avg[i],sep=" ")

deltas_valid = []
deltas_invalid = []
deltas_total = []

positive = []
negative = []
normal_flow = []
event_flow = []
plot_valid_percentage = []
plot_invalid_percentage = []

# iteration parameters
step_n = 10
number_of_samples = x.shape[0] / step_n

validation_matrix = MatrixCreator.createVadilationMatrix(inp, noisy, percentage, 0.50)
negative_start = validation_matrix.shape[0] * 0.75

recall_list = []
precision_list = []
f1_score_list = []
delta_list = []

# End of initialization of statistic arrays



# Iteration through many \Delta values
for d in range(0, 100):
    # Initialize local statistic matrices
    true_positive = []
    true_negative = []
    false_positive = []
    false_negative = []
    print("Non normalized Delta: ", d)
    plot_delta = delta
    delta = d / max

    t0 = time()
    labels = []

    counter_valid = 0
    total_avg_distance_valid = 0

    # total_avg_valid=[float]*int(x.shape[1]/daily_samples)
    total_avg_valid = np.zeros([int(x.shape[1] / daily_samples)])
    counter_invalid = 0
    total_avg_distance_invalid = 0
    # total_avg_invalid=[float]*int(x.shape[1]/daily_samples)
    total_avg_invalid = np.zeros([int(x.shape[1] / daily_samples)])

    # all_days = []
    # worst_days = []
    # m = np.mean(x, axis=0)
    # for i in range(0, x.shape[0]):
    #     all_days.append(i)
    #     worst_days.append(linalg.norm(x[i, :] - m))
    #     # print(linalg.norm(x[i,:] - m))
    # s = sorted(zip(worst_days, all_days), reverse=True)


    print("Total Samples", validation_matrix.shape[0])
    print("Negative Start", negative_start)
    path = "synthetic-" + str(validation_matrix.shape[0]) + "-" + str(negative_start)

    # Iteration through Matrices samples
    for l in range(0, int(validation_matrix.shape[0])):

        # Initialize local statistic matrices (for each sample)
        i = l
        normal_total = np.zeros([int(x.shape[1] / daily_samples), 0])
        event_total = np.zeros([int(x.shape[1] / daily_samples), 0])
        avg = np.zeros([int(x.shape[1] / daily_samples)])
        avg_distance = 0
        for k in range(0, int(x.shape[1] / daily_samples)):
            index = np.arange(start=k * daily_samples, stop=k * daily_samples + daily_samples)
            avg_distance += abs(sensor_avg[k] - np.average(x[i, index]))
            avg[k] = (np.average(x[i, index]))
        value = ProjectionAlgorithm.solveSystem(h=L, b=(validation_matrix[i, :]), delta=delta)
        if value == 0:
            if i < negative_start:
                true_positive.append(1)
            if i > negative_start:
                false_positive.append(1)
            total_avg_distance_valid += avg_distance / (x.shape[1] / daily_samples)
            total_avg_valid += avg
            counter_valid += 1
            normal_total = np.concatenate([normal_total, avg.reshape((avg.shape[0], 1))], axis=1)
            new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="normal", Delta=delta,
                           day=i, path=path)
            labels.append(0)
            daily_avg = []
            sensor_id = []
            for y in range(0, int(x.shape[1] / daily_samples)):
                temp1 = x[i, (30 * y):(y * 30 + 30)]
                temp2 = np.average(temp1)
                sensor_id.append(y)
                daily_avg.append(temp2)
            DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                          k=n_components, delta=delta, type="normal", path=path)
        if value == 2:

            if i > negative_start:
                true_negative.append(1)
            if i < negative_start:
                false_negative.append(1)

            total_avg_distance_invalid += avg_distance / (x.shape[1] / daily_samples)
            total_avg_invalid += avg
            event_total = np.concatenate([event_total, avg.reshape((avg.shape[0], 1))], axis=1)

            sensor_id = []
            daily_avg = []
            for y in range(0, int(x.shape[1] / daily_samples)):
                labels.append(1)
                temp1 = x[i, (30 * y):(y * 30 + 30)]
                temp2 = np.average(temp1)
                sensor_id.append(y)
                daily_avg.append(temp2)
            DistanceChart(avgFlow=sensor_avg, normalFlow=[], eventFlow=daily_avg, day=i,
                          k=n_components, delta=delta, type="event", path=path)
            # print(avg/19)
            labels.append(1)
            new_input_plot(daily_samples=daily_samples, x=x[i, :], K=n_components, type="event", Delta=delta, day=i,
                           path=path)
            counter_invalid += 1
            # print("EVENT DAY:")
            # distance = sorted(zip(daily_avg, sensor_id), reverse=True)[:10]
            # print(distance)
        # DistanceChart(avgFlow=sensor_avg, normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components)
        plotStandardDeviation(deviationMatrix=samples_Standard_deviation, avgFlow=sensor_avg,
                              normalFlow=normal_total, eventFlow=event_total, day=i, k=n_components,
                              type="Standard", delta=delta, path=path)
        plotStandardDeviation(deviationMatrix=samples_Mean_deviation, avgFlow=sensor_avg, normalFlow=normal_total,
                              eventFlow=event_total, day=i, k=n_components, type="Mean", delta=delta, path=path)
    print("--------------------------------")
    print("Delta: ", delta)
    print("-----")
    print("Normal: ", (counter_valid / (counter_invalid + counter_valid)) * 100, "%")
    plot_valid_percentage.append(counter_valid / (x.shape[0] / step_n) * 100)
    if counter_valid != 0:
        print("Distance from average: ", total_avg_distance_valid / counter_valid)
    else:
        print("Distance from average: Not Found")

    print("-----")
    print("Event : ", (counter_invalid / (counter_invalid + counter_valid)) * 100, "%")
    plot_invalid_percentage.append((counter_invalid / (x.shape[0] / step_n)) * 100)
    if counter_invalid != 0:
        print("Distance from average: ", total_avg_distance_invalid / counter_invalid)
    else:
        print("Distance from average:Not Found ", )

    print("--------------------------------")
    positive.append((counter_valid / (x.shape[0] / step_n)) * 100)
    negative.append((counter_invalid / (x.shape[0] / step_n)) * 100)
    deltas_total.append(delta)
    if counter_valid != 0:
        normal_flow.append(total_avg_distance_valid / counter_valid)
        # total_avg_valid = [x / counter_valid for x in total_avg_valid]
        total_avg_valid = total_avg_valid / counter_valid
        deltas_valid.append(delta)
    if counter_invalid != 0:
        event_flow.append(total_avg_distance_invalid / counter_invalid)
        deltas_invalid.append(delta)
        total_avg_invalid = total_avg_invalid / counter_invalid
        # total_avg_invalid = [x / counter_invalid for x in total_avg_invalid]

    avgDistanceChart(avgFlow=sensor_avg, normalFlow=total_avg_valid, eventFlow=total_avg_invalid, delta=delta,
                     k=n_components, path=path)
    # new_input_whole_sensor_data_plot(daily_samples=daily_samples, x=x, K=n_components, Delta=delta,
    #                                  remained=remained, labels=s, path=path)
    print("Test done in %0.3fs." % (time() - t0))
    print("True positives", len(true_positive))
    print("True negatives", len(true_negative))
    print("False positives", len(false_positive))
    print("False negatives", len(false_negative))
    precision = len(true_positive) / (len(true_positive) + len(false_positive))
    recall = len(true_positive) / (len(true_positive) + len(false_negative))
    # F-Measure
    # b=0.1
    # f_measure = (1+b*b)*((precision*recall)/(b*b*precision+recall))


    f1_score = 2 * (precision * recall) / (precision + recall)
    print("Precision:", precision)
    print("Recall:", recall)
    print("F1 Score:", f1_score)
    recall_list.append(recall)
    precision_list.append(precision)
    f1_score_list.append(f1_score)
    delta_list.append(d)

#Varius Plots

plotValidInvalidPercentage(k=n_components, valid=plot_valid_percentage, invalid=plot_invalid_percentage,
                           deltas=deltas_total, path=path)
deltaVarianceChart(deltas_total, positive, negative, k=n_components, path=path)
deltaVarianceImpactOnFlowChart(deltas_valid, deltas_invalid, normal_flow, event_flow, k=n_components, path=path)
recallPlot(recall_list, delta_list, path=path)
precisionPlot(precision_list, delta_list, path=path)
f1ScorePlot(f1_score_list, delta_list, path=path)

# Re-initialization of local matrices
deltas_valid = []
deltas_invalid = []
deltas_total = []

positive = []
negative = []
normal_flow = []
event_flow = []
plot_valid_percentage = []
plot_invalid_percentage = []
