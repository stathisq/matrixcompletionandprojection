import pandas as pd
import numpy as np
import pickle


data = pd.read_csv("dataset_ubicomp2013_checkins.txt", sep='\t')
print(len(data["Users"]))

users = set()

init_users=dict()
for user in data["Users"]:
    if user in init_users:
        init_users[user]+=1
    else:
        init_users[user]=0

for k,v in init_users.items():
    if v >10:
        users.add(k)

print(len(users))

matrix_users_dict = dict()
i = 0
for user in users:
    matrix_users_dict[i] = user
    i += 1

users_matrix_dict = dict()

for key in matrix_users_dict.keys():
    users_matrix_dict[matrix_users_dict[key]] = key


init_pois=dict()

for poi in data["Pois"]:
    if poi in init_pois:
        init_pois[poi]+=1
    else:
        init_pois[poi]=0


pois = set()

for k,v in init_pois.items():
    if v >2:
        pois.add(k)


matrix_pois_dict = dict()

i = 0
for poi in pois:
    matrix_pois_dict[i] = poi
    i += 1

pois_matrix_dict = dict()

for key in matrix_pois_dict.keys():
    pois_matrix_dict[matrix_pois_dict[key]] = key

print(len(pois))

X = np.zeros([len(users), len(pois)],dtype=int)

for d in data.iterrows():
    # print(d[1]["Users"])
    if (d[1]['Users'] in users_matrix_dict) and (d[1]['Pois'] in pois_matrix_dict):

        X[users_matrix_dict[d[1]['Users']], pois_matrix_dict[d[1]['Pois']]] += 1

f = open("input_matrix_non_zeros","w")
ltz = np.where(X>0)
for i in range(0,ltz[0].shape[0]):
        s= str(ltz[0][i]) + ", "+ str(ltz[1][i])+ ", "+ str(X[ltz[0][i],ltz[1][i]]) + "\n"
        f.write(s)

pickle.dump(X, open("X.p", 'wb'))
