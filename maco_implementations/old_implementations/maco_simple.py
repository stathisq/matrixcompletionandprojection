from time import time

import numpy as np
from scipy import linalg as lg, random
import multiprocessing
import sys

from plots.maco_coverage import coverage


# Calculate E subset
def calcE(X):
    return np.where(X > 0)


# Calculate Lipschitz constant for Row update (Eq 6)
def calcRowW(m, R, E, E_unique, r, i):
    W = R[r, E[1][np.where(E[0] == i)]]
    W1 = R[r, E_unique[:, 1][np.where(E_unique[:, 0] == i)]]
    return m + np.sum(np.square(W)) + np.sum(np.square(W1))


# Calculate Lipschitz constant for Column update (Eq 10)
def calcColW(m, L, E, E_unique, r, j):
    W = L[E[0][np.where(E[1] == j)], r]
    W1 = L[E_unique[:, 0][np.where(E_unique[:, 1] == j)], r]
    return m + np.sum(np.square(W)) + np.sum(np.square(W1))


# Calculate Delta for Row Update (Eq 8)
def computeRowDelta(L, R, X, X_u, X_l, m, i, r, E, E_u, E_l, W):
    delta = (m * L[i, r]) + np.sum((np.dot(
        L[i, :].dot(R[:, E[1][np.where(E[0] == i)]]) - X[i, E[1][np.where(E[0] == i)]],
        R[r, E[1][np.where(E[0] == i)]])))

    l_sum = 0
    u_sum = 0
    for j in E_u[:, 1][np.where(E_u[:, 0] == i)]:
        temp = L[i, :].dot(R[:, j])

        if temp > X_u[i, j]:
            l_sum += (temp - X_u[i, j]) * R[r, j]

    for j in E_l[:, 1][np.where(E_l[:, 0] == i)]:
        temp = L[i, :].dot(R[:, j])
        if temp < X_l[i, j]:
            u_sum += (temp - X_l[i, j]) * R[r, j]

    delta += l_sum + u_sum
    return -delta / W


# Calculate Delta for Column Update (Eq. 11)
def computeColDelta(L, R, X, X_u, X_l, m, j, r, E, E_u, E_l, W):
    delta = (m * R[r, j]) + np.sum((np.dot(
        L[E[0][np.where(E[1] == j)], :].dot(R[:, j]) - X[E[0][np.where(E[1] == j)], j],
        L[E[0][np.where(E[1] == j)], r])))
    l_sum = 0
    u_sum = 0
    for i in E_u[:, 0][np.where(E_u[:, 1] == j)]:
        temp = L[i, :].dot(R[:, j])
        if temp > X_u[i, j]:
            l_sum += (temp - X_u[i, j]) * L[i, r]

    for i in E_l[:, 0][np.where(E_l[:, 1] == j)]:
        temp = L[i, :].dot(R[:, j])
        if temp < X_l[i, j]:
            u_sum += (temp - X_l[i, j]) * L[i, r]

    delta += l_sum + u_sum
    return -delta / W


def maco(X, X_u, X_l, dimension=15, iterations=10000, m=0.0001):

    t0=time()

    ##### variables for plots
    l_up_run = 0
    r_up_run = 0
    RMSE = []
    dimensions = X.shape

    # Init E, L, R and upper/low bounds
    E = calcE(X)
    E_l = calcE(X_l)
    E_u = calcE(X_u)
    print(E_l)
    print(E_u)
    # x = np.concatenate([x, temp], axis=1)

    E_l = np.column_stack((E_l[0], E_l[1]))
    E_u = np.column_stack((E_u[0], E_u[1]))
    E_lu = np.concatenate([E_l, E_u], axis=0)
    E_unique = np.unique(E_lu, axis=0)
    # final_EL = np.zeros[(0,2)]
    # my_set = set(E_lu)

    # L and R are initiated with random values
    L = np.random.rand(dimensions[0], dimension)
    R = np.random.rand(dimension, dimensions[1])
    #
    m_current = sys.maxsize
    print("Max iterations: ", iterations)
    counter = 0
    iter=0
    #Alg line 2
    while (l_up_run + r_up_run) / (X.shape[0] + X.shape[1]) <100 and iter<iterations:
    # for iter in range(0, iterations):  # Alg. line 2
        iter+=1
    #Alg line 5
        r = random.randint(0, dimension)
        idx = np.random.randint(low=0, high=L.shape[0], size=int(multiprocessing.cpu_count()))
        #Alg lines 6-8
        for i in np.nditer(idx):
            l_up_run += 1
            w = calcRowW(m, R, E, E_unique, r, i)
            delta = computeRowDelta(L, R, X, X_u, X_l, m, i, r, E, E_u, E_l, w)
            L[i, r] += delta
        # Alg line 9
        r = random.randint(0, dimension)
        idx = np.random.randint(low=0, high=R.shape[1], size=int(multiprocessing.cpu_count()))
    # Alg lines 10-14
        for j in np.nditer(idx):
            r_up_run += 1
            w = calcColW(m, L, E, E_unique, r, j)
            delta = computeColDelta(L, R, X, X_u, X_l, m, j, r, E, E_u, E_l, w)
            R[r, j] += delta

        print("Iteration:", iter)
        current = lg.norm(X - L.dot(R))
        RMSE.append(current)
        # If does not coverage for 4 continuous times, stop the algorithm
        if m_current < current:
            print("Counter: ", counter)
            if counter > 14:
                break
            counter += 1
            print(current)
            m_current = current
            continue
        counter = 0
        print("Reconstruction Error: ",current)
        print("Epoch:", str((l_up_run + r_up_run) / (X.shape[0] + X.shape[1])))
        m_current = current
        #Decrease m
        m = m * 0.9


    # coverage(RMSE)
    np.savetxt("L.csv", L, delimiter=",")
    np.savetxt("R.csv", R, delimiter=",")
    print("Matrix Factorization done in %0.3fs." % (time() - t0))

    return [L, R]

