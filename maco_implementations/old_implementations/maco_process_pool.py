from time import time
import sys
import numpy as np
import multiprocessing as mp
from scipy import linalg as lg, random
from multiprocessing import Process
import multiprocessing
from plots.maco_coverage import coverage

import SharedArray as sa
import warnings



def copy_to_shared_Array(np_array,name="name"):
    # array= Array('d' ,np_array.shape)
    array = sa.create("shm://"+name,np_array.shape)
    for i in range(0,np_array.shape[0]):
        for j in range(0,np_array.shape[1]):
            array[i,j]=np_array[i,j]
    return array


def runInParallel(*fns):
  proc = []
  for fn in fns:
    p = Process(target=fn)
    p.start()
    proc.append(p)
  for p in proc:
    p.join()


def calcE(X):
    return np.where(X > 0)

def calcE_l(X):
    return np.where(X >= 0)
# Calculate Lipschitz constant for Row update (Eq 6)
def calcRowW(m, R, E, E_unique, r, i):
    W = R[r, E[1][np.where(E[0] == i)]]
    W1 = R[r, E_unique[:, 1][np.where(E_unique[:, 0] == i)]]
    return m + np.sum(np.square(W)) + np.sum(np.square(W1))

# Calculate Lipschitz constant for Column update (Eq 10)
def calcColW(m, L, E, E_unique, r, j):
    W = L[E[0][np.where(E[1] == j)], r]
    W1 = L[E_unique[:, 0][np.where(E_unique[:, 1] == j)], r]
    return m + np.sum(np.square(W)) + np.sum(np.square(W1))



def computeRowDelta(L, R, X,X_u,X_l, m, i, r, E,E_u,E_l,E_unique):
    delta = (m * L[i, r]) + np.sum((np.dot(
        L[i, :].dot(R[:, E[1][np.where(E[0] == i)]]) - X[i, E[1][np.where(E[0] == i)]],
        R[r, E[1][np.where(E[0] == i)]])))

    W= calcRowW(m, R, E, E_unique, r, i)

    l_sum = 0
    u_sum = 0
    for j in E_u[:, 1][np.where(E_u[:, 0] == i)]:
        temp = L[i, :].dot(R[:, int(j)])
        if temp > X_u[i, int(j)]:
            l_sum += (temp - X_u[i, int(j)]) * R[r, int(j)]

    for j in E_l[:, 1][np.where(E_l[:, 0] == i)]:
        temp = L[i, :].dot(R[:, int(j)])
        if temp < X_l[i, int(j)]:
            u_sum += (temp - X_l[i, int(j)]) * R[r, int(j)]

    delta += l_sum + u_sum
    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        try:
            L[i, r] += -delta / W
        except Warning as e:
            L[i, r] += np.random.sample()
    # q.put([i,r,-delta/W])

def computeColDelta(L, R, X, X_u ,X_l , m, j, r, E, E_u ,E_l, E_unique):
    delta = (m * R[r, j]) + np.sum((np.dot(
        L[E[0][np.where(E[1] == j)], :].dot(R[:, j]) - X[E[0][np.where(E[1] == j)], j],
        L[E[0][np.where(E[1] == j)], r])))

    W = calcColW(m, L, E, E_unique, r, j)

    l_sum = 0
    u_sum = 0
    for i in E_u[:, 0][np.where(E_u[:, 1] == j)]:

        temp = L[int(i), :].dot(R[:, j])
        if temp > X_u[int(i), j]:
            l_sum += (temp - X_u[int(i), j]) * L[int(i), r]

    for i in E_l[:, 0][np.where(E_l[:, 1] == j)]:
        temp = L[int(i), :].dot(R[:, j])
        if temp < X_l[int(i), j]:
            u_sum += (temp - X_l[int(i), j]) * L[int(i), r]

    delta += l_sum + u_sum
    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        try:
            R[r, j] += -delta / W
        except Warning as e:
            R[r,j] += np.random.sample()
    # q.put([r,j,-delta/W])
    return -delta / W


def maco(X, X_u, X_l, dimension=15, m=0.0001,epoch_n=100):
    t0 = time()

    ##### variables for plots
    l_up_run = 0
    r_up_run = 0
    RMSE = []
    dimensions = X.shape
    current = sys.maxsize

    # Init E, L, R and upper/low bounds
    E = calcE(X)
    E_l = calcE_l(X_l)
    E_u = calcE(X_u)

    E_l = np.column_stack((E_l[0], E_l[1]))
    E_u = np.column_stack((E_u[0], E_u[1]))
    E_lu = np.concatenate([E_l, E_u], axis=0)
    E_unique = np.unique(E_lu, axis=0)
   # L, R = pickle.load(open("L-R.p",'rb'))
    L = np.random.rand(dimensions[0], dimension)
    R = np.random.rand(dimension, dimensions[1])
    L[L == 0] = 0.01
    R[R == 0] = 0.01
    l= sa.list()

    for i in l:
        print(str(i[0]))
        sa.delete("shm://"+ str(i[0].decode('UTF-8')))

    E_l = copy_to_shared_Array(E_l,"E_l")
    E_u= copy_to_shared_Array(E_u,"E_u")
    #E_unique = copy_to_shared_Array(E_unique,"E_unique")
    # L and R are initiated with random values

    L= copy_to_shared_Array(L,"L")
    R= copy_to_shared_Array(R, "R")
    X_u =copy_to_shared_Array(X_u,"X_u")
    X_l =copy_to_shared_Array(X_l,"X_l")
    X = copy_to_shared_Array(X,"X")
    #

    iter = 0

    # Alg line 2
    try:

        while (l_up_run + r_up_run) / (X.shape[0] + X.shape[1]) <epoch_n:
            # for iter in range(0, iterations):  # Alg. line 2
            t1 = time()
            iter += 1
            # Alg line 5
            r = random.randint(0, dimension)
            proc = []
            results = []
            idx = np.random.randint(low=0, high=L.shape[0], size=int(multiprocessing.cpu_count()))
            for i in np.nditer(idx):
                l_up_run += 1
                results.append((L, R, X,X_u,X_l, m, i, r, E,E_u,E_l, E_unique))
            pool = mp.Pool(int(mp.cpu_count()))
            pool.starmap(computeRowDelta, results)
            pool.close()
            pool.join()
            results = []

            r = random.randint(0, dimension)
            idx = np.random.randint(low=0, high=R.shape[1], size=int(multiprocessing.cpu_count()))
            for j in np.nditer(idx):
                r_up_run += 1
                results.append((L, R, X,X_u,X_l, m, j, r, E, E_u, E_l, E_unique))

            pool = mp.Pool(int(mp.cpu_count()))
            pool.starmap(computeColDelta, results)
            pool.close()
            pool.join()


            temp_current = lg.norm(X - L.dot(R))

            if temp_current > current:
                m = m / 10
            current = temp_current
            print("Iteration time:", time() - t1)

            print("Reconstruction Error: ", current)
            RMSE.append(current)
            print("Iteration:", iter)



            # current = lg.norm(X - L.dot(R))
        # If does not coverage for 4 continuous times, stop the algorithm
            print("Epoch:", str((l_up_run + r_up_run) / (X.shape[0] + X.shape[1])))
        # Decrease m
        # m = m * 0.9
    except KeyboardInterrupt:
        pass
    coverage(RMSE)
    temp_current = lg.norm(X - L.dot(R))

    current = temp_current
    print("Reconstruction Error: ", current)
    print("Matrix Factorization done in %0.3fs." % (time() - t0))
    return [L, R]



X = np.array([[1.0, 0.0, 2.0], [0.0, 1.0, 1.0], [1.0, 0.0, 3.0]])
X_l=np.zeros(X.shape)
X_u = np.array([[1.0, 0.1, 2.0], [0.0, 1.0, 1.0], [1.0, 0.3, 3.0]])
L,R=maco(X,X_u,X_l,2,m=0.000000000001,epoch_n=1000)

res= L.dot(R)
print(res)