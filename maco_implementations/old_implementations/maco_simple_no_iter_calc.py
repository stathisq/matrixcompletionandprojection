import multiprocessing
from time import time

import numpy as np
from scipy import linalg as lg, random
import sys

# Calculate E subset
from plots.maco_coverage import coverage


def calcE(X):
    return np.where(X > 0)


# Calculate Lipschitz constant for Row update (Eq 6)
def calcRowW(m, R, E, E_unique, r, i):
    W = R[r, E[1][np.where(E[0] == i)]]

    W1 = R[r, E_unique[:, 1][np.where(E_unique[:, 0] == i)]]

    return m + np.sum(np.square(W)) + np.sum(np.square(W1))


# Calculate Lipschitz constant for Column update (Eq 10)
def calcColW(m, L, E, E_unique, r, j):
    W = L[E[0][np.where(E[1] == j)], r]
    W1 = L[E_unique[:, 0][np.where(E_unique[:, 1] == j)], r]
    return m + np.sum(np.square(W)) + np.sum(np.square(W1))


# Calculate Delta for Row Update (Eq 8)
def computeRowDelta(L, R, X, X_u, X_l, m, i, r, E, E_u, E_l, W):
    delta = (m * L[i, r]) + np.sum((np.dot(
        L[i, :].dot(R[:, E[1][np.where(E[0] == i)]]) - X[i, E[1][np.where(E[0] == i)]],
        R[r, E[1][np.where(E[0] == i)]])))

    l_sum = 0
    u_sum = 0
    for j in E_u[:, 1][np.where(E_u[:, 0] == i)]:
        temp = L[i, :].dot(R[:, j])

        if temp > X_u[i, j]:
            l_sum += (temp - X_u[i, j]) * R[r, j]

    for j in E_l[:, 1][np.where(E_l[:, 0] == i)]:
        temp = L[i, :].dot(R[:, j])
        if temp < X_l[i, j]:
            u_sum += (temp - X_l[i, j]) * R[r, j]

    delta += l_sum + u_sum
    return -delta / W


# Calculate Delta for Column Update (Eq. 11)
def computeColDelta(L, R, X, X_u, X_l, m, j, r, E, E_u, E_l, W):
    delta = (m * R[r, j]) + np.sum((np.dot(
        L[E[0][np.where(E[1] == j)], :].dot(R[:, j]) - X[E[0][np.where(E[1] == j)], j],
        L[E[0][np.where(E[1] == j)], r])))
    l_sum = 0
    u_sum = 0
    for i in E_u[:, 0][np.where(E_u[:, 1] == j)]:
        temp = L[i, :].dot(R[:, j])
        if temp > X_u[i, j]:
            l_sum += (temp - X_u[i, j]) * L[i, r]

    for i in E_l[:, 0][np.where(E_l[:, 1] == j)]:
        temp = L[i, :].dot(R[:, j])
        if temp < X_l[i, j]:
            u_sum += (temp - X_l[i, j]) * L[i, r]

    delta += l_sum + u_sum
    return -delta / W


def maco(X, X_u, X_l, dimension=15, m=0.0001,epoch_n=100):

    t0=time()

    ##### variables for plots
    l_up_run = 0
    r_up_run = 0
    RMSE = []
    dimensions = X.shape
    current= sys.maxsize

    # Init E, L, R and upper/low bounds
    E = calcE(X)
    print("calcE(X) done")
    E_l = calcE(X_l)
    print("calcE(X_l) done")

    E_u = calcE(X_u)
    print("calcE(X_u) done")

    E_l = np.column_stack((E_l[0], E_l[1]))
    E_u = np.column_stack((E_u[0], E_u[1]))
    E_lu = np.concatenate([E_l, E_u], axis=0)
    print("Es done")
    E_unique = np.unique(E_lu, axis=0)

    #E_unique =uniqueValuesIndices(E_lu)
    print("Eunique done")

    # final_EL = np.zeros[(0,2)]
    # my_set = set(E_lu)

    # L and R are initiated with random values
    L = np.random.rand(dimensions[0], dimension)
    L[L==0]=0.01
    R = np.random.rand(dimension, dimensions[1])
    R[R==0]=0.01

    #
    iter=0
    #Alg line 2
    try:
        while (l_up_run + r_up_run) / (X.shape[0] + X.shape[1]) <epoch_n:
        # for iter in range(0, iterations):  # Alg. line 2
            t1=time()
            iter+=1
        #Alg line 5
            r = random.randint(0, dimension)
            idx = np.random.randint(low=0, high=L.shape[0], size=int(multiprocessing.cpu_count()))
            #Alg lines 6-8
            for i in np.nditer(idx):
                l_up_run += 1
                w = calcRowW(m, R, E, E_unique, r, i)
                delta = computeRowDelta(L, R, X, X_u, X_l, m, i, r, E, E_u, E_l, w)
                L[i, r] += delta
            # Alg line 9
            r = random.randint(0, dimension)
            idx = np.random.randint(low=0, high=R.shape[1], size=int(multiprocessing.cpu_count()))
        # Alg lines 10-14
            for j in np.nditer(idx):
                r_up_run += 1
                w = calcColW(m, L, E, E_unique, r, j)
                delta = computeColDelta(L, R, X, X_u, X_l, m, j, r, E, E_u, E_l, w)
                R[r, j] += delta
            print("Iteration time:", time() - t1)

            temp_current = lg.norm(X - L.dot(R))

            if temp_current > current:
                m = m / 10
            current = temp_current

            # current = lg.norm(X - L.dot(R))
            print("Reconstruction Error: ", current)
            RMSE.append(current)
            print("Iteration:", iter)
            # If does not coverage for 4 continuous times, stop the algorithm
            print("Epoch:", str((l_up_run + r_up_run) / (X.shape[0] + X.shape[1])))
            #Decrease m
            # m = m * 0.9


    except KeyboardInterrupt:
        pass
    coverage(RMSE)
    temp_current = lg.norm(X - L.dot(R))

    current=temp_current
    print("Reconstruction Error: ", current)
    print("Matrix Factorization done in %0.3fs." % (time() - t0))
    # coverage(RMSE)
    return [L, R]

def uniqueValuesIndices(input_matrix):
    E_unique_dict=dict()
    for i in range(0, input_matrix.shape[0]):
        for j in range(0, input_matrix.shape[1]):
            E_unique_dict[tuple(input_matrix[i, :])] = i

    E_unique = np.empty((0,input_matrix.shape[1]), dtype=np.int)
    for key, value in E_unique_dict.items():
            E_unique = np.concatenate((E_unique,np.reshape(input_matrix[value,:],(1,input_matrix.shape[1]))))

    return E_unique
