from __future__ import division
from __future__ import print_function

import os
import sys
sys.path.extend(['/home/stathis/PycharmProjects/matrixcompletionandprojection'])

sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname("__file__"), '..')))

# temporary solution for relative imports in case pyod is not installed
# if pyod is installed, no need to use the following line
from pyod.models.abod import ABOD
from pyod.models.hbos import HBOS
from pyod.models.pca import PCA

from tools import MatrixCreator

import matplotlib.pyplot as plt

from pyod.models.knn import KNN
from pyod.utils.data import get_outliers_inliers
from pyod.utils.data import check_consistent_shape
from pyod.utils.data import evaluate_print
import numpy as np
from sklearn import metrics



def visualize(clf_name, X_train, y_train, X_test, y_test, y_train_pred,
              y_test_pred, show_figure=True, save_figure=False):
    """Utility function for visualizing the results in examples.
    Internal use only.

    Parameters
    ----------
    clf_name : str
        The name of the detector.

    X_train : numpy array of shape (n_samples, n_features)
        The training samples.

    y_train : list or array of shape (n_samples,)
        The ground truth of training samples.

    X_test : numpy array of shape (n_samples, n_features)
        The test samples.

    y_test : list or array of shape (n_samples,)
        The ground truth of test samples.

    y_train_pred : numpy array of shape (n_samples, n_features)
        The predicted binary labels of the training samples.

    y_test_pred : numpy array of shape (n_samples, n_features)
        The predicted binary labels of the test samples.

    show_figure : bool, optional (default=True)
        If set to True, show the figure.

    save_figure : bool, optional (default=False)
        If set to True, save the figure to the local.

    """

    def _add_sub_plot(X_inliers, X_outliers, sub_plot_title,
                      inlier_color='blue', outlier_color='orange'):
        """Internal method to add subplot of inliers and outliers.

        Parameters
        ----------
        X_inliers : numpy array of shape (n_samples, n_features)
            Outliers.

        X_outliers : numpy array of shape (n_samples, n_features)
            Inliers.

        sub_plot_title : str
            Subplot title.

        inlier_color : str, optional (default='blue')
            The color of inliers.

        outlier_color : str, optional (default='orange')
            The color of outliers.

        """
        plt.axis("equal")
        plt.scatter(X_inliers[:, 0], X_inliers[:, 1], label='inliers',
                    color=inlier_color, s=40)
        plt.scatter(X_outliers[:, 0], X_outliers[:, 1],
                    label='outliers', color=outlier_color, s=50, marker='^')
        plt.title(sub_plot_title, fontsize=15)
        plt.xticks([])
        plt.yticks([])
        plt.legend(loc=3, prop={'size': 10})
        return

    # check input data shapes are consistent
    X_train, y_train, X_test, y_test, y_train_pred, y_test_pred = \
        check_consistent_shape(X_train, y_train, X_test, y_test, y_train_pred,
                               y_test_pred)

    if X_train.shape[1] != 2:
        raise ValueError("Input data has to be 2-d for visualization. The "
                         "input data has {shape}.".format(shape=X_train.shape))

    X_train_outliers, X_train_inliers = get_outliers_inliers(X_train, y_train)
    X_train_outliers_pred, X_train_inliers_pred = get_outliers_inliers(
        X_train, y_train_pred)

    X_test_outliers, X_test_inliers = get_outliers_inliers(X_test, y_test)
    X_test_outliers_pred, X_test_inliers_pred = get_outliers_inliers(
        X_test, y_test_pred)

    # plot ground truth vs. predicted results
    fig = plt.figure(figsize=(12, 10))
    plt.suptitle("Demo of {clf_name} Detector".format(clf_name=clf_name),
                 fontsize=15)

    fig.add_subplot(221)
    _add_sub_plot(X_train_inliers, X_train_outliers, 'Train Set Ground Truth',
                  inlier_color='blue', outlier_color='orange')

    fig.add_subplot(222)
    _add_sub_plot(X_train_inliers_pred, X_train_outliers_pred,
                  'Train Set Prediction', inlier_color='blue',
                  outlier_color='orange')

    fig.add_subplot(223)
    _add_sub_plot(X_test_inliers, X_test_outliers, 'Test Set Ground Truth',
                  inlier_color='green', outlier_color='red')

    fig.add_subplot(224)
    _add_sub_plot(X_test_inliers_pred, X_test_outliers_pred,
                  'Test Set Prediction', inlier_color='green',
                  outlier_color='red')

    if save_figure:
        plt.savefig('{clf_name}.png'.format(clf_name=clf_name), dpi=300)

    if show_figure:
        plt.show()

    return




def pyodExperiment():

    input_matrix="/home/stathis/PycharmProjects/matrixcompletionandprojection/stored_matrices/timeSeriesFull.csv"
    #The size of the input matrix
    size = 200
    #The number of event records
    # custom_event_number = 50
    #The test/train percentage
    train_percentage= 0.70
    #The two matrices

    normal, noisy = MatrixCreator.createSynthMatrixKeepZeros(input_matrix, size=size, Delta=20,
                                             mean=500,sensors_to_keep=10)
    # x = np.genfromtxt(input_matrix, delimiter=',')
    # #
    # noisy = MatrixCreator.createNoisyMatrixWithStaticNoise(x, 200, 100)

    # x=np.genfromtxt(input_matrix, delimiter=',')
    #The number of samples of the train matrix
    noOfSamples  = normal.shape[1]

    #The split point
    split= (int(size*train_percentage),int(size*(1-train_percentage)))

    #The normal and event train matrices
    normal_train = normal[:split[0],:noOfSamples]


    event_train = noisy[:split[0],:noOfSamples]
    #Keep only a specific number of event records
    # event_train = event_train[:custom_event_number,:noOfSamples]

    #The normal and event test matrices
    normal_test = normal[-split[1]:,:noOfSamples]




    event_test = noisy[-split[1]:,:noOfSamples]
    #Keep only a specific number of event records
    # event_test = event_test[-custom_event_number:,:noOfSamples]

    #
    half_shape_train=(normal_train.shape[0],1)
    half_shape_test=(normal_test.shape[0],1)


    # custom_shape_event=(custom_event_number,1)

    models = [KNN(),PCA(),HBOS(),ABOD()]
    clf_names = ['KNN','PCA','HBOS','ABOD']

    #The final train matrix
    x_train = np.concatenate((normal_train,event_train))

    #The final label matrix
    y_train = np.concatenate((np.full(half_shape_train,0),np.full(half_shape_train,1)))
   
   
    # y_train = np.concatenate((np.full(half_shape_train,0),np.full(custom_shape_event,1)))


    #The final test matrix
    x_test = np.concatenate((normal_test,event_test))
    #The final test labels matrix
    # y_test = np.concatenate((np.full(half_shape_test,0),np.full(custom_shape_event,1)))
    print(x_train.shape[0])
    print(split)
    # print(custom_event_number)
    evaluateCompetitors(x_train,x_test,x_test.shape[0]-len(event_test),y_train=y_train)
    # evaluateCompetitors(x_train,x_test,x_test.shape[0]-custom_event_number)
    # i=0
    # for model in models:
    #
    #     clf = model
    #     clf_name = clf_names[i]
    #     i+=1
    #     clf.fit(x_train,y_train)
    #
    #     # get the prediction labels and outlier scores of the training data
    #     y_train_pred = clf.labels_  # binary labels (0: inliers, 1: outliers)
    #     y_train_scores = clf.decision_scores_  # raw outlier scores
    #
    #     y_test_pred = clf.predict(x_test)  # outlier labels (0 or 1)
    #     y_test_scores = clf.decision_function(x_test)  # outlier scores
    #
    #     print("\nOn Training Data:")
    #     evaluate_print(clf_name, y_train, y_train_scores)
    #     print("\nOn Test Data:")
    #     evaluate_print(clf_name, y_test, y_test_scores)
    #     # visualize the results
    #     # visualize(clf_name, x_train, y_train, x_test, y_test, y_train_pred,
    #     #           y_test_pred, show_figure=True, save_figure=True)
    #
    #


def evaluateCompetitors(trainMatrix, validationMatrix, negative_start,y_train):
    models = [KNN(), PCA(), HBOS(), ABOD()]
    clf_names = ['KNN', 'PCA', 'HBOS', 'ABOD']
    recalls = []
    precisions=[]
    f1=[]
    y = np.full(trainMatrix.shape[0], 0)
    print(validationMatrix.shape)
    print(trainMatrix.shape)
    y_validation = np.concatenate((np.full((negative_start,1),0 ),np.full((validationMatrix.shape[0]-negative_start,1),1 )))


    i=0
    for model in models:

        clf = model
        clf_name = clf_names[i]
        i+=1
        clf.fit(trainMatrix,y_train)

        y_pred = clf.predict(validationMatrix)

        evaluate_print(clf_name, y_pred, y_validation)
        precisions.append(metrics.precision_score(y_validation, y_pred))
        recalls.append(metrics.recall_score(y_validation, y_pred))
        f1.append(metrics.f1_score(y_validation, y_pred))
        print("Accuracy Score", round(metrics.accuracy_score(y_validation, y_pred), 2))
        print("Precision Score", metrics.precision_score(y_validation, y_pred))
        print("Recall Score", metrics.recall_score(y_validation, y_pred))
        print("F1 Score", round(metrics.f1_score(y_validation, y_pred), 2))

    print(precisions)
    print(recalls)
    print(f1)


pyodExperiment()