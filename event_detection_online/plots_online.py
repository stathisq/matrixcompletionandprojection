from matplotlib import pyplot as plt
import os
import pickle
import numpy as np


def negatives_positives_days(positives: list = [], negatives: list = [], delta: str = "0", figure_n: object = 0) -> object:
    """

    :type inputs: Array containing all instances of RMSE
    """
    fontsize = 15
    plt.figure(figure_n)
    ax = plt.figure(figure_n).gca()

    ones = onelistmaker(len(negatives))
    zeros = zerolistmaker(len(positives))
    plt.plot(positives, zeros,"+",color= 'green', label="Normal ")
    plt.plot(negatives, ones, "*",color='red', label="Event")

    plt.xlabel("Day", fontsize=fontsize)
    plt.ylabel("Event", fontsize=fontsize)
    plt.xticks(fontsize=fontsize)
    plt.yticks((0,1),fontsize=fontsize)
    plt.legend(fontsize=fontsize, )
    # plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()
    if not os.path.exists("plot_files/"):
        os.makedirs("plot_files/")
    save_plase = "plot_files/negatives_positives_"+delta+".pdf"
    plt.savefig(save_plase)
    # plt.show()

    plt.close()





def positives_negatives_bars(positives, negatives,deltas):

    fontsize = 15
    n_groups = len(positives)

    means_men = positives
    # std_men = (2, 3, 4, 1, 2)

    means_women = negatives
    # std_women = (3)

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.4

    opacity = 1
    error_config = {'ecolor': '0.3'}

    rects1 = ax.bar(index, means_men, bar_width,
                    alpha=opacity, color='g',
                    label='Offline')

    rects2 = ax.bar(index + bar_width, means_women , bar_width,
                    alpha=opacity, color='r',
                    label='Online')

    ax.set_xlabel('Delta value',fontsize=15)
    ax.set_ylabel('Time',fontsize= 15)
    # ax.set_title('Scores by group and gender')
    ax.set_xticks(index + bar_width / 2)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    # plt.xlim(-1.5,2)
    plt.legend(fontsize=15)
    ax.set_xticklabels(deltas, fontsize=15)

    fig.tight_layout()
    plt.show()


def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros


def onelistmaker(n):
    listofones = [1] * n
    return listofones

#
# positives = pickle.load(open("positives.p", 'rb'))
# negatives = pickle.load(open("negatives.p", 'rb'))
# negatives_positives_days(positives,negatives)

results = pickle.load(open("results.p",'rb'))

n_positives=[]
n_negatives=[]
deltas = []
for k in results.keys():
    positives = results[k]["positives"]
    negatives = results[k]["negatives"]
    negatives_positives_days(positives=positives,negatives=negatives,delta=str(k))

for k in results.keys():
    n_positives.append(len(results[k]["positives"]))
    n_negatives.append(len(results[k]["negatives"]))
    deltas.append(k)
    # negatives_positives_days(positives=positives,negatives=negatives,delta=str(k))


positives_negatives_bars(n_positives,n_negatives,deltas)
