# Credit: Josh Hemann

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from collections import namedtuple

fontsize = 15
n_groups = 1

means_men = (45876)
# std_men = (2, 3, 4, 1, 2)

means_women = (458)
std_women = (3)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.4

opacity = 1
error_config = {'ecolor': '0.3'}

rects1 = ax.bar(index, means_men, bar_width,
                alpha=opacity, color='g',
                label='Offline')

rects2 = ax.bar(index + bar_width, means_women , bar_width,
                alpha=opacity, color='r',
                yerr=std_women, error_kw=error_config,
                label='Online')

ax.set_xlabel('Algorithm Version',fontsize=15)
ax.set_ylabel('Time',fontsize= 15)
# ax.set_title('Scores by group and gender')
ax.set_xticks(index + bar_width / 2)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlim(-1.5,2)
plt.legend(fontsize=15)
ax.set_xticklabels('Training time', fontsize=15)

fig.tight_layout()
plt.show()

